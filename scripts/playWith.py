#!/usr/bin/env tdaq_python
# Collects data from IS variables, OKS, cool DB and publishes
# data in IS according to a specific class

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object
import random
import sys
#print sys.path # to check pythonpath
import time
import re
import getopt
import os
from eformat import helper
from ispy import *
import ers
from ipc  import OWLTime
import pm.project
#import dl
#sys.setdlopenflags(dl.RTLD_GLOBAL | dl.RTLD_NOW)
import signal
from hilltrot.hilltrot import *
from sfodb.sfodb import SFOFetch
from optparse import OptionParser


# Global variables
maxBW = 105         # real value is 110 MB/s -
                    # value to be used for all 1 Gbit link connection (e.g. single SFI, pROS)
maxSFOBW_net = 210  # Network limit - real value is 220 MB/s
maxSFOBW = 350      # Disk limit, measured in May 2012
                    # It enters in the game after the dual network cards upgrade of the SFO (June 2012)

blackDict={}
specialAttributesDict = { 'RunNumber'      : ('_lb', 'RunNumber'),
                          'RunLB'          : ('_lb', 'LumiBlockNumber'),
                          'RCState'        : ('_rc', 'state'),
                          'DCSStableBeam'  : ('_stableBeam',),
                          # 'DCSBeamMode'    : ('_beamMode',),
                          'DCSSolenoid'    : ('F_setMagnets', 'solenoid', 7730.),
                          'DCSToroid'      : ('F_setMagnets', 'toroid', 20400.),
                          'T0RunTransferPerc'    : ('F_transfer2t0Perc',),
                          'InstantaneousLumi'    : ('_instLumi',),
                          'Mu'                   : ('_mu',)
                          }
attributesDict = {'RunOverview'    : ('F_runStr',),                       # Run
                  'RunTime'        : ('F_runtime_s',),
                  'RCFault'        : ('_rc', 'fault'),                    # Run control
                  'RCError'        : ('_rc', 'errorDescription'),
                  'RCBusy'         : ('_rc', 'busy'),
                  'RCGlobalBusy'   : ('_ctpcorebusy',),
                  'RCDAQBusy'      : ('_daqbusy',),
                  'HLTSMK'         : ('_tkeys', 'sm', 0),                 # HLT
                  'HLTBunchGroupK' : ('_tkeys', 'l1bg', 0),
                  'HLTPSL1'        : ('_tkeys', 'l1', 0),
                  'HLTPSHLT'       : ('_tkeys', 'hlt', 0),
                  'HLTPSL1phys'    : ('_tkeys', 'l1.p', 0),
                  'HLTPSL1standby' : ('_tkeys', 'l1.s', 0),
                  'HLTPSHLTphys'   : ('_tkeys', 'hlt.p', 0),
                  'HLTPSHLTstandby': ('_tkeys', 'hlt.s', 0),
                  'HLTSummary'     : ('F_hltStr',),
                  'DAQBackPressureStatus' : ('F_dfStr',),                 # DF overview
                  'HotRosName'     : ('_hotRosName',),                    # ROS
                  'HotRosRate'     : ('_hotRosRate',),
                  'HotRobinOcc'    : ('_hotRobinOcc',),
                  'HotRobinName'   : ('_hotRobinName',),
                  'HighROSLoad'   : ('_highRosLoad',),
                  'HighROSLoadName' : ('_highRosLoadName',),
                  'NumOfBusyROLs'  : ('F_len', '_busyRols'),
                  'L1OutRate'      : ('_l2sv', 'IntervalEventRate'),      # ROIB and L1
                  'ROIBOutChStatus': ('_roib', 'Output Channel Status'),
                  'ROIBInChStatus' : ('_roib', 'Input Channel Status'),
                  'BusyROIB'       : ('F_roibbusy',),
                  'SFOFarm'          : ('F_sfoStr',),                     # SFO
                  'SFOnum'           : ('_supervsfo', '_tot'),
                  'SFOnum_notup'     : ('_supervsfo', '_notup'),
                  'SFOnum_out'       : ('_supervsfo','_out'),
                  'SFOnum_fault'     : ('_supervsfo','_fault'),
                  'SFOmaxInBW'       : ('F_maxSFOInBW',),
                  'SFOEvRecRate'     : ('_sfo', 'CurrentEventReceivedRate'),
                  'SFOEvRecBW'       : ('_sfo', 'CurrentDataReceivedRate'),
                  'SFOdataBWPerc'    : ('F_sfoBWPerc',),
                  'SFOEvSaveRate'    : ('_sfo', 'CurrentEventSavedRate'),
                  'SFOEvSaveBW'      : ('_sfo', 'CurrentDataSavedRate'),
                  'SFOdiskUsageTop'  : ('F_sfoUsage',),
                  'SFOcpuPerc'       : ('_sfoProcAvg','IntervalCPU',),
                  'SFOReceivedphysRate'  : ('_sfoRecphy','Rate'),
                  'SFOReceivedcalibRate' : ('_sfoReccal','Rate'),
                  'SFOReceiveddebugRate' : ('_sfoRecdeb', 'Rate'),
                  'SFOReceivedexprRate'  : ('_sfoRecexp', 'Rate'),
                  'SFOReceivedphysBW'    : ('F_kB_MB_conv', '_sfoRecphy', 'Bandwidth'),
                  'SFOReceivedcalibBW'   : ('F_kB_MB_conv', '_sfoReccal', 'Bandwidth'),
                  'SFOReceiveddebugBW'   : ('F_kB_MB_conv', '_sfoRecdeb', 'Bandwidth'),
                  'SFOReceivedexprBW'    : ('F_kB_MB_conv', '_sfoRecexp', 'Bandwidth'),
                  'SFOSavedphysRate'     : ('_sfoSavphy', 'Rate'),
                  'SFOSavedcalibRate'    : ('_sfoSavcal', 'Rate'),
                  'SFOSaveddebugRate'    : ('_sfoSavdeb', 'Rate'),
                  'SFOSavedexprRate'     : ('_sfoSavexp', 'Rate'),
                  'SFOSavedphysBW'       : ('F_kB_MB_conv', '_sfoSavphy', 'Bandwidth'),
                  'SFOSavedcalibBW'      : ('F_kB_MB_conv', '_sfoSavcal', 'Bandwidth'),
                  'SFOSaveddebugBW'      : ('F_kB_MB_conv', '_sfoSavdeb', 'Bandwidth'),
                  'SFOSavedexprBW'       : ('F_kB_MB_conv', '_sfoSavexp', 'Bandwidth'),
                  'SFORSizePhys'         : ('F_ComputeMeanEvSize', '_sizesSfoR', 'physics'),
                  'SFORSizeCalib'        : ('F_ComputeMeanEvSize', '_sizesSfoR', 'calibration'),
                  'SFORSizeExp'          : ('F_ComputeMeanEvSize', '_sizesSfoR', 'express'),
                  'SFORSizeDebug'        : ('F_ComputeMeanEvSize', '_sizesSfoR', 'debug'),
                  'SFOSSizePhys'         : ('F_ComputeMeanEvSize', '_sizesSfoS', 'physics'),
                  'SFOSSizeCalib'        : ('F_ComputeMeanEvSize', '_sizesSfoS', 'calibration'),
                  'SFOSSizeExp'          : ('F_ComputeMeanEvSize', '_sizesSfoS', 'express'),
                  'SFOSSizeDebug'        : ('F_ComputeMeanEvSize', '_sizesSfoS', 'debug'),
                  'SFOSizeAvg'           : ('F_AvgSfoSize',),
                  'SFOdiskUsageList'     : ('_sfoDisks',),
                  'DetectorsList'        : ('_detectors',),      # Mix
                  'TopLoadList'          : ('F_topLoad',),
                  'TopLoadMem'           : ('F_topMem',),
                  'Streams'              : ('F_streams',),
                  'BusyList'             : ('F_l1ct',)

                  }


# Exceptions
class Exception( ers.Issue ):
    def __init__( self, msg, params, cause ):
        ers.Issue.__init__( self, msg, params, cause )

class ISKeyError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Invalid Criteria "%s" ' % msg , {}, cause )

class ISRepositoryNotFound(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Repository Not Found "%s" ' % msg , {}, cause )

class ISInvalidIterator(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Repository Not Found "%s" ' % msg , {}, cause )

class ISAttributeError(Exception):
    def __init__( self, fname, err, cause = None ):
        Exception.__init__( self, 'Problems with function "%s" due to error "%s ' % (fname, err) , {'function_name': fname, 'error':err}, cause )

class MyIOError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Problem: "%s" ' % msg , {}, cause )

class MyRuntimeError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Problem: "%s" ' % msg , {}, cause )


def treatErrors( e, is_entry, attributeName ):
    if attributeName in blackDict:
        blackDict[attributeName] = blackDict[attributeName]+1
        if blackDict[attributeName] == 3:
            if type(e).__name__ == 'ISAttributeError':
                ers.warning ( e )
            else:
                mrs_msg = ers.Issue( "Problem: '%s'" %e, {}, None)
                ers.warning(mrs_msg)
        elif blackDict[attributeName] == 30:
            if type(e).__name__ == 'ISAttributeError':
                ers.error ( e )
            else:
                mrs_msg = ers.Issue( "Problem: '%s'" %e, {}, None)
                ers.error(mrs_msg)
    else:
        blackDict[attributeName] = 1



def rootControllerState(partition):
    part = IPCPartition(partition)
    if not part.isValid():
        ers.log( '%s is not a valid partition name' % part_name)
        sys.exit(1)

    state = None
    try:
        r = InfoReader(part,'RunCtrl','RootController')
        r.update()
        state = r.objects['RunCtrl.RootController'].state
    except ers.Issue as e:
        # caught if in the constructor you pass a wrong ISServer name
        if e.__class__.__name__ == "RepositoryNotFound":
            raise ISRepositoryNotFound(e)
        elif e.__class__.__name__ == "InvalidIterator":
            raise ISInvalidIterator(e)
    except KeyError as e:
        raise ISKeyError(e)
    except AttributeError as e:
        raise ISAttributeError('rootControllerState', e)

    return state


# Extend the Summer class providing a dump to string
class Xummer(Summer): ##I don't understand Xummer class
    print("SUMMER = ", Summer)
    def summary(self):
        s = [ "%s(%d)" % (self.prefix, len(self.objects))]
        for attr in self.attributes:
            t = " sum.%s.%s" % (self.prefix, attr)
            s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
        s.append("\n")
        for attr in self.averages:
            t = " avg.%s.%s" % (self.prefix, attr)
            s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
        return ''.join(s) #puts list of strings together separated by ''

# Statistics of the RunCtrl.Supervision
class superv(object):
    _tot, _notup, _out, _fault = 0, 0, 0, 0
    def reset(self):
        self._tot, self._notup, self._out, self._fault = 0, 0, 0, 0
    def upd(self, y, theSet=None):
        self._tot += 1
        #if y.fault              : self._fault += 1
        #if y.membership == 'OUT': self._out   += 1
        if not y.membership: self._out +=1
        # y is a ISinfoDynAny object. Means it will take all the attributes from IS for that variable.
        # what is membership, host, status...
        if y.status     != 'UP' : self._notup += 1
        if theSet != None: #I never see theSet called in, why even have this?
            h=y.host.replace('.cern.ch','')
            if   y.status == 'UP': theSet.add(h)
            else:
                try: theSet.remove(h)
                except(KeyError): pass
    def dump(self): #never see this called either...
        return '%6d %6d %6d %6d' % (self._notup, self._out, self._fault, self._tot)

# Check ROL enable/disable state

from itertools importchain
from operator import attrgetter


det_folder2det_id = {'FORWARD':(0x81,0x84),
                     'L1CALO':(0x71,0x75),
                     'LAR':(0x41,0x48),
                     'MUON_MDT':(0x61,0x64),
                     'MUON_RPC':(0x65,0x65),
                     'MUON_SCT':(0x69,0x6a),
                     'MUON_TGC':(0x67,0x68),
                     'PIXEL':(0x11,0x13),
                     'SCT':(0x21,0x24),
                     'TDAQ_CTP':(0x77,0x77),
                     'SCT':(0x21,0x24),
                     'TDAQ_MUON_CTP':(0x76,0x76),
                     'TILE':(0x50,0x54),
                     'TRT':(0x30,0x34)}


det_id2det_folder = dict(chain(*[zip(det_ids,[folder]*len(det_ids)) for det_ids,folder in ((range(v[0],v[1]+1),folder) for (folder,v) in list(det_folder2det_id.items()))]))


def rolenabled(partition, rosid, id):

    part = IPCPartition(partition)
    rdb = pm.project.Project('rdbconfig:RDB@%s' % partition)

    ros = rdb.getObject('ROS',rosid)
    detector = ros.Detector.LogicalId

    rolid = [rol.id for rol in chain(*map(attrgetter('Contains'),ros.Contains))if rol.Id == id][0]

    folder = det_id2det_folder[detector]

    isinfo = 'Resources.Enabled:%s/Robins/%s' % \
             (folder, rolid)

    resenabled = ISObject(part, isinfo, 'EnabledResource')

    try:
        resenabled.checkout()
        return True
    except:
        return False


# Class Worker
class Worker(object):
    """Class gathering info from IS"""

    # Configuration
    _limitL2Rate= 75000.
    _isDelay    =     5      # Is update delay in s
    _isDelayFact=    20      # Is update delay multiplication factor for slow servers

    _rc         = ISInfoAny();         _rp          = ISInfoAny()
    _lb         = ISInfoAny();         _gb          = ISInfoAny()
    _roib       = ISInfoAny();         _efdsum      = ISInfoAny()
    _ptsum      = ISInfoAny();         _l2pusum     = ISInfoAny()
    _l2svsum    = ISInfoAny();
    _l1ctH       = ISInfoAny()
    _l2puTcpAvg = ISInfoAny();         _l2puProcAvg = ISInfoAny()
    _ptProcAvg  = ISInfoAny();         _sfoProcAvg  = ISInfoAny()
    _conf_sfi   = None;                _conf_dfm    = None
    _conf_l2sv  = None;                _conf_l2rh   = None
    _l2racks    = 0;                   _efracks     = 0
    _efXPUracks = 0;
    _maxL2BW    = 0;                   _maxEFBW     = 0
    _sfoDisks   = [];                  _detectors   = ""
    _top10mem   = [];                  _top10load   = []
    _busyCntSfi = 0;                   _busyDiffSfi = 0
    _ctpcorebusy= 0;                   _daqbusy     = 0
    _sizesSfoS  = {}; _sizesSfoR = {}; _sizesSfi = {}; _sizesFragSfi = {};
    _magnets    = {}
    _supervl2pu = superv();            _supervsfi   = superv()
    _supervefd  = superv();
    _supervpt    = superv()
    _supervsfo  = superv();            _supervl2rh  = superv()
    _supervl2sv = superv();
    _numLVL2Dec = 0;                   _numEfDec = 0
    _numLVL2Acc = 0;                   _numEfAcc = 0
    _l2Hosts    = set();               _efHosts     = set()
    _l2Err      = 0;                   _efErr       = 0
    _l2rhErr    = 0;
    _nSfos      = 0;                   _roibOut     = ''
    _streams    = {};
    _hotRobinOcc= 0;                   _hotRobinName= "";
    _hotRosRate = 0;                   _hotRosName  = ""
    _highRosLoadName = "";             _highRosLoad = 0
    _busyRols   = [];
    _stableBeam = False;               _1stLoop     = True
    _beamMode   = [];                  _forceUpdate = False
    _instLumi   = 0;                   _mu          = 0;
    _verbose    = False;
    _counter    = 0;
    _l2puRecB   = 0;                   _l2puRecT    = 0
    _efioSfiBrkConn = 0;
    _efioSfoBrkConn = 0
    _v = [-1,'']
    _tkeys      = { 'sm':   _v,  'l1':   _v, 'hlt':  _v, 'l1bg':  _v,
                    'l1.p': _v, 'hlt.p': _v, 'l1.s': _v, 'hlt.s': _v }
    _ctpcout12  = ['LUCID', 'PIXEL', 'SCT'     , 'TRT'      , 'CALO']
    _ctpcout13  = ['BCM'  , 'ZDC'  , 'LAR_FCAL', 'LAR_EM_E' , 'LAR_EM_B']
    _ctpcout14  = ['LHCf' , 'MDT_B', 'MDT_E'   , 'TILECAL_B', 'TILECAL_E']
    _ctpcout15  = ['CSC'  , 'ALFA', 'TGC'     , 'RPC'      , 'MUON_CTP']

    def readRDB(self):
        ers.log( '\nReading RDB ....')
        t0 = time.time()
        db = pm.project.Project('rdbconfig:RDB@%s' % (part_name))

        #self._conf_sfi = db.getObject('SFIConfiguration')[0]
        #self._conf_dfm = db.getObject('DFMConfiguration')[0]
        #self._conf_l2sv= db.getObject('L2SVConfiguration')[0]
        #self._conf_l2rh= db.getObject('L2RHConfiguration')[0]
        t1 = time.time()

        #ers.log( '\nSFI input buffer : %d\nSFI output buffer: %d\nDFM input buffer : %d\nL2SV l2 queue sz : %d\nL2RH store Size  : %d\nReading RDB done [%4.2f s]' % ( self._conf_sfi.MaxAssignEvents, self._conf_sfi.MaxOutputQueueSize, self._conf_dfm.MaxInputQueueSize, self._conf_l2sv.l2puQueueSize, self._conf_l2rh.storeSize, (t1-t0) ) )


    #
    def __init__(self, part_name):
        self._p       = IPCPartition(part_name) # Create partition object
        if not self._p.isValid():
            ers.log('%s is not a valid partition name' % part_name)
            sys.exit(1)

        self._pi      = IPCPartition('initial')
        if not self._pi.isValid():
            ers.log('initial is not a valid partition name')
            sys.exit(1)
        ''' put pack in at P1!
        self._pOLC      = IPCPartition('OLC')
        if not self._pOLC.isValid():
          ers.log('OLC is not a valid partition name')
          self.readRDB()
        '''

        # Data members


        self._sfo  = Xummer('SFO', self._p, [ 'DF' ], 'SFO-[0-9]+',
                            [ 'CurrentDataReceivedRate','CurrentDataSavedRate',
                              'CurrentEventReceivedRate','CurrentEventSavedRate', 'EventsReceived', 'EventsSaved' ])


        self._sfoRecphy = Xummer('PhyReceivRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Received.physics',     ['Rate', 'Bandwidth'] )
        self._sfoReccal = Xummer('CalReceivRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Received.calibration', ['Rate', 'Bandwidth'] )
        self._sfoRecexp = Xummer('ExpReceivRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Received.express',     ['Rate', 'Bandwidth'] )
        self._sfoRecdeb = Xummer('DebReceivRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Received.debug',       ['Rate', 'Bandwidth'] )
        self._sfoSavphy = Xummer('PhySaveRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Saved.physics',     ['Rate', 'Bandwidth'] )
        self._sfoSavcal = Xummer('CalSaveRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Saved.calibration', ['Rate', 'Bandwidth'] )
        self._sfoSavexp = Xummer('ExpSaveRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Saved.express',     ['Rate', 'Bandwidth'] )
        self._sfoSavdeb = Xummer('DebSaveRate', self._p, [ 'DF' ], 'SFO-.*StreamInfo.Saved.debug',       ['Rate', 'Bandwidth'] )

        self._sfongCounters = Xummer('SFOngCounters', self._p, [ 'DF' ], 'SFO-.*Counters.Global',
                                     ['EventsInsideAverage', 'ProcessingEventRate', 'ProcessingDataRate', 'WritingEventRate','WritingDataRate'],
                                     ['EventsLifeTimeAverage', 'ProcessingRunTimeAverage', 'ProcessingLifeTimeAverage', 'WritingRunTimeAverage', 'WritingLifeTimeAverage'])

        #self._l2proctime = Xummer('L2PU', self._p, ['DF-L2.*'], 'L2PU-[0-9]+', [],
        #                          ['ProcessingTime', 'ProcessingTimeFraction', 'IntervalTime'])

        #self._efproctime = Xummer('PT', self._p, ['DF-EF.*'], 'PT-.*:[0-9]+', [],
        #                          ['ProcessingTime', 'ProcessingTimeFraction', 'ProcessingTimeInterval'])


#     try:
#       self.readOracle()
#     except (MyIOError), e:
#       ers.warning(e)

    #

    def updateRCState(self):
#    y  = ISInfoAny()
        try:
            self._rc = ISObject(self._p, 'RunCtrl.RootController', 'RCStateInfo')
            self._rc.checkout()
            #it = ISInfoIterator(self._p, 'RunCtrl')
            #while it.next():
            #if it.name().startswith('RunCtrl.RootController'):
            #    it.value(self._rc)
            #    self._rc._update(it.name(), self._p)
        except ers.Issue as e:
            # caught if in the constructor you pass a wrong ISServer name
            if e.__class__.__name__ == "RepositoryNotFound":
                raise ISRepositoryNotFound(e)
            elif e.__class__.__name__ == "InvalidIterator":
                raise ISInvalidIterator(e)
        except KeyError as e:
            raise ISKeyError(e)
        except AttributeError as e:
            raise ISAttributeError('updateRC', e)


        #

    def updateRC(self):
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
                #self._supervl2pu.reset();  self._supervsfi.reset();
            self._supervsfo.reset()
            #self._supervefd.reset();   self._supervpt.reset();  self._supervl2rh.reset()
            #self._supervl2sv.reset();

        y  = ISInfoAny()
        '''
        try:

        except ers.Issue, e:
          # caught if in the constructor you pass a wrong ISServer name
          if e.__class__.__name__ == "RepositoryNotFound":
            ers.error(e)
        except KeyError, e:
          raise ISKeyError(e)

        '''

        self._rc = ISObject(self._p, 'RunCtrl.RootController', 'RCStateInfo')
        self._rc.checkout()
        if ((self._counter % self._isDelayFact) == 1 or self._1stLoop) :  # Parse supervisors occasionally
            it = ISInfoIterator(self._p, 'RunCtrl',ISCriteria('Supervision.SFO-.*'))
            while next(it):
                try:
                    y = ISInfoDynAny()
                    it.value(y)
                    self._supervsfo.upd(y)
                except AttributeError as e:
                    raise ISAttributeError('updateRC', e)

    #

    def updateRP(self):
        try:
            self._rp = ISObject(self._p, 'RunParams.RunParams', 'RunParams')
            self._rp.checkout()
        except:
            pass

        try:
            self._lb = ISObject(self._p, 'RunParams.LuminosityInfo', 'LumiBlock')
            self._lb.checkout()
        except:
            pass

        try:
            self._gb = ISObject(self._p, 'RunParams.GlobalBusy', 'GLOBALBUSY')
            self._gb.checkout()
        except:
            pass

        '''
        try:
          x = ISObject(self._p, 'RunParams.TrigConfSmKey', 'TrigConfSmKey')
          x.checkout()
          self._tkeys['sm']   = [x.SuperMasterKey, x.SuperMasterComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.TrigConfL1BgKey', 'TrigConfL1BgKey')
          x.checkout()
          self._tkeys['l1bg'] = [x.L1BunchGroupKey, x.L1BunchGroupComment]
        except:
          pass


        try:
          x = ISObject(self._p, 'RunParams.TrigConfL1PsKey', 'TrigConfL1PsKey')
          x.checkout()
          self._tkeys['l1']   = [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.TrigConfHltPsKey', 'TrigConfHltPsKey')
          x.checkout()
          self._tkeys['hlt']  = [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.Physics.L1PsKey', 'TrigConfL1PsKey')
          x.checkout()
          self._tkeys['l1.p'] = [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.Physics.HltPsKey', 'TrigConfHltPsKey')
          x.checkout()
          self._tkeys['hlt.p']= [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.Standby.L1PsKey', 'TrigConfL1PsKey')
          x.checkout()
          self._tkeys['l1.s']= [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
          pass

        try:
          x = ISObject(self._p, 'RunParams.Standby.HltPsKey', 'TrigConfHltPsKey')
          x.checkout()
          self._tkeys['hlt.s'] = [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
          pass
        '''
        if(self._counter % self._isDelayFact) == 1:
            self._detectors = ""
            detmask = helper.DetectorMask(self._rp.detector_mask)
            for d in detmask.sub_detectors():
                self._detectors += ( str(d).replace('_SIDE','') + '\n')

    #
    def updateSums(self):
        ht0 = time.time()
        '''
        self._dfm.update()
        self._l2sv.update()
        self._l2svm.update()
        self._l2rh.update()
        '''
        ht1 = time.time()
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            '''
            self._sfiphy.update()
            self._sfical.update()
            self._sfiexp.update()
            self._sfideb.update()
            '''
            self._sfoRecphy.update()
            self._sfoReccal.update()
            self._sfoRecexp.update()
            self._sfoRecdeb.update()
            self._sfoSavphy.update()
            self._sfoSavcal.update()
            self._sfoSavexp.update()
            self._sfoSavdeb.update()
            self._sfongCounters.update()
        ht2 = time.time()
        self._sfo.update()
        #self._sfi.update()
        #self._efd.update()
        ht3 = time.time()
        #if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            #self._l2proctime.update()
            #self._efproctime.update()
        ht4 = time.time()
        #self._l2exp.update()
        #self._busyDiffSfi = self._sfi.NumBusyCount-self._busyCntSfi
        #self._busyCntSfi  = self._sfi.NumBusyCount
        ht5 = time.time()
        if self._verbose:
            ers.log( 'SUM times:\ndfm/l2sv/l2rh meanEvSize sfi/sfo/efd procTime busy/l2exp\n%3.2f %3.2f %3.2f %3.2f %3.2f' % (ht1-ht0, ht2-ht1, ht3-ht2, ht4-ht3, ht5-ht4))


    def updateDF(self):
        self._streams      = {};  busych = {}
        self._hotRobinOcc  = 0;   self._hotRobinName = ""
        self._hotRosRate   = 0;   self._hotRosName   = ""
        self._highRosLoad  = 0;   self._highRosLoadName = "";
        saveRates = {}

        try:
            it = ISInfoIterator(self._p, 'DF', ISCriteria('.*'))
        except ers.Issue as e:
            # caught if in the constructor you pass a wrong ISServer name
            if e.__class__.__name__ == "RepositoryNotFound":
                ers.error(e)
        except KeyError as e:
            raise ISKeyError(e)

        x = ISInfoAny()
        try:
            while next(it):
                if it.name().startswith('DF.RoIB.RoIB_status'):
                    it.value(self._roib); self._roib._update(it.name(), self._p)
                    self._roibOut = re.split("\n", str(self._roib))[7]
                    # this is for listing all streams active in a run
                elif it.name().find('StreamInfo.Received') > 0:
                    it.value(x); x._update(it.name(), self._p)
                    s = x.Stream.replace('Received.','')
                    if s in self._streams:
                        self._streams[s] += x.Rate
                    else:
                        self._streams[s]  = x.Rate
                elif it.name().startswith('DF.ROS.ROS-') :
                    if it.name().find('.DataChannel') > 0:
                        it.value(x)
                        x._update(it.name(), self._p)
                        # y = x.pagesInUse / float(x.pagesInUse + x.pagesFree)
                        y=0
                        if y > self._hotRobinOcc :
                            self._hotRobinOcc  = y*100 # we want occupancy expressed in range [0-100]
                            self._hotRobinName = it.name().replace('DF.ROS.', '')
                        #if x.rolXoffStat and self._lb.RunNumber:
                           # busych[it.name().replace('DF.ROS.', '')] = x.robId
                    if not re.search('DF.ROS.ROS-.*\.+',it.name()):
                        it.value(x)
                        x._update(it.name(), self._p)
                        if x.requestRateHz > self._hotRosRate:
                            self._hotRosRate = x.requestRateHz
                            self._hotRosName = it.name().replace('DF.ROS.', '')
                        if x.numberOfQueueElements > 30:
                            if x.rosLoad > 70:
                                self._highRosLoadName = it.name().replace('DF.ROS.', '')
                                self._highRosLoad     = x.rosLoad

        # Check if the busy channel are disasbled
            self._busyRols = []
            for ch, v in busych.items():
                try:
                    if rolenabled(part_name, ch.split('.',1)[0], v):
                        self._busyRols.append(ch)
                except(KeyError, RuntimeError): self._busyRols.append(ch + '_??')

        except AttributeError as e:
            raise ISAttributeError('updateDF', e)

    #
    def updateMeanEvSize(self, path):
        StreamEvDict = {}

        it = ISInfoIterator(self._p, 'DF', ISCriteria(path))
        while next(it):
            if it.name().find("_") != -1:
                streamName = " "
            else:
                streamName = it.name().rpartition(".")[2]
                if streamName != " ":
                    any = ISInfoAny()
                    it.value(any)
                    any._update(it.name(), self._p)
                    rate = any.__getattribute__('Rate')
                    try:
                        bandwidth = any.__getattribute__('Bandwidth')
                        evnSize = old_div(bandwidth,rate)
                    except ZeroDivisionError:
                        evnSize = 0

                    if evnSize != 0:
                        if not streamName in list(StreamEvDict.keys()):
                            StreamEvDict[streamName] = [evnSize, 1]
                        else:
                            StreamEvDict[streamName][0] = StreamEvDict[streamName][0] + evnSize
                            StreamEvDict[streamName][1] = StreamEvDict[streamName][1] + 1
        return StreamEvDict

    def updateMeanFragSize(self, path):
        tot_frag_detId=[]
        tot_frag_size=[]
        tot_collection={}

        it = ISInfoIterator(self._p, 'DF', ISCriteria(path))
        while next(it):
            any = ISInfoAny()
            it.value(any)
            any._update(it.name(), self._p)
            tot_frag_detId = any.__getattribute__('SouceID')
            tot_frag_size = any.__getattribute__('Size')
            counter = 0
            for entry in tot_frag_detId[:]:
                if not entry in list(tot_collection.keys()):
                    tot_collection[entry] = [tot_frag_size[counter], 1]
                else:
                    tot_collection[entry][0] = tot_collection[entry][0] + tot_frag_size[counter]
                    tot_collection[entry][1] = tot_collection[entry][1] + 1
                    counter = counter + 1
        return tot_collection

    #Not here, but probably still need
    '''
    def updateL1CT(self):
      try:
        it = ISInfoIterator(self._p, 'L1CT-History', ISCriteria('.*'))
      except ers.Issue, e:
        # caught if in the constructor you pass a wrong ISServer name
        if e.__class__.__name__ == "RepositoryNotFound":
          ers.error(e)
      except KeyError, e:
        raise ISKeyError(e)

      try:
        while it.next():
          if it.name().startswith('L1CT-History.ISCTPBUSY'):
            it.value(self._l1ctH); self._l1ctH._update(it.name(), self._p)
            self._ctpcorebusy = self._l1ctH.ctpcore_moni0_rate  #Final Trigger Veto - Low Priority (in BusyPanel)
            self._daqbusy     = self._l1ctH.ctpcore_mon_rate #CTPCORE - Derand-2 (in BusyPanel)
      except(UnboundLocalError):
        self._ctpcorebusy = -1
        self._daqbusy     = -1
    '''

    #
    def updatePmg(self):
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            self._top10mem,     self._top10load, self._sfoDisks   = [], [], []
            nSfos = 0
            it = ISInfoIterator(self._p, 'PMG', ISCriteria('.*')); next(it)
            x = ISInfoAny()
            while next(it):
                if it.name().startswith('PMG.AGENT_'):
                    it.value(x); x._update(it.name(), self._p)
                    h = it.name().replace('PMG.AGENT_', '').replace('.cern.ch','')
                    self._top10mem.append( '%5d%s [%3d%s] %s' % (x.ram, '%', x.swap, '%', h) )
                    self._top10load.append('%6.1f %s'         % (x.load1, h))
                    if h.startswith('pc-tdq-sfo-'):
                        nSfos += 1
                        l = x.fs.split()
                        self._sfoDisks.append('%3s %s:%s' % (l[6],  h, l[5] ))
                        self._sfoDisks.append('%3s %s:%s' % (l[10], h, l[9] ))
                        self._sfoDisks.append('%3s %s:%s' % (l[14], h, l[13]))
                if it.name().startswith('PMG.pc-tdq-tpu'):
                    y = it.name().replace('PMG.','').replace('.cern.ch','')

                    #if self._1stLoop:
                    #  if   y.find('|PT')  > 0: self._efHosts.add(y.split('|')[0])
                    #  elif y.find('|L2PU')> 0: self._l2Hosts.add(y.split('|')[0])

            self._nSfos = nSfos
            self._sfoDisks.sort()
            self._sfoDisks.reverse()
            if self._1stLoop:
                #ers.log( 'Farm allocation nodes: L2/EF = %d/%d ' % (len(self._l2Hosts), len(self._efHosts)))
                self._1stLoop = False

    #DCS not there - add in again?
    def updateIni(self):
        try:
            #r = InfoReader(self._pi, 'DCS_GENERAL', 'Magnet.*Current.value')
            #r.update()
            #for obj in r.objects.values():
                #if   obj.get_name().find('Toroids')  > 0 : self._magnets['toroid']   = obj.value
                #elif obj.get_name().find('Solenoid') > 0 : self._magnets['solenoid'] = obj.value
            r = InfoReader(self._pi, 'LHC', 'StableBeamsFlag')
            r.update()
            for obj in list(r.objects.values()):
                if obj.get_name().startswith('LHC.StableBeamsFlag'): self._stableBeam = obj.value
            r = InfoReader(self._pi, 'LHC', 'BeamMode')
            r.update()
            for obj in list(r.objects.values()):
                if obj.get_name().startswith('LHC.BeamMode'): self._beamMode = obj.value
        except KeyError as e:
            raise ISKeyError(e)
        except AttributeError as e:
            raise ISAttributeError('updateIni', e)
        except  ers.Issue as e:
            if e.__class__.__name__ == "RepositoryNotFound":
                raise ISRepositoryNotFound(e)

           #
    #only at P1 --put back in
    '''
    def updateOLC(self):
      try:
        r = InfoReader(self._pOLC, 'OLC', 'OLCApp/ATLAS_PREFERRED_Inst')
        r.update()
        for obj in r.objects.values():
          if obj.get_name().startswith('OLC.OLCApp/ATLAS_PREFERRED_Inst'):
            self._instLumi = obj.CalibLumi
            self._mu =  obj.Mu
      except KeyError, e:
        raise ISKeyError(e)
      except AttributeError, e:
        raise ISAttributeError('updateOLC', e)
      except  ers.Issue, e:
        if e.__class__.__name__ == "RepositoryNotFound":
          raise ISRepositoryNotFound(e)
    '''



    # Read from IS
    def Update(self, mode='long'):
        """Update ISInfo objects"""

        self._counter += 1

        tt=[time.time()]

        # 1. Update from initial partition: Magnets and stable beam
        try:
            self.updateIni()
            tt.append(time.time())
        except (ISKeyError,ISAttributeError,ISRepositoryNotFound) as e:
            ers.warning(e)

        # 2. Update from OLC partition: instantaneous luminosity & Mu--put back in at P1
        '''
        try:
          self.updateOLC()
          tt.append(time.time())
        except (ISKeyError,ISAttributeError,ISRepositoryNotFound), e:
          ers.warning(e)
        '''
        # 3. Update RunParams.RunParams
        try:
            self.updateRP()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # If not running skip the other updates
        if mode=='short':
            return

        # 4. Update RunCtrl
        try:
            self.updateRC()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 5. Update summers
        try:
            self.updateSums()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 6. Update DF: ROIB, Sizes, BusyRols
        try:
            self.updateDF()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 7. Update updatemeanEvSize & FragmentSizes

        try:
            #self._sizesSfi  = self.updateMeanEvSize('SFI-.*.StreamInfo.*')
            self._sizesSfoR = self.updateMeanEvSize('SFO-.*.StreamInfo.Received.*')
            self._sizesSfoS = self.updateMeanEvSize('SFO-.*.StreamInfo.Saved.*')
            #self._sizesFragSfi  = self.updateMeanFragSize('SFI-.*.FragSize')
            #self._idsFragSfi  = self.updateMeanFragSize('SFI-.*.FragSize')
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)
        tt.append(time.time())

        ''' Add back!
        # 9. Update L1CT (current CTP busy status)
        try:
          self.updateL1CT()
        except ISKeyError, e:
          ers.warning(e)
        '''
        # 10. Update OS info form PMG; do it at a low rate (every 5 loops)
        tt.append(time.time())
        self.updatePmg()
        tt.append(time.time())


        t = time.time()
        s = ''
        for i in range(1, len(tt)):
            s += '%3.2f ' % (tt[i]-tt[i-1])
        o = "[%5d]: %4.2f s (%s)" % (self._counter, t-tt[0] , s)
        if self._verbose:
            ers.log('%s' % o)



# ------ Functions -----

    def F_l1ct(self):
        out = ['From L1CT History\n']
        dis = ['Disabled(?)\n']
        try:
            out.append('%6.1f%s %-10s [%s]\n' % (self._ctpcorebusy, '%', 'Global', 'ctpcore_moni0_rate'))
            out.append('%6.1f%s %-10s [%s]\n' % (self._daqbusy,     '%', 'DAQ',    'ctpcore_mon_rate'))

            for i in range(len(self._ctpcout12)):
                d = self._ctpcout12[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_12[i], '%', d, 'ctpcout12')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout13)):
                d = self._ctpcout13[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_13[i], '%', d, 'ctpcout13')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout14)):
                d = self._ctpcout14[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_14[i], '%', d, 'ctpcout14')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout15)):
                d = self._ctpcout15[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_15[i], '%', d, 'ctpcout15')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            out.sort(); out.reverse()
            dis.sort(); dis.reverse()
        except AttributeError as e:
            raise ISAttributeError('F_l1ct', e)
        return out + dis

    def F_streams(self):
        out = []
        for k, v in self._streams.items():
            d = self._sfo.CurrentEventReceivedRate
            p = 0.
            if d > 0: p = old_div(100*v,d)
            out.append("%6.1f Hz  %6.1f %s   %s" % (v, p, '%',k))
        out.sort()
        out.reverse()
        return out[:50]

    def F_topMem(self):
        self._top10mem.sort()
        self._top10mem.reverse()
        return self._top10mem[:10]

    def F_topLoad(self):
        self._top10load.sort();
        self._top10load.reverse()
        return self._top10load[:10]

    def F_runtime_s(self):
        try:
            if self._rp.timeEOR.c_time() : diffsec =  self._rp.timeEOR.c_time() - self._rp.timeSOR.c_time()
            else                         : diffsec =  OWLTime().c_time() - self._rp.timeSOR.c_time()
            return diffsec
        except AttributeError as e:
            raise ISAttributeError(' F_runtime_s', e)

    def F_runStr(self):
        try:
            diffsec = self.F_runtime_s()
            h, m  = old_div(diffsec,3600), old_div((diffsec%3600),60)
            s = "LB@Run: %d@%d   %dh:%dm" % (self._lb.LumiBlockNumber, self._lb.RunNumber, h, m)
            return s
        except AttributeError as e:
            raise ISAttributeError('F_runStr', e )

    def F_sfoBWPerc(self):
        v = self._sfo.CurrentDataReceivedRate
        try: p = float(v)/ self.F_maxSFOInBW()
        except (ZeroDivisionError): p = 0
        return p * 100

    def F_sfoStr(self):
        s =  "%3d SFOs, max input B/W: %d MB/s" % (self._supervsfo._tot-self._supervsfo._out, self.F_maxSFOInBW())
        return s

    def F_ComputeMeanEvSize(self, streamDict_str, streamType):
        streamDict = getattr(self, streamDict_str)
        mean = 0
        if streamType in streamDict:
            try:
                mean = old_div(streamDict[streamType][0], streamDict[streamType][1])
            except ZeroDivision:
                mean = 0
        if streamDict_str == '_sizesSfi':
            mean = mean * 1000.
        return mean

    def F_ComputeMeanFragSize(self, id_frag_Dict_str, type):
        id_frag_Dict = getattr(self, id_frag_Dict_str)
        tot_id = []
        tot_size = []
        for entry in id_frag_Dict.keys():
            tot_id.append(entry)
            mean =  old_div(id_frag_Dict[entry][0],id_frag_Dict[entry][1])
            tot_size.append(mean)
        if type == 'id':
            return tot_id
        elif type == 'size':
            return tot_size

    def F_AvgSfoSize(self):
        try: v =(1000.*self._sfo.CurrentDataReceivedRate) / float(self._sfo.CurrentEventReceivedRate)
        except (ZeroDivisionError): v = 0
        return v

    def F_hltStr(self):
        try:
            s = "SMK %d, BG %d, PS %d/%d [phys %d/%d, standby %d/%d]" % (
              self._tkeys['sm'][0],
              self._tkeys['l1bg'][0],
              self._tkeys['l1'][0],
              self._tkeys['hlt'][0],
              self._tkeys['l1.p'][0],
              self._tkeys['hlt.p'][0],
              self._tkeys['l1.s'][0],
              self._tkeys['hlt.s'][0],
              )
            return s
        except AttributeError as e:
            raise ISAttributeError('F_hltStr', e )


    def F_roibbusy(self):
        s = ""
        try: s = re.split(":", self._roibOut )[1].replace(' ----','')
        except(IndexError): s = "??"
        p = 0
        if s.find('1') > 0: p=1
        return p

    def F_sfoUsage(self):
        try: v = int(self._sfoDisks[0].split('%')[0])
        except(IndexError): v = 0
        return v

    def F_transfer2t0Perc(self):
        run = self._lb.RunNumber
        p = 0.
        return p*100
        try:
            local_sfo_fetch = SFOFetch(1, 'online')
            nr_closed = local_sfo_fetch.nr_closed_files([run])
            nr_trans = local_sfo_fetch.nr_transferred_files([run])
            p = 1.*nr_trans[0]/nr_closed[0]
        except (TypeError, ZeroDivisionError):
            p = 0.
        except (RuntimeError,NameError) as e:
            raise MyRuntimeError(e)
        return p*100


    def F_setMagnets(self, magnet_name, nominal_value):
        try:
            return (old_div(100*self._magnets[magnet_name],nominal_value))
        except AttributeError as e:
            raise ISAttributeError(' F_setMagnets', e)
        except KeyError as e:
            raise ISKeyError(e)


    def F_len(self, param):
        try:
            return len(getattr(self, param))
        except AttributeError as e:
            raise ISAttributeError(' F_len', e)

    '''
    def F_DFMRatePerc(self):
      try:
        return getattr(self._dfm, 'Rate of built events') /self._limitL2Rate
      except AttributeError, e:
        raise ISAttributeError('F_DFMRatePerc', e)
      except ZeroDivisionError, e:
        raise ISAttributeError('F_DFMRatePerc - zero division', e)


    def F_DFMBW(self):
      try:
        return float(getattr(self._dfm, 'Rate of built events')) / float(self._sfi.EventPayload)
      except AttributeError, e:
        raise ISAttributeError('F_DFMBW', e)
      except ZeroDivisionError, e:
        return -1

    def F_DFMBWPerc(self):
      try:
        return float(self.F_DFMBW()) / self.F_maxSFIInBW()
      except ZeroDivisionError, e:
        raise ISAttributeError('F_DFMBWPerc - zero division', e)
    '''
    def F_maxSFOInBW(self):
        return (self._supervsfo._tot - self._supervsfo._out) * maxSFOBW

    #!!
    '''
    def F_maxSFIInBW(self):
      return (self._supervsfi._tot - self._supervsfi._out) * maxBW
    '''
    #!!
    def F_getOKSparam(self, *args):
        try:
            return getattr(getattr(self, args[0]), args[1])
        except AttributeError as e:
            raise ISAttributeError('F_getOKSparam', e)


    def F_kB_MB_conv(self, param1, param2):
        try:
            return getattr(getattr(self, param1), param2) / 1000.
        except AttributeError as e:
            raise ISAttributeError('F_kB_MB_conv', e)



    def Publish(self, dictionary):
        for entry in dictionary.items():
            try:
            #print 'CHECK!', type(getattr(self, entry[1][0])).__name__, entry[0]
            #print '1', entry[0]
            #print '1a', getattr(pub, entry[0])
            #print '2', entry[1][0]
            #print '2a', getattr(self, entry[1][0])
            #print 'type', type(getattr(self, entry[1][0]))

                if len(entry[1]) == 1:
                    if type(getattr(self, entry[1][0])).__name__ == 'instancemethod':
                        setattr(pub, entry[0], getattr(self, entry[1][0])())
                    else:
                        setattr(pub, entry[0], getattr(self, entry[1][0]))
                        #print 'setattr(pub, entry[0], getattr(self, entry[1][0])):', pub, entry[0], getattr(self, entry[1][0])
                else:
                    if type(getattr(self, entry[1][0])).__name__ == 'instancemethod':
                        for arg in entry[1]:
                            if arg==entry[1][0]: pass
                            else:
                                args.append(arg)
                        setattr(pub, entry[0], getattr(self, entry[1][0])(*args))
                        args[:]=[]
                    elif type(getattr(self, entry[1][0])).__name__ == 'ISInfoAny':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'instance':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    #wtf??
                    elif type(getattr(self, entry[1][0])).__name__ == 'dict':
                        setattr(pub, entry[0], getattr(self, entry[1][0])[entry[1][1]][entry[1][2]])
                    else:
                        ers.log( 'PROBLEM!!! %s' % type(getattr(self, entry[1][0])))


            except (ISAttributeError, ISKeyError, ISInvalidIterator, AttributeError, OverflowError, UnboundLocalError, MyRuntimeError) as e:
                ers.log( 'Know exception caught in Publish(): %s' % e)
                if args:
                    args[:]=[]
                treatErrors( e, entry[0], entry[1])
            except Exception as e:
                ers.log( 'Unknown exception caught in Publish(): %s '% e)

#    print "-------------------------"
#    print 'dictionary', dictionary
#    print 'The dictionary contains %d variables' % len(attributesDict)
#    print 'blackDict', blackDict
#    print "-------------------------"



    def initISObj(self): #initializing everything to 0 it looks like
        initDict={}
        for entry in attributesDict.items():
            #print entry[0], type(getattr(pub, entry[0])).__name__
            if (type(getattr(pub, entry[0])).__name__ == 'str'):
                initDict[entry[0]] = 'No Info'
            elif (type(getattr(pub, entry[0])).__name__ == 'list'):
                if (pub.getAttributeDescription(entry[0]).typeName() == 'string'):
                    iniList=[] #why is there a list?
                    iniList.append("No Info")
                    initDict[entry[0]] = iniList
                elif (pub.getAttributeDescription(entry[0]).typeName() == 'u32'):
                    initDict[entry[0]] = [0]
            elif (type(getattr(pub, entry[0])).__name__ == 'int') or (type(getattr(pub, entry[0])).__name__ == 'float'):
                initDict[entry[0]] = 0
            else:
                ers.log( '??? %s' % type(getattr(pub, entry[0])).__name__ )

        pub.set(initDict)



# ======== signal handler =========
def sig_handler(signum, f_globals):
    global run
    ers.log( 'Signal handler called with signal %s' % signum)
    run = False

#==================== MAIN =====================================


#Readcommand line parameters
global run
run = True

parser = OptionParser()
parser.add_option('-p', '--partition',
                  dest='part_name', type='string', default = 'ATLAS',
                  help='Pass the partition name - default = ATLAS')
parser.add_option('-v', '--verbose',
                  dest='verbose', action="store_true", default = False,
                  help='Set it if you want a verbose mode - default=False')
parser.add_option('-u', '--isslowupdate',
                  dest='isslowupdate', type='int', default = 6,
                  help='After how many publication you want to update information which are cpu consuming - default = 6')
parser.add_option('-I', '--isinfoname',
                  dest='isinfoname', type='string', default = 'DFSummary',
                  help='Name of the IS variable - default = DFSummary')
(options, args) = parser.parse_args()

ers.log( '\nSTART\nPartition name = %s,\nisinfoname = %s, \nVerbose = %s,\nIS slow update every %s times' % (options.part_name, options.isinfoname, options.verbose, options.isslowupdate))

verbose      = options.verbose
part_name    = options.part_name
isinfoname   = options.isinfoname

# Instantiate worker object
worker = Worker(part_name)
worker._verbose    = verbose
worker._isDelayFact= options.isslowupdate


main_part       = IPCPartition(options.part_name)

pub = ISObject(main_part, 'DF.%s' % options.isinfoname, 'DFSummary')

#main_part       = IPCPartition('dfs_test')
#pub = ISObject(main_part, 'DF.%s' % options.isinfoname, 'DFSummary')

worker.initISObj()

while run:
    try:
        worker.updateRCState()
        if worker._rc.state == 'RUNNING':
            worker.Update()
            worker.Publish(specialAttributesDict)
            worker.Publish(attributesDict)
        else:
            worker.initISObj()
            worker.Update('short')
            worker.Publish(specialAttributesDict)
            time.sleep(10)
    except (ISAttributeError, ISRepositoryNotFound, ISInvalidIterator, ISKeyError) as e:
        ers.error( e )
    except Exception as e:
        ers.error( 'Unknown Exception ', e)

    pub.checkin()
