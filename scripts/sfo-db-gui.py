#!/usr/bin/env tdaq_python

from future import standard_library
standard_library.install_aliases()
from builtins import range
from builtins import object
from tkinter import *
from tktools import Calendar
from tktools import MultiListbox
import cx_Oracle
import datetime
import time
import sfodb.sfodb_auth

class App(object):

    def __init__(self, master):

        self.frame = Frame(master)
        self.frame.pack
        
        db_connection = sfodb.sfodb_auth.useCoral()
        self.orcl = cx_Oracle.connect(db_connection)
        #Radio frame

        rframe = Frame(self.frame)

        l = Label(rframe, text="Table: ")
        l.grid(row=0,column=0,sticky=W)

        self.v = StringVar()
        self.v.set("RUN")

        r = Radiobutton(rframe, text="Run", variable=self.v, value="RUN", indicatoron=0, command=self.togglecheck)
        r.grid(row=0,column=1,sticky=W)

        r = Radiobutton(rframe, text="LumiBlock", variable=self.v, value="LUMIBLOCK", indicatoron=0, command=self.togglecheck)
        r.grid(row=0,column=2,sticky=W)
        r = Radiobutton(rframe, text="File", variable=self.v, value="FILE", indicatoron=0, command=self.togglecheck)
        r.grid(row=0,column=3,sticky=W)

        rframe.grid(row=0,sticky=W)

        #Output
        oframe = Frame(self.frame)

        l = Label(oframe, text="Output: ")
        l.grid(row=0,column=0,sticky=W)

        self.sfo = IntVar()
        self.sfo.set(1)
        c = Checkbutton(oframe, text="SFOID",
                        variable=self.sfo, indicatoron=0)
        c.grid(row=0,column=1,sticky=W)

        self.runnr = IntVar()
        self.runnr.set(1)
        c = Checkbutton(oframe, text="RUN#",
                        variable=self.runnr, indicatoron=0)
        c.grid(row=0,column=2,sticky=W)

        self.lbnr = IntVar()
        self.lbnr.set(0)
        c = Checkbutton(oframe, text="LB#",
                        variable=self.lbnr, indicatoron=0, state=DISABLED)
        c.grid(row=0,column=3,sticky=W)
        self.lbcheck = c;

        self.streamtype = IntVar()
        self.streamtype.set(0)
        c = Checkbutton(oframe, text="StreamType",
                        variable=self.streamtype, indicatoron=0)
        c.grid(row=0,column=4,sticky=W)

        self.streamname = IntVar()
        self.streamname.set(0)
        c = Checkbutton(oframe, text="StreamName",
                        variable=self.streamname, indicatoron=0)
        c.grid(row=0,column=5,sticky=W)

        self.nrevent = IntVar()
        self.nrevent.set(0)
        c = Checkbutton(oframe, text="Event #",
                        variable=self.nrevent, indicatoron=0,state=DISABLED)
        c.grid(row=0,column=6,sticky=W)
        self.nreventcheck = c

        self.fsize = IntVar()
        self.fsize.set(0)
        c = Checkbutton(oframe, text="Size (GB)",
                        variable=self.fsize, indicatoron=0,state=DISABLED)
        c.grid(row=0,column=7,sticky=W)
        self.fsizecheck = c

        oframe.grid(row=1,sticky=W)

        #Selection frame
        sframe = Frame(self.frame)
        l = Label(sframe, text="Selections: ")
        l.grid(row=0,column=0,sticky=W)

        self.maxrun = StringVar()
        self.maxrun.set("")
        t = Entry(sframe,textvariable=self.maxrun,width=10)
        t.grid(row=0,column=1,sticky=W)

        l = Label(sframe, text="> Run # >=")
        l.grid(row=0,column=2,sticky=W)

        self.minrun = StringVar()
        self.minrun.set("")
        t = Entry(sframe,textvariable=self.minrun,width=10)
        t.grid(row=0,column=3,sticky=W)

        self.nop1 = IntVar()
        self.nop1.set(0)
        c = Checkbutton(sframe, text="Out of P1",
                        variable=self.nop1)

        self.maxlb = StringVar()
        self.maxlb.set("")
        t = Entry(sframe,textvariable=self.maxlb,width=10)
        t.grid(row=1,column=1,sticky=W)

        l = Label(sframe, text="> LB # >=")
        l.grid(row=1,column=2,sticky=W)

        self.minlb = StringVar()
        self.minlb.set("")
        t = Entry(sframe,textvariable=self.minlb,width=10)
        t.grid(row=1,column=3,sticky=W)


        b = Button(sframe, text="C", command=self.maxcal)
        b.grid(row=2,column=0,sticky=E)

        self.maxdate = StringVar()
        self.maxdate.set("")
        t = Entry(sframe,textvariable=self.maxdate,width=10)
        t.grid(row=2,column=1,sticky=W)

        l = Label(sframe, text=">= Date >=")
        l.grid(row=2,column=2,sticky=W)

        self.mindate = StringVar()
        self.mindate.set("")
        t = Entry(sframe,textvariable=self.mindate,width=10)
        t.grid(row=2,column=3,sticky=W)

        b = Button(sframe, text="C", command=self.mincal)
        b.grid(row=2,column=4,sticky=W)

        c.grid(row=0,column=4,sticky=W)

        sframe.grid(row=2,sticky=W)

        #Button frame
        butframe = Frame(self.frame)

        button = Button(butframe, text="QUIT", fg="red",
                             command=self.frame.quit)
        button.pack(side=LEFT)
        button.grid(row=0,column=0)

        self.act = Button(butframe, text="Execute", command=self.update)
        self.act.pack(side=LEFT)
        self.act.grid(row=0,column=1)

        self.clean = Button(butframe, text="Clear", command=self.clear)
        self.clean.config(state=DISABLED)
        self.clean.pack(side=LEFT)
        self.clean.grid(row=0,column=2)

        butframe.pack()
        butframe.grid(row=3,sticky=W)

    def update(self):
        self.act.config(state=DISABLED)
        self.clean.config(state=NORMAL)
        rows=[]
        title=[]
        self.query(title=title,rows=rows)

        self.table = MultiListbox.MultiListbox(self.frame,title)
        self.table.config(height=5000)
        self.table.pack(expand=YES,fill=BOTH)
        self.table.grid(row=4)

        for i in range(0,len(rows)):
            self.table.insert(END,rows[i])

    def clear(self):
        self.table.destroy()
        self.act.config(state=NORMAL)
        self.clean.config(state=DISABLED)

    def mincal(self):
        Calendar.tkCalendar(self.frame, time.localtime()[0],
                            time.localtime()[1],
                            time.localtime()[2], self.mindate )

    def maxcal(self):
        Calendar.tkCalendar(self.frame, time.localtime()[0],
                            time.localtime()[1],
                            time.localtime()[2], self.maxdate )

    def togglecheck(self):
        if self.v.get() == "RUN":
            self.lbcheck.config(state=DISABLED)
            self.lbnr.set(0)
            self.nreventcheck.config(state=DISABLED)
            self.nrevent.set(0)
            self.fsizecheck.config(state=DISABLED)
            self.fsize.set(0)
        elif self.v.get() == "LUMIBLOCK":
            self.lbcheck.config(state=NORMAL)
            self.nreventcheck.config(state=DISABLED)
            self.nrevent.set(0)
            self.fsizecheck.config(state=DISABLED)
            self.fsize.set(0)
        else:
            self.lbcheck.config(state=NORMAL)
            self.nreventcheck.config(state=NORMAL)
            self.fsizecheck.config(state=NORMAL)

    def buildquery(self,title):
        args=[]
        keys={}
        sql="select "
        table="SFO_TZ_"+self.v.get()
        fields = []
        if self.sfo.get() == 1:
            fields.append("sfoid")
            title.append(("SFOID",10))
        if self.runnr.get() == 1:
            fields.append("runnr")
            title.append(("RUN #",10))
        if self.lbnr.get() == 1:
            fields.append("lumiblocknr")
            title.append(("LB #",10))
        if self.streamtype.get() == 1:
            fields.append("streamtype")
            title.append(("S. Type",15))
        if self.streamname.get() == 1:
            fields.append("stream")
            title.append(("S. Name",25))

        for i in fields: sql+="fir."+i+","

        countersel = []
        if self.v.get() == "FILE":
            countersel.append(("count(LFN)","",""))
            countersel.append(("count(LFN)","FILESTATE","OPENED"))
            countersel.append(("count(LFN)","FILESTATE","CLOSED"))
            countersel.append(("count(LFN)","TRANSFERSTATE","TRANSFERRED"))
            titlevar = "Files"
        else:
            countersel.append(("count(STATE)","",""))
            countersel.append(("count(STATE)","STATE","OPENED"))
            countersel.append(("count(STATE)","STATE","CLOSED"))
            countersel.append(("count(STATE)","STATE","TRANSFERRED"))
            titlevar = "Streams"

        title.append(("Total "+titlevar,10))
        title.append(("Opened "+titlevar,10))
        title.append(("Closed "+titlevar,10))
        title.append(("Transferred "+titlevar,10))


        notcountersel = []
        if self.nrevent.get() == 1:
            notcountersel.append(("sum(nrevents)","FILESTATE","OPENED"))
            title.append(("#Event",10))

        if self.fsize.get() == 1:
            notcountersel.append(("to_char(sum(filesize)/(1024*1024*1024),9999999999.999)","FILESTATE","OPENED"))
            title.append(("Size (GB)",10))

        for i in range(0,len(countersel)):
            sql+=" (select "+countersel[i][0]+" "
            sql+=" from " + table
            sql+=" where "
            if countersel[i][1]!="":
                sql+=countersel[i][1]+" = '"+countersel[i][2]+"' and "

            for k in fields:
                sql+=" fir."+k+" = "+k+" and "
            sql=sql[:len(sql)-4]
            sql+="),"

        for i in range(0,len(notcountersel)):
            sql+=" (select "+notcountersel[i][0]+" "
            sql+=" from " + table
            sql+=" where "
            if notcountersel[i][1]!="":
                sql+=notcountersel[i][1]+" <> '"+notcountersel[i][2]+"' and "

            for k in fields:
                sql+=" fir."+k+" = "+k+" and "
            sql=sql[:len(sql)-4]
            sql+="),"


        sql = sql[:len(sql)-1]

        sql+=" from "+table+ " fir"
        cond = 0
        if self.nop1.get() == 0:
            sql+=" where fir.runnr < 1200000000 "
            cond = 1

        if self.maxrun.get()!="":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            sql+=" fir.runnr < " + self.maxrun.get() + " "
            cond = 1

        if self.minrun.get()!="":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            sql+=" fir.runnr >= " + self.minrun.get() + " "
            cond = 1

        if self.maxlb.get()!="" and table!="RUN":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            sql+=" fir.lumiblocknr < " + self.maxlb.get() + " "
            cond = 1

        if self.minlb.get()!="" and table!="RUN":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            sql+=" fir.lumiblocknr >= " + self.minlb.get() + " "
            cond = 1

        if self.mindate.get()!="":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            cond = 1
            pieces=self.mindate.get().split('/')
            sql+=" fir.t_stamp > :pmindate "
            keys['pmindate']=datetime.datetime(int(pieces[2]),
                                               int(pieces[1]),
                                               int(pieces[0]),0,0,0)


        if self.maxdate.get()!="":
            if cond == 0:
                sql+=" where "
            else:
                sql+=" and "
            cond = 1
            pieces=self.maxdate.get().split('/')
            sql+=" fir.t_stamp < :pmaxdate "
            keys['pmaxdate']=datetime.datetime(int(pieces[2]),
                                               int(pieces[1]),
                                               int(pieces[0]),0,0,0)


        sql+=" group by "
        for i in fields: sql+="fir."+i+","
        sql = sql[:len(sql)-1]

        sql+=" order by "
        for i in fields:
            if i!="sfoid":
                sql+="fir."+i+" desc,"

        if not (len(fields)==1 and fields[0]=="sfoid"):
            sql = sql[:len(sql)-1]
        else:
            sql = sql[:len(sql)-9]

        args.insert(0,sql)

        #print args
        #print keys

        return args,keys

    def query(self, title, rows):
        curs = self.orcl.cursor()
        args,keys=self.buildquery(title)
        func=getattr(cx_Oracle.Cursor,'execute')
        args.insert(0,curs)
        func(*args, **keys)
        rows.extend(curs.fetchall())
        curs.close()
        self.orcl.commit()


root = Tk()
root.title("SFOTZ Interface")
app = App(root)

root.mainloop()
