#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import str
from builtins import range
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from time import gmtime, strftime
from sfodb.sfodb import SFOFetch
import logging
from os import getpid
from optparse import OptionParser

class MainWindow(QMainWindow):

    def __init__(self,db,parent=None):
        super(MainWindow,self).__init__(parent)
        self.setWindowTitle("SFO-DISPLAY")
        self.db=db
        self.resize(800,500)
        #self update timer
        timer = QTimer(self)
        timer.setInterval(30000)
        timer.start()
        #menubar coniguration
        exitMenu=self.menuBar().addMenu("&Quit")
        exitAction=self.createAction("&Quit",self.close,
                                     "Ctrl+Q","filequit",
                                     "Close the application")
        exitMenu.addAction(exitAction)
        def toggle_log():
            """helper function to toggle the display of the log area"""
            if self.logArea.isVisible():
                self.logArea.setVisible(False)
            else:
                self.logArea.setVisible(True)
                logFile=open(LOG_FILE).read()
                self.logArea.setPlainText(logFile)

        viewMenu=self.menuBar().addMenu("V&iew")
        viewAction=self.createAction(text = "&Log",slot = toggle_log,
                                     shortcut = "Ctrl+L",tip="show the log",
                                     checkable=True)
        viewMenu.addAction(viewAction)

        def modify_search():
            #QInputDialog.getText(self,'Modify Search String',"SFO name form:")
            dialog = DBDialog()
            dialog.exec_()
            self.search_string = dialog.search_string
        #setting a default search string:
        self.search_string = """(SFOID like 'SFO%' or SFOID like 'MUCal%')"""

        searchEditMenu = self.menuBar().addMenu("App String")
        modifySearchAction=self.createAction(text="Modify App String",
                                            slot=modify_search)
        searchEditMenu.addAction(modifySearchAction)


        #toolbar configuration

        #this set of commands creates the search text box and buttons
        self.searchButton = QPushButton("Search")
        self.clearButton = QPushButton("Clear")
        self.searchBox = QLineEdit()
        self.searchBox.setInputMask("900000")
        self.searchBox.clear()
        self.searchBox.setMaximumSize(60,20)
        self.searchGroup=QGroupBox()
        self.searchGroup.setMaximumSize(QSize(200,70))
        self.numberCombo = QComboBox()
        self.searchGroup.setTitle("Sea&rch run number")
        self.searchGroupLayout=QHBoxLayout()
        self.searchGroupLayout.addWidget(self.searchBox)
        self.searchGroupLayout.addWidget(self.searchButton)
        self.searchButton.setEnabled(False)
        self.searchGroupLayout.addWidget(self.clearButton)
        self.searchGroup.setLayout(self.searchGroupLayout)
        self.searchBox.textChanged.connect(self.toggle_search_button)

        self.searchArea = self.addToolBar('searchArea')

        #this set of commands creates the history depth combo box and label
        self.numberCombo.addItems(['5','10','15'])
        self.dispLabel = QLabel("""<html><u>H</u>istory: </html>""")
        self.searchShortcut=QShortcut("Ctrl+R",self.searchBox,
                                      self.searchBox.setFocus)
        self.dispShortcut=QShortcut("Ctrl+H",self.numberCombo,
                                    self.numberCombo.setFocus)

        #this configures the online/offline db options
        self.connectRadioBox = QGroupBox()
        connectRadioLayout = QVBoxLayout()
        self.connectRadioBox.setTitle("DB")
        self.radiooff = QRadioButton("offline")
        self.radiooff.setChecked(True)
        self.radiooff.setEnabled(False)
        self.radioon =  QRadioButton("online")
        self.radioon.setEnabled(False)
        connectRadioLayout.addWidget(self.radiooff)
        connectRadioLayout.addWidget(self.radioon)
        self.radiooff.setChecked(True)
        self.connectRadioBox.setLayout(connectRadioLayout)




        self.searchArea.addWidget(self.searchGroup)
        self.searchArea.addWidget(self.dispLabel)
        self.searchArea.addWidget(self.numberCombo)
        self.searchArea.addWidget(self.connectRadioBox)

        #configure Bars area (main data bar area)
        self.barsArea  = BarsArea(int(self.numberCombo.currentText()),self.db,self.search_string)
        self.barsArea.setFrameShape(2)
        #configure run details area
        self.infoArea = InfoArea(self.db)
        self.infoArea.setObjectName("infoFrame")


        self.infoArea.setAlignment(Qt.AlignVCenter|Qt.AlignHCenter)
        self.infoArea.setFrameShape(1)


        #configure the log area and logging details
        def update_log():
            """helper function to periodically updated the log"""
            logFile=open(LOG_FILE).read()
            self.logArea.setPlainText(logFile)
            #scrollbar to max
            sb=self.logArea.verticalScrollBar()
            sb.setValue(sb.maximum())


        self.logArea = QTextEdit("Log")
        self.logArea.setObjectName("logArea")
        self.logArea.setAlignment(Qt.AlignVCenter|Qt.AlignHCenter)
        self.logArea.setFrameShape(1)
        self.logArea.setVisible(False)
        timer.timeout.connect(update_log)
        self.mainWidget = QSplitter()
        self.mainWidget.setOrientation(Qt.Vertical)
        self.mainWidget.setChildrenCollapsible(False)
        self.infoSplitter = QSplitter()
        self.infoSplitter.setOrientation(Qt.Horizontal)
        self.infoSplitter.setChildrenCollapsible(False)
        self.infoSplitter.addWidget(self.barsArea)
        self.infoSplitter.addWidget(self.infoArea)
        self.mainWidget.addWidget(self.infoSplitter)
        self.mainWidget.addWidget(self.logArea)

        self.numberCombo.currentIndexChanged.connect(self.updateBarsArea)

        #configure the status bar
        self.statusBar = self.statusBar()

        legend = QGroupBox()
        legend.setTitle("Legend")
        legend.setAlignment(Qt.AlignRight)
        legendLayout = QHBoxLayout()
        buttonopen=QPushButton("opened")
        buttonclosed=QPushButton("closed")
        buttontrans=QPushButton("transferred")
        buttonopen.setObjectName("opened")
        buttonclosed.setObjectName("closed")
        buttontrans.setObjectName("transferred")

        legendLayout.addWidget(buttonopen)
        legendLayout.addWidget(buttonclosed)
        legendLayout.addWidget(buttontrans)
        legend.setLayout(legendLayout)
        self.statusBar.addPermanentWidget(legend)


        self.setCentralWidget(self.mainWidget)
        #self.setFixedHeight(700)
    def keyPressEvent(self,event):
        if event.key() ==Qt.Key_Escape:
            self.clearButton.click()
    def toggle_search_button(self):
        if str(self.searchBox.text())=="":
            self.searchButton.setEnabled(False)
        else:
            self.searchButton.setEnabled(True)

    def update_info_area(self,runnr):
        try:
            infoArea=InfoArea(self.db,runnr)
            infoArea.setObjectName("infoFrame")
            infoArea.setFrameShape(1)
            self.mainWidget.widget(0).widget(1).setParent(None)
            self.mainWidget.widget(0).insertWidget(1,infoArea)
        except:
            logging.error("oops! the info area could not be created :" + str(sys.exc_info()))
            raise


    def updateBarsArea(self):


        currentindex = int(self.numberCombo.currentText())
        try:
            barsarea=BarsArea(currentindex,self.db,self.search_string)
            self.mainWidget.widget(0).widget(0).setParent(None)
            self.mainWidget.widget(0).insertWidget(0,barsarea)

            logging.info("Bars Area updated")
            self.statusBar.showMessage("Updated: " +
                                       strftime("%a, %d %b %y %H:%M:%S",gmtime()))
        except:
            logging.error("oops! the bars area could not be updated")
            raise

    def return_run(self):
        """This function gets the run number from the text area and returns
        a Bar object with that run's data"""
        try:
            datagetter = SFOFetch(arraysize=1,db=self.db)
            run=int(self.searchBox.displayText())
            open_files=datagetter.nr_opened_files([run])
            closed_files=datagetter.nr_closed_files([run])
            trans_files=datagetter.nr_transferred_files([run])
        except TimoutException:
            return 0
        except (RuntimeError,NameError):
            raise

        try:
            ratio=100.*trans_files[0]/closed_files[0]
        except ZeroDivisionError:
            ratio=0

        bar=Bar(label=str(run),value=ratio)
        bar.progBar.setFormat( ("%1.2f" % (ratio))+"%")
        if open_files[0]!=0:
            bar.infoButton.setObjectName("opened")
        elif closed_files==trans_files:
            bar.infoButton.setObjectName("transferred")
        else:
            bar.infoButton.setObjectName("closed")
        return bar

    def display_run(self):
        """this method takes a run bar and displays it alone on
        the bar area"""
        try:
            run = self.return_run()
            run.setObjectName("infoFrame")
            run.setFrameShape(1)

            self.mainWidget.widget(0).widget(0).setParent(None)
            self.mainWidget.widget(0).insertWidget(0,run)
        except (RuntimeError,NameError):
            raise
        except:
            logging.error("oops! the run could not be found or displayed.")
            raise

    def createAction(self, text,slot=None,shortcut=None,icon=None,tip=None,
                    checkable=False,signal="triggered"):
        """ a helper function that supplies a template
        for creating QT actions"""
        action=QAction(text,self)
        if icon is not None:
            action.setIcon(QIcon("/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            getattr(action, signal).connect(slot)
        if checkable:
            action.setCheckable(True)
        return action

class BarsArea(QScrollArea):
    """this class represents the bar area. it has a method that is in charge
    of updating itself periodically"""
    def __init__(self,numberOfBars,db,search_string,parent=None):
        super(BarsArea,self).__init__(parent)
        self.db=db
        self.search_string=search_string
        self.setWidgetResizable(False)
        self.setObjectName("infoFrame")
        layout = QVBoxLayout()
        self.numberOfBars=numberOfBars
        self.bars= [Bar(str(i)) for i in range(numberOfBars)]
        for bar in self.bars:
            layout.addWidget(bar)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setFrameShape(1)
        frame=QFrame()
        frame.setLayout(layout)
        self.setWidget(frame)
        self.setWidgetResizable(True)
        self.datagetter = SFOFetch(arraysize=self.numberOfBars,db=self.db)
        timer = QTimer(self)
        timer.setInterval(30000)
        timer.start()
        timer.timeout.connect(self.update)

        self.update()


    def update(self):

        runs = self.datagetter.valid_runs(parameterdict={'minrunnr':1},sfo_search_string=self.search_string)
        open_files = self.datagetter.nr_opened_files(runs)
        closed_files = self.datagetter.nr_closed_files(runs)
        trans_files= self.datagetter.nr_transferred_files(runs)

        for run in runs:
            index=runs.index(run)
            open,closed,trans = (open_files[index],closed_files[index],
                                trans_files[index])
            try:
                ratio=100.*trans/closed

            except ZeroDivisionError:
                ratio=0
            self.bars[index].label.setText(str(run))

            if ratio<1:
                self.bars[index].infoButton.setEnabled(False)
            else:
                self.bars[index].infoButton.setEnabled(True)
            self.bars[index].progBar.setValue(int(ratio))
            self.bars[index].progBar.setFormat("%1.2f" % (ratio)+'%')
            if open!=0:
                self.bars[index].infoButton.setObjectName("opened")
            elif trans==closed:
                self.bars[index].infoButton.setObjectName("transferred")
            else:
                self.bars[index].infoButton.setObjectName("closed")
        logging.info("Bars area updated")

class InfoArea(QTextEdit):
    """This class represents the info area that is generated when the "i"
    button is clicked. It provides methods for generating an HTML table
    with the needed data"""
    def __init__(self,db,runnr=0,parent=None):
        super(InfoArea,self).__init__(parent)
        self.db=db
        self.runnr=runnr
        self.setReadOnly(True)
        tabletimer = QTimer(self)
        interval=10000
        tabletimer.setInterval(interval)
        tabletimer.start()

        self.infogetter=SFOFetch(arraysize=100,db=self.db)
        tabletimer.timeout.connect(self.make_table)

        if self.runnr==0:
            self.setText("""<html><h1>Info Area </h1>
            <p> more information available only when more than 1% has been transfered</p><html>""")
        else:
            info=self.make_table()
            self.setText(str(info))

    def add_table_row(self,SFO,info):
        op=info[0]
        cl=info[1]
        tr=info[2]
        upratio=info[3]*100
        delratio=info[4]*100
        truncated=info[5]
        if op!=0:
            rowcolor="blue" #open
        elif tr==cl:
            rowcolor="green" #transferred
        else:
            rowcolor="yellow" #closed

        #as the variable named "cl" is in fact not the closed number of files, but the
        #non open number of files, i subtract the transferred number from cl to get
        #the actual number of closed files.

        row = ("<tr> <td bgcolor =" +rowcolor+ ">" +
               """<td align="center">""" + str(SFO) + "</td>" +
               """<td align = "center">""" + "%d%%" % upratio + "</td>" +
               """<td align = "center">""" + "%d%%" % delratio + "</td>" +
               """<td align = "center">""" + "%d" % truncated + "</td>"+
               """<td align = "center">""" + "{0}/{1}/{2}".format(op,
               cl-tr,tr) +"</td></tr>")

        return row

    #helper function to display human readable file sizes
    def sizeof_fmt(self,num):
        for x in ['bytes','KB','MB','GB','TB']:
            if num < 1000.0:
                return "%3.1f%s" % (num, x)
            num /= 1000.0

    def make_table(self):

        """This function generates the run info table that is displayed in the info area"""
        if self.runnr==0:
            return
        info = self.infogetter.get_run_details(self.runnr)
        table = ("""<html><h2 align ="center">"""+str(self.runnr) +
                """</h2><table border="1" align="center">
                <tr>
                <th>Status</th>
                <th>Application</th>
                <th>Uploaded</th>
                <th>Deleted</th>
                <th>Truncated</th>
                <th> Open/Closed/Transferred <br> files</th>
                </tr>
                """)
        #just sorting the keys so we get sorted results
        k=list(info.keys())
        k.sort()
        for key in k:
            table+=(self.add_table_row(key,info[key]))
        table+=("</table>")

        self.totals=self.infogetter.get_run_data(self.runnr)
        totalop=self.totals[0]
        totalcl=(self.totals[1]-self.totals[2])
        totaltr=self.totals[2]
        creationtimes=self.totals[3]
        totalsize=self.totals[4]
        #get the earliest creation time and remove anything beyong seconds
        if creationtimes!=[]:
            creationtime=str(creationtimes[0])
        else:
            creationtime=''

        creationtime=creationtime.partition('.')[0]


        table+=("<h2>Additional run details</h2>" +
               "<p> Total # open files:{0}, total # closed files:{1}, total # transferred files: {2}".format(totalop,totalcl,totaltr)
               +"</p>"+"<p> Total transferred volume: " + self.sizeof_fmt(totalsize)+"</p>"
               +"<p> Run Created on:" + creationtime + "</p>"+
               "</html>")

        logging.info("Info table updated")
        return table



class Bar(QFrame):
    """This class represents a single data bar with a text label,
    an info button and a status indicator"""
    def __init__(self,label="None",value=0,parent=None):


        super(Bar,self).__init__(parent)
        self.infoButton = QPushButton("i")
        self.progBar = QProgressBar()
        if value<1:
            self.infoButton.setEnabled(False)
        else:
            self.infoButton.setEnabled(True)
        self.progBar.setValue(value)
        self.setContentsMargins(0,0,0,0)
        self.barLayout = QHBoxLayout()
        self.label = QLabel(label)
        self.barLayout.addWidget(self.label)
        self.barLayout.addWidget(self.progBar)
        self.barLayout.addWidget(self.infoButton)
        self.setLayout(self.barLayout)
        self.setStyleSheet(stylesheet)
        self.infoButton.clicked.connect(self.getInfo)


    def getInfo(self):
        main.update_info_area(int(self.label.text()))

class DBDialog(QDialog):
    """this class represents the search application name selection dialog"""

    def __init__(self,parent=None):
        super(DBDialog,self).__init__(parent)
        self.setWindowTitle("Application name selection")
        self.nameComboBox=QComboBox()
        self.nameComboBox.addItem("SFO%")
        self.nameComboBox2=QComboBox()
        self.nameComboBox2.addItem("MUCal%")
        self.selectionComboBox=QComboBox()
        self.selectionComboBox.addItem("OR")
        likeLabel=QLabel("like")
        okButton=QPushButton("&Ok")
        okButton.setDefault(True)

        cancelButton=QPushButton("&Cancel",)
        QObject.connect(okButton, SIGNAL('clicked()'), self.make_search_string)
        QObject.connect(cancelButton, SIGNAL('clicked()'), self.close)

        layout=QHBoxLayout()
        layout.addWidget(likeLabel)
        layout.addWidget(self.nameComboBox)
        layout.addWidget(self.selectionComboBox)
        layout.addWidget(self.nameComboBox2)
        layout.addWidget(okButton)
        layout.addWidget(cancelButton)
        self.setLayout(layout)

    def make_search_string(self):
        self.search_string=("(SFOID like '" + self.nameComboBox.currentText() +"' "+
                             self.selectionComboBox.currentText() + " SFOID like '"
                             +self.nameComboBox2.currentText() +"')")
        logging.info("Application name format search string changed to: "+
            self.search_string)
        self.close()


    def closeEvent(self,event):
        return self.make_search_string()


stylesheet="""QProgressBar:horizontal {
                  border: 1px solid gray;
                  border-radius: 3px;
                  background: white;
                  padding: 1px;
                  text-align: right;
                  margin-right: 8ex;
                  }
                  QProgressBar::chunk:horizontal{
                  background-color: #3861aa;
                  width: 10px;
                  margin: 1px;
                  }
                  QPushButton {
                  border-width: 2px;
                  border-radius: 3px;
                  min-width: 2em;
                  border: 1px solid gray;
                  font: 12px;
                  }
                  QPushButton:hover{
                  border: 1px solid white;
                  }
                  QPushButton#opened{
                  background-color: blue;
                  }
                  QPushButton#closed{
                  background-color: yellow;
                  }

                  QPushButton#transferred{
                  background-color: green;
                  }
                  td{
                    align: center;
                  }
                  QFrame#infoFrame{
                  background-color: white;
                  }
                  QLabel#logArea{
                      background-color: white;
                  }
                  """



if __name__=='__main__':
    #setting up command line options
    parser=OptionParser()
    parser.add_option('-d', '--database',
                      dest = 'db', default = 'offline',
                      help='Database: online, offline. Default: [%default]')
    (options,args)=parser.parse_args()
    db = (options.db)
   #starting PyQt
    app=QApplication(sys.argv)
    app.setStyleSheet(stylesheet)
    LOG_FILE='/tmp/sfo-db-dispaly_' + str(getpid()) + '.out'
    #testing try catch block: LOG_FILE='/afs/cern.ch/user/n/ngarelli/private/log.log'
    try:
        logging.basicConfig(format = '%(asctime)s %(message)s',
                           filename=LOG_FILE,
                            filemode='w',
                            level=logging.DEBUG,
                            datefmt="%d/%m/%y %H:%M:%S")
    except IOError:
        logging.basicConfig(format = '%(asctime)s %(message)s',
                            stream=sys.stdout,
                            level=logging.DEBUG,
                            datefmt="%d/%m/%y %H:%M:%S")

    logging.info("application started")


    #by default, we use the offline db settings
    try:
        main=MainWindow(db)
        if db=='online':
            main.radioon.setChecked(True)
            main.radiooff.setChecked(False)
        else:
            main.radioon.setChecked(False)
            main.radiooff.setChecked(True)
        
        main.numberCombo.currentIndexChanged.connect(main.barsArea.update)
        main.clearButton.clicked.connect(main.updateBarsArea)

        main.searchButton.clicked.connect(main.display_run)
        main.searchBox.returnPressed.connect(main.display_run)
        main.show()
        app.exec_()
    except (RuntimeError,NameError) as e:

        logging.error('problems with the db,' + str(e))
        print('FATAL ERROR: problems with the db,', e)
    #except:
        #print "unexpected error:", sys.exc_info()[0]
