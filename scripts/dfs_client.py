#!/usr/bin/env tdaq_python
from __future__ import division
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import range
from builtins import object
from past.utils import old_div
import random
import tkinter
import tkinter.font
import sys
import time
import re
import getopt
import os
import tkinter.messagebox
from collections import deque
from eformat import helper
from ispy import IPCPartition, Summer, InfoReader, ToolTip, MultiInfoReader
from ispy import ISInfoIterator, ISInfoAny, ISCriteria, ISServerIterator
from ipc  import OWLTime
import pm.project


# Global variables, defaults
verbose     = False
fontsize    =    12
isupdate    =     7    # sec  
isslowupdate=   100    # sec
graphupdate =  2000    # ms
limitSize   = 16000    # kB
part_name   = 'ATLAS'
blkSizes    = True
blkToolTips = True
blkDF       = True
blkRC       = True
blkIni      = True


# ToDo
# - Update DF variables only if status > BOOTED (or LB>0)

def usage():
  print('Graphical DAQ monitor based on ispy')
  print('Usage: ', sys.argv[0], "[-h] [-p Partition] [-f FontSize] [-u sec] [-g ms]")
  print('                        [-s kb] [-v] [-STHBDXCZ]')
  print('    -u  IS update interval in sec (def=5)')
  print('    -w  IS update interval in sec for cpu consuming servers (def=100s)')
  print('    -g  graphical update interval in ms')
  print('    -s  Upper size limit in kb (def=16000)')
  print('    -v  Verbose')
  print('    -S  Disable event size block')
  print('    -T  Disable tool tips block')
  print('    -D  Disable DF block')
  print('    -R  Disable RC block')
  print('    -I  Disable initial partition info sub-block')
  print('    -X  Disable reading of RunCtrl IS server')
  print('    -C  Disable COOL DB checking')
  sys.exit(1)


# Extend the Summer class providing a dump to string
class Xummer(Summer):
  def summary(self):
    s = [ "%s(%d)" % (self.prefix, len(self.objects))]
    for attr in self.attributes:
      t = " sum.%s.%s" % (self.prefix, attr)
      s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
    s.append("\n")
    for attr in self.averages:
      t = " avg.%s.%s" % (self.prefix, attr)
      s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
    return ''.join(s)


# Class Worker ---------------
class Worker(object):
  """Class gathering info from IS"""

  # Configuration
  _nbrSfis    =   100      # EB dimension
  _limitL1Rate= 75000. 
  _limitSize  = 16000      # Ev size upper limit in kB
  _limitTime  = 72000.     # 20 h
  _limitros   = 25000      # Hz
  _grDelay    =  2000      # Graph update delay in ms
  _isDelay    =     5      # Is update delay in s
  _isDelayFact=    20      # Is update delay multiplication factor for slow servers

  _dfsummary  = ISInfoAny(); 
  _rc         = ISInfoAny()         
  _rp         = ISInfoAny()    
  _lb         = ISInfoAny()         
  _gb         = ISInfoAny()    
  _roib       = ISInfoAny()         
  _efdsum     = ISInfoAny()
  _ptsum      = ISInfoAny()         
  _l2svsum    = ISInfoAny()       
  _l2rhsum    = ISInfoAny()     
  _sfisum     = ISInfoAny()       
  _sfosum     = ISInfoAny()    

#DB
  _conf_sfi   = None                
  _conf_dfm   = None           
  _conf_l2sv  = None                
  _conf_l2rh  = None 

  _qlog       = deque()
  _1stLoop    = True
  _verbose    = False;              
  _counter    = 0;                   
  _trigkeys   = []
  _oldtime    = time.time()-(_isDelay+10)

  def readRDB(self):
    print('--')
    print("Reading RDB ....")
    t0 = time.time()
    try: # Try to access the DB
      db = pm.project.Project('rdbconfig:RDB@%s' % (part_name))
    except UserWarning:
      print("Cannot open DB for partition '%s'" % (part_name))
      sys.exit(1)
   
    self._conf_sfi = db.getObject('SFIConfiguration')[0]
    self._conf_dfm = db.getObject('DFMConfiguration')[0]
    self._conf_l2sv= db.getObject('L2SVConfiguration')[0]
    self._conf_l2rh= db.getObject('L2RHConfiguration')[0]
    t1 = time.time()

    print('SFI input buffer : %d' % (self._conf_sfi.MaxAssignEvents))
    print('SFI output buffer: %d' % (self._conf_sfi.MaxOutputQueueSize))
    print('DFM input buffer : %d' % (self._conf_dfm.MaxInputQueueSize))
    print('L2SV l2 queue sz : %d' % (self._conf_l2sv.l2puQueueSize))
    print('L2RH store Size  : %d' % (self._conf_l2rh.storeSize))
    print('Reading RDB done [%4.2f s] ' % (t1-t0))
    print('--')
    

  #
  def __init__(self, part_name, initp = False):
    self._initp = initp
    self._p       = IPCPartition(part_name) # Create partition object
    self._pi      = IPCPartition('initial')

    self.readRDB()

    # XUMMER Data members
#    self._sfo  = Xummer('SFO', self._p, [ 'DF' ], 'SFO-[0-9]+', 
#                    [ 'CurrentDataReceivedRate','CurrentDataSavedRate',
#                      'CurrentEventReceivedRate','CurrentEventSavedRate', 'EventsReceived', 'EventsSaved' ])
#    self._l2sv = Xummer('L2SV', self._p, [ 'DF-L2SV.*' ], 'L2SV-[0-9]+', 
#                    [ 'IntervalEventRate', 'AcceptedEvents',
#                      'RejectedEvents', 'ForcedAccepts', 'LVL1_events', 'LVL2_events', 'AvgEventRate', 'errors' ])
#    self._l2rh = Xummer('L2RH', self._p, [ 'DF' ], 'LVL2ResultHandler-[0-9]+.L2RH',
#                    [ 'LVL2Requests' , 'EBRequests', 'Cleared events', 'MissingLVL2Results', 'duplicate_L1IDs', 
#                      'ResultStoreUsed', 'max_store_size', 'EBIntervalAccessRate', 'EBDataRate' ] )
    self._dfm  = Xummer('DFM',  self._p, [ 'DF' ], 'DFM-[0-9]+',
                    [ 'Rate of built events' , 'cleared events', 'LVL2 accepts',  'assigned events', 'built events', 
                      'number of XOFF', 'current XOFF', 'input queue', 'queue occupancy', 'deadtime'])

  #
  def qlog(self, msg):
    if len(self._qlog) > 20: self._qlog.popleft()
    self._qlog.append(msg)

  # To read the .RootController
  def updateRC(self):
    # if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
      if self._1stLoop: 
        self._1stLoop = False
      y  = ISInfoAny()
      it = ISInfoIterator(self._p, 'RunCtrl'); next(it)
      while next(it):
        if it.name().startswith('RunCtrl.RootController'):
          it.value(self._rc); self._rc._update(it.name(), self._p)
          break

  def formatKeys(self, name, value, comment):
    s = "%-20s: %8d   %s" % ( name.replace('RunParams.',''), value, comment)
    return s

  #
  def updateRP(self):
    self._trigkeys = []
    it = ISInfoIterator(self._p, 'RunParams')
    next(it)
    x = ISInfoAny()
    while next(it):
      if it.name().startswith('RunParams.RunParams'):
        it.value(self._rp); self._rp._update(it.name(), self._p)
      if it.name().startswith('RunParams.LumiBlock'):
        it.value(self._lb); self._lb._update(it.name(), self._p)
      if it.name().startswith('RunParams.GlobalBusy'):
        it.value(self._gb); self._gb._update(it.name(), self._p)
      
      if it.name().startswith('RunParams.TrigConfSmKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.SuperMasterKey, x.SuperMasterComment))
      if it.name().startswith('RunParams.TrigConfL1BgKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.L1BunchGroupKey, x.L1BunchGroupComment))
      if it.name().startswith('RunParams.TrigConfL1PsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.L1PrescaleKey, x.L1PrescaleComment))
      if it.name().startswith('RunParams.TrigConfHltPsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.HltPrescaleKey, x.HltPrescaleComment))
      if it.name().startswith('RunParams.Physics.L1PsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.L1PrescaleKey, x.L1PrescaleComment))
      if it.name().startswith('RunParams.Physics.HltPsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.HltPrescaleKey, x.HltPrescaleComment))
      if it.name().startswith('RunParams.Standby.L1PsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.L1PrescaleKey, x.L1PrescaleComment))
      if it.name().startswith('RunParams.Standby.HltPsKey'):
        it.value(x); x._update(it.name(), self._p);   
        self._trigkeys.append( self.formatKeys(it.name(), x.HltPrescaleKey, x.HltPrescaleComment))
    self._trigkeys.sort() 

  #
  def updateSums(self):
    self._dfm.update()

  # DFSummary and ROIB-1
  def updateDF(self):
    x = ISInfoAny()
    it = ISInfoIterator(self._p, 'DF')
    next(it)
    doneDFSummary = doneROIB = False
    while next(it):
      if it.name().startswith('DF.DFSummary') and not doneDFSummary:
        it.value(self._dfsummary); self._dfsummary._update(it.name(), self._p)
        #print '>>>', self._dfsummary.RunOverview 
        doneDFSummary = True
      if it.name().startswith('DF.ROIB-1') and not doneROIB:
        it.value(self._roib); self._roib._update(it.name(), self._p)
        self._roibOut = re.split("\n", str(self._roib))[7]
        doneROIB = True

  #
  def updateMonaIsa(self):
    server = 'RunCtrlStatistics'
    it = ISInfoIterator(self._p, server)
    while next(it):
      if it.name().startswith(server + '.EFD-SUM'):
        it.value(self._efdsum);  self._efdsum._update(it.name(), self._p)
      if it.name().startswith(server + '.PT-SUM'):
        it.value(self._ptsum);   self._ptsum._update(it.name(), self._p)
      if it.name().startswith(server + '.L2SV-SUM'):
        it.value(self._l2svsum); self._l2svsum._update(it.name(), self._p)
      if it.name().startswith(server + '.L2RH-SUM'):
        it.value(self._l2rhsum); self._l2rhsum._update(it.name(), self._p)
      if it.name().startswith(server + '.SFI-SUM'):
        it.value(self._sfisum); self._sfisum._update(it.name(), self._p)
      if it.name().startswith(server + '.SFO-SUM'):
        it.value(self._sfosum); self._sfosum._update(it.name(), self._p)


  # Read from IS 
  def Update(self):
    """Update ISInfo objects"""
    now  = time.time()
    diff = now - self._oldtime
    if diff > self._isDelay: # Update every _isDelay seconds
      self._oldtime  = now
      self._counter += 1
       
      # Zombie collector
      if self._counter % 20 == 0:
        try: os.waitpid(0, os.WNOHANG)
        except(OSError): pass
        # print 'Zombie collector 

      try:
        tt=[time.time()]
 
        # 1. Update RunCtrl
        self.updateRC()
        tt.append(time.time())

        # 1.5 If not running skip the updates
        if self._rc.state == 'INITIAL' or self._rc.state == 'NONE':
          if not self._1stLoop:
            if self._verbose: print('Skipping update')
            return 

        # 2. Update RunParams.RunParams
        self.updateRP()
        tt.append(time.time())
    
        # 3. Update summers: DFM
        self.updateSums()
        tt.append(time.time())

        # 4. Update DF: DF-Summary and ROIB 
        self.updateDF()
        tt.append(time.time())
   
        # 5. Update from MonaIsa (SUMS)
        self.updateMonaIsa()
        tt.append(time.time())

      # End of try
      except(UserWarning): 'Caught UserWarning'
      t = time.time()
      s = ''
      for i in range(1, len(tt)): 
        s += '%3.2f ' % (tt[i]-tt[i-1])
      o = "[%5d]: %4.2f s (%s)" % (self._counter, t-tt[0] , s)  
      self.qlog(o+'\n')
      if self._verbose:
        print(o)

      
  # --- ToolTip msg methods ---------------
  def tt_qlog(self):
    o = [' Update  Total  ( RC   RP  SUM   DF  Mona )\n']
    for i in self._qlog: o.append(i)
    return ''.join(o)

  def tt_error(self):  return "Error: " + self._dfsummay.RCError
  def tt_roib(self):   return str(self._roib)
  def tt_rc(self):     return str(self._rc)
  def tt_rp(self):     return str(self._rp)
  def tt_gb(self):     return str(self._gb)
  def tt_l1ct(self):   return ''.join( self._dfsummary.BusyList )
  def tt_dfm(self):    return self._dfm.summary()
  def tt_l2rh(self):   return str(self._l2rhsum)
  def tt_sfi(self):    return str(self._sfisum)
  def tt_sfo(self):    return str(self._sfosum)
  def tt_l2sv(self):   return str(self._l2svsum)
  def tt_efd(self):    return str(self._efdsum)
  def tt_det(self):    return '\n'.join(self._dfsummary.DetectorsList)
  def tt_disk(self):   return '\n'.join(self._dfsummary.SFOdiskUsageList)
  
  def tt_mag(self):   #OK
    s = 'Toroid  : %d%s \nSolenoid: %3d%s' % ( 
           int(round( self._dfsummary.DCSSolenoid )), '%', 
           int(round( self._dfsummary.DCSToroid   )), '%') 
    return s        
  
  def tt_sizes(self):
    s = ['@SFI']
    s.append("  %-14s: %6d kB " % ("Physics    ", self._dfsummary.SFISizePhys ))
    s.append("  %-14s: %6d kB " % ("Express    ", self._dfsummary.SFISizeExp  ))
    s.append("  %-14s: %6d kB " % ("Calibration", self._dfsummary.SFISizeCalib))
    s.append("  %-14s: %6d kB " % ("Debug      ", self._dfsummary.SFISizeDebug))
    s.append("  %-14s: %6d kB " % ("Average    ", self._dfsummary.SFISizeAvg  ))
    s.append("\n@SFO (Received)")
    s.append("  %-14s: %6d kB " % ("Physics    ", self._dfsummary.SFORSizePhys ))
    s.append("  %-14s: %6d kB " % ("Express    ", self._dfsummary.SFORSizeExp  ))
    s.append("  %-14s: %6d kB " % ("Calibration", self._dfsummary.SFORSizeCalib))
    s.append("  %-14s: %6d kB " % ("Debug      ", self._dfsummary.SFORSizeDebug))
    s.append("  %-14s: %6d kB " % ("Average    ", self._dfsummary.SFOSizeAvg  ))
    s.append("\n@SFO (Storage)")
    s.append("  %-14s: %6d kB " % ("Physics    ", self._dfsummary.SFOSSizePhys ))
    s.append("  %-14s: %6d kB " % ("Express    ", self._dfsummary.SFOSSizeExp  ))
    s.append("  %-14s: %6d kB " % ("Calibration", self._dfsummary.SFOSSizeCalib))
    s.append("  %-14s: %6d kB " % ("Debug      ", self._dfsummary.SFOSSizeDebug))
    return '\n'.join(s)

  def tt_counters(self):
    l1 = getattr(self._dfm, 'cleared events')
    l2 = getattr(self._dfm, 'LVL2 accepts')
    eb = getattr(self._dfm, 'built events')
    s = []
    s.append('Level 1      = %9d\n' % l1 )
    s.append('Level 2      = %9d\n' % l2 )
    s.append('Event Builder= %9d\n' % eb )
    s.append('Event Filter = %9d\n' % self._sfosum.EventsReceived )
    s.append('    rejected = %9d\n' % getattr(self._ptsum, 'EF Rejects'))
    s.append('Recorded     = %9d\n' % self._sfosum.EventsSaved )
#    s.append('------------\n')
#    s.append('There are %d events inside the DAQ system' % (l1-self._sfosum.EventsReceived) )
    return ''.join(s)

  def tt_streams(self):  return '\n'.join(self._dfsummary.Streams)

  def tt_ros(self):
    s = [ "Hot Robin : %5.1f %s   %s" % (self._dfsummary.HotRobinOcc, '%', self._dfsummary.HotRobinName),
          "Hot Ros   : %5d Hz  %s"    % (self._dfsummary.HotRosRate,       self._dfsummary.HotRosName) ,
          "Busy ROLs : %5d"           % (self._dfsummary.NumOfBusyROLs) 
        ]
    s.append("\n ToDo: add _busyRols list")
    return '\n'.join(s)

  def tt_top(self):
    s1 = ['Top mem [swap]']
    s2 = ['\nTop load 1m']
    return '\n'.join(s1+self._dfsummary.TopLoadMem +s2+self._dfsummary.TopLoadList )

  def tt_sv(self):
    s = ['         notUp   Out  Fault  Total\n']
    s.append('L2SVs: %6d %6d %6d %6d\n' % ( self._dfsummary.numL2SV_notup,
                                            self._dfsummary.numL2SV_out,
                                            self._dfsummary.numL2SV_fault,
                                            self._dfsummary.numL2SV) )
    s.append('L2PUs: %6d %6d %6d %6d\n' % ( self._dfsummary.numL2PU_notup,
                                            self._dfsummary.numL2PU_out,
                                            self._dfsummary.numL2PU_fault,
                                            self._dfsummary.numL2PU) )
    s.append('L2RHs: %6d %6d %6d %6d\n' % ( self._dfsummary.numL2RHs_notup,
                                            self._dfsummary.numL2RHs_out,
                                            self._dfsummary.numL2RHs_fault,
                                            self._dfsummary.numL2RHs) )
    s.append('SFIs : %6d %6d %6d %6d\n' % ( self._dfsummary.EBnumSFI_notup,
                                            self._dfsummary.EBnumSFI_out,
                                            self._dfsummary.EBnumSFI_fault,
                                            self._dfsummary.EBnumSFI) )
    s.append('EFDs : %6d %6d %6d %6d\n' % ( self._dfsummary.EFnumEFDs_notup,
                                            self._dfsummary.EFnumEFDs_out,
                                            self._dfsummary.EFnumEFDs_fault,
                                            self._dfsummary.EFnumEFDs) )
    s.append('PTs  : %6d %6d %6d %6d\n' % ( self._dfsummary.EFnumPTs_notup,
                                            self._dfsummary.EFnumPTs_out,
                                            self._dfsummary.EFnumPTs_fault,
                                            self._dfsummary.EFnumPTs) )
    s.append('SFOs : %6d %6d %6d %6d\n' % ( self._dfsummary.SFOnum_notup,
                                            self._dfsummary.SFOnum_out,
                                            self._dfsummary.SFOnum_fault,
                                            self._dfsummary.SFOnum) )
    return ''.join(s)

  def tt_keys(self): return '\n'.join(self._trigkeys) 

  # ------ Meter filling methods -----
  def f_run(self, meter): 
    self.Update()
    meter.set(old_div(self._dfsummary.RunTime,self._limitTime), self._dfsummary.RunOverview)
    meter.after(self._grDelay*5, lambda: self.f_run(meter))

  def f_rc(self, meter): 
    self.Update()
    t = ""
    if self._dfsummary.RCFault: t += ' Fault;'
    if self._dfsummary.RCBusy:  t += ' RCBusy;'
    s = "%s [%s]" % (self._rc.state, t)  # RC state not available in DFsummary IS class
    x = self._dfsummary.RCFault or self._dfsummary.RCBusy
    meter.set(x, s)
    meter.after(self._grDelay, lambda: self.f_rc(meter))
  
  def f_globalbusy(self, meter): 
    self.Update()
    v = self._dfsummary.RCGlobalBusy
    p = v/100.
    s = "Global: %d %s" % (int(v), '%')
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_globalbusy(meter))

  def f_daqbusy(self, meter):  
    self.Update()
    v = self._dfsummary.RCDAQBusy
    p = v/100.
    s = "Daq: %d %s" % (int(v), '%')
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_daqbusy(meter))
  
  def f_hlt(self, meter): 
    self.Update()
    meter.set( 1, self._dfsummary.HLTSummary ) 
    meter.after(self._grDelay, lambda: self.f_hlt(meter))

  def f_sfiRate(self, meter): 
    self.Update()
    b = self._dfsummary.EBsfiBW
    r = self._dfsummary.EBsfiRate
    try: p = old_div(b, self._dfsummary.EFmaxBW)
    except ZeroDivisionError: p=0
    s = "%5d MB/s %8.0f Hz" % (b , r)
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_sfiRate(meter))

  def f_sfiRateI(self, meter):  
    self.Update()
    s = "%5d MB/s %8.0f Hz" % ( self._dfsummary.EBsfiBW , self._dfsummary.EBsfiRate )
    meter.set( self._dfsummary.EBsfiInputBW, s)
    meter.after(self._grDelay, lambda: self.f_sfiRateI(meter))

  def f_sfiOccOut(self, meter):
    self.Update()
    v = self._dfsummary.EBsfiOutputQ
    p = self._dfsummary.EBsfiOutputOcc
    s = "%6.1f %s %10d #" % (p, '%', v)
    meter.set(p/100., s)
    meter.after(self._grDelay, lambda: self.f_sfiOccOut(meter))

  def f_sfiOccIn(self, meter): 
    self.Update()
    v = self._dfsummary.EBsfiInputQ
    p = self._dfsummary.EBsfiInputOcc
    s = "%6.1f %s %10d #" % (p, '%', v )
    meter.set(p/100., s)
    meter.after(self._grDelay, lambda: self.f_sfiOccIn(meter))

  def f_l2Occ(self, meter):  
    self.Update()
    v = self._dfsummary.L2EventsInside
    p = self._dfsummary.L2OccupPerc
    s = "%6.1f %s %10d #" % (p, '%', v )
    meter.set(p/100., s)
    meter.after(self._grDelay, lambda: self.f_l2Occ(meter))

  def f_l1Rate(self, meter):
    self.Update()
    v = self._dfsummary.L1OutRate
    meter.set( old_div(v,self._limitL1Rate)  , "%19.0f Hz" % (v) )
    meter.after(self._grDelay, lambda: self.f_l1Rate(meter))

  def f_ros2l2(self, meter): 
    self.Update()
    v = self._dfsummary.L2Ros2L2BW
    try: p = old_div(v,self._dfsummary.L2Ros2L2MaxBW)
    except (ZeroDivisionError): p = 0
    s = '%5d MB/s %11s' % (v,'')
    meter.set(p, s)
    meter.after(self._grDelay*10, lambda: self.f_ros2l2(meter))

  def f_l2rhOcc(self, meter): 
    self.Update()
    p = self._dfsummary.L2RHOcc
    v = self._dfsummary.L2RHEventsInside
    s = "%6s %s %10d #" % (' '  , ' ', v )
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_l2rhOcc(meter))

  def f_l2rhRate(self, meter): 
    self.Update()
    v = self._dfsummary.L2RHRate
    p = self._dfsummary.L2RHRatePerc
    s = "%5d MB/s %8.0f Hz" % (v , self._dfsummary.L2RHAccessRate )
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_l2rhRate(meter))

  def f_l2rhErr(self, meter): 
    self.Update()
    v = self._dfsummary.L2RHMisL2ResInterval
    t = self._dfsummary.L2RHMisL2Res
    s = "MissingR%4d [%6d]" % (v, t)
    meter.set(v, s)
    meter.after(self._grDelay, lambda: self.f_l2rhErr(meter))

  def f_dfmRate(self, meter):  
    self.Update()
    v = self._dfsummary.DFMRate
    p = self._dfsummary.DFMRatePerc
    s = "%19.0f Hz" % (v)
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_dfmRate(meter))

  def f_robinOcc(self, meter): # TODO: check tha value
    self.Update()
    v = self._dfsummary.HotRobinOcc
    #s = "%6.1f %s %10s " % (v*100, '%', 'Hot robin' )
    s = "%6.1f %s %10s " % (v, '%', 'Hot robin' )
    meter.set(v/100., s)
    meter.after(self._grDelay, lambda: self.f_robinOcc(meter))

  def f_rosRate(self, meter): 
    self.Update()
    v = self._dfsummary.HotRosRate 
    s = "%-8s %10d Hz" % ('Hot Ros', v)
    meter.set( v / float(self._limitros) , s)
    meter.after(self._grDelay, lambda: self.f_rosRate(meter))

  def f_efdProc(self, meter):
    self.Update()
    v = self._dfsummary.EFEvWaitingForPro
    p = self._dfsummary.EFProcQOcc
    s = "%6.1f %s %10d #" % (p, '%', v)
    meter.set(p/100.,s)
    meter.after(self._grDelay, lambda: self.f_efdProc(meter))

  def f_efdDeliv(self, meter):
    self.Update()
    v = self._dfsummary.EFEvWaitingForDel
    p = self._dfsummary.EFDelivQOcc
    s = "%6.1f %s %10d #" % (p, '%', v)
    meter.set(p/100.,s)
    meter.after(self._grDelay, lambda: self.f_efdDeliv(meter))

  def f_efioSfi(self, meter):
    self.Update()
    v = self._dfsummary.EFIOsfiBrkConnInterval
    t = self._dfsummary.EFIOsfiBrkConn
    p = 1 if v > t else 0
    s = "%7s %4d [%6d]" % ('efio',v, t)
    meter.set(p,s)
    meter.after(self._grDelay, lambda: self.f_efioSfi(meter))
    
  def f_efioSfo(self, meter): 
    self.Update()
    v = self._dfsummary.EFIOsfoBrkConnInterval
    t = self._dfsummary.EFIOsfoBrkConn
    p = 1 if v > t else 0
    s = "%7s %4d [%6d]" % ('efio', v, t)
    meter.set(p,s)
    meter.after(self._grDelay, lambda: self.f_efioSfo(meter))
    
  def f_efdFlow(self, meter):
    self.Update()
    v = self._dfsummary.EFFlowCtrlAvg
    p = v / 400.  
    s = "FlowCtrlDelay: %5d ms" % (v)
    meter.set(p,s)
    meter.after(self._grDelay, lambda: self.f_efdFlow(meter))

  def f_sfoDataS(self, meter): #TODO verify p!
    self.Update()
    b = self._dfsummary.SFOEvSaveBW
    r = self._dfsummary.SFOEvSaveRate
    p = old_div(600, self._dfsummary.SFOnum)
    s = "%5d MB/s %8.0f Hz" % (b , r)
    meter.set(p/100., s)
    meter.after(self._grDelay, lambda: self.f_sfoDataS(meter))

  def f_sfoDataR(self, meter): 
    self.Update()
    b = self._dfsummary.SFOEvRecBW
    r = self._dfsummary.SFOEvRecRate
    p = self._dfsummary.SFOdataBWPerc
    s = "%5d MB/s %8.0f Hz" % (b , r)
    meter.set(p/100., s)
    meter.after(self._grDelay, lambda: self.f_sfoDataR(meter))

  def f_df(self, meter):  
    self.Update()
    x = self._dfsummary.DAQBackPressureStatus
    meter.set( int(not x.startswith("OK")) , x)
    meter.after(self._grDelay, lambda: self.f_df(meter))

  def f_l2A(self, meter):  
    self.Update()
    v = self._dfsummary.L2accPerc
    meter.set(1, "Acc. %5.1f%s" % (v, '%') )
    meter.after(self._grDelay*10, lambda: self.f_l2A(meter))

  def f_efA(self, meter):
    self.Update()
    v = self._dfsummary.EFaccPerc
    meter.set(1, "Acc. %5.1f%s" % (v, '%') )
    meter.after(self._grDelay*10, lambda: self.f_efA(meter))

  def f_ef(self, meter):  
    self.Update()
    meter.set(1, self._dfsummary.EFFarm)
    meter.after(self._grDelay*2, lambda: self.f_ef(meter))

  def f_l2(self, meter):  
    self.Update()
    meter.set(1, self._dfsummary.L2Farm)
    meter.after(self._grDelay*2, lambda: self.f_l2(meter))

  def f_eb(self, meter): 
    self.Update()
    meter.set(1, self._dfsummary.EBFarm)
    meter.after(self._grDelay*2, lambda: self.f_eb(meter))

  def f_sfo(self, meter): 
    self.Update()
    meter.set(1, self._dfsummary.SFOFarm) 
    meter.after(self._grDelay*2, lambda: self.f_sfo(meter))

  def f_l2Err(self, meter):  
    self.Update()
    t = self._dfsummary.L2forceAcc
    v = self._dfsummary.L2forceAccInterval
    s = "ForceAcc%4d [%6d]" % (v, t)
    meter.set(v, s)
    meter.after(self._grDelay*2, lambda: self.f_l2Err(meter))
 
  def f_efErr(self, meter):
    self.Update()
    t = self._dfsummary.EFforceAcc
    v = self._dfsummary.EFforceAccInterval
    s = "ForceAcc%4d [%6d]" % (v, t)
    meter.set(v, s)
    meter.after(self._grDelay*2, lambda: self.f_efErr(meter))

  def f_l2Cpu(self, meter): 
    self.Update()
    v = self._dfsummary.L2cpuPerc
    s = "%6.1f %s %10s  " % (v , '%', 'cpu')
    meter.set(v/100., s)
    meter.after(self._grDelay*2, lambda: self.f_l2Cpu(meter))

  def f_efCpu(self, meter):
    self.Update()
    v = self._dfsummary.EFcpuPerc
    s = "%6.1f %s %10s  " % (v , '%', 'cpu')
    meter.set(v/100., s)
    meter.after(self._grDelay*2, lambda: self.f_efCpu(meter))

  def f_sfoCpu(self, meter):
    self.Update()
    v = self._dfsummary.SFOcpuPerc
    s = "%6.1f %s %10s  " % (v , '%', 'cpu')
    meter.set(v/100., s)
    meter.after(self._grDelay*2, lambda: self.f_sfoCpu(meter))

  def f_PhySfoSize(self, meter):
    self.Update()
    v = self._dfsummary.SFORSizePhys
    meter.set( old_div(v,self._limitSize), "Phys: %6d kB" % (v)) 
    meter.after(self._grDelay*2, lambda: self.f_PhySfoSize(meter))

  def f_CalSfoSize(self, meter):
    self.Update()
    v = self._dfsummary.SFORSizeCalib
    meter.set( old_div(v,self._limitSize), "Calib: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_CalSfoSize(meter))

  def f_PhySSfoSize(self, meter):
    self.Update()
    v = self._dfsummary.SFOSSizePhys
    meter.set( old_div(v,self._limitSize), "Phys: %6d kB" % (v)) 
    meter.after(self._grDelay*2, lambda: self.f_PhySSfoSize(meter))

  def f_CalSSfoSize(self, meter):
    self.Update()
    v = self._dfsummary.SFOSSizeCalib
    meter.set( old_div(v,self._limitSize), "Calib: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_CalSSfoSize(meter))

  def f_PhySfiSize(self, meter):
    self.Update()
    v = self._dfsummary.SFISizePhys
    meter.set( old_div(v,self._limitSize), "Phys: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_PhySfiSize(meter))

  def f_CalSfiSize(self, meter):
    self.Update()
    v = self._dfsummary.SFISizeCalib
    meter.set( old_div(v,self._limitSize), "Calib: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_CalSfiSize(meter))

  def f_AvgSfiSize(self, meter):
    self.Update()
    v = self._dfsummary.SFISizeAvg
    meter.set( old_div(v,self._limitSize), "Avg: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_AvgSfiSize(meter))

  def f_AvgSfoSize(self, meter):
    self.Update()
    v = self._dfsummary.SFOSizeAvg
    meter.set( old_div(v,self._limitSize), "Agv: %6d kB" % (v))
    meter.after(self._grDelay*2, lambda: self.f_AvgSfoSize(meter))

  def f_rosbusy(self, meter): 
    self.Update()
    v = self._dfsummary.NumOfBusyROLs
    s = '# busy ROLs: %d' % v
    meter.set(v, s)
    meter.after(self._grDelay, lambda: self.f_rosbusy(meter))

  def f_dfmbusy(self, meter):
    self.Update()
    v = self._dfsummary.DFMDeadtimePerc
    t = self._dfsummary.DFMNumXOFF
    s = "%6.1f %s %10d #" % (v,'%',t)
    meter.set(v/100., s)
    meter.after(self._grDelay, lambda: self.f_dfmbusy(meter))

  def f_dfmOcc(self, meter):  
    self.Update()
    a = self._dfsummary.DFMInputQueue
    v = self._dfsummary.DFMQueueOccPerc
    s = "%6.1f %s %10d #" % (v, '%', a)
    meter.set(v/100., s)
    meter.after(self._grDelay, lambda: self.f_dfmOcc(meter))

  def f_sfibusy(self, meter):  
    self.Update()
    v = self._dfsummary.EBsfiBusyMsgsInterval
    t = self._dfsummary.EBsfiBusyMsgs
    try: p = old_div(v,self._dfsummary.EBnumSFI)
    except(ZeroDivisionError): p = 0
    s = "%7s %4d [%6d]" % ('busyMsg',v, t)
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_sfibusy(meter))

  def f_roibbusy(self, meter): # TODO verify
    self.Update()
    v = self._dfsummary.ROIBOutChStatus
    s = ""
    try: s = v.replace(' ----','')
    except(IndexError): s = "??"
    s = "ROIB " + s
    p = self._dfsummary.BusyROIB
    meter.set(p, s)
    meter.after(self._grDelay, lambda: self.f_roibbusy(meter))

  def f_sfoUse(self, meter):
    self.Update()
    v = self._dfsummary.SFOdiskUsageTop
    s = "%6.1f %s %10s  " % (v, '%', ' ' )
    meter.set(v/100., s)
    meter.after(self._grDelay*10, lambda: self.f_sfoUse(meter))
   
  def f_t0(self, meter):
    self.Update()
    p = self._dfsummary.T0RunTransferPerc
    meter.set(p/100., "%4.2f %s" % (p, '%'))
    meter.after(self._grDelay*20, lambda: self.f_t0(meter))

  def f_sol(self, meter):  
    self.Update()
    v = self._dfsummary.DCSSolenoid
    meter.set(old_div(v,100), "Solenoid %4.1f %s" % (v, '%'))
    meter.after(self._grDelay*10, lambda: self.f_sol(meter))

  def f_tor(self, meter): 
    self.Update()
    v = self._dfsummary.DCSToroid
    meter.set(old_div(v,100), "Toroid %4.1f %s" % (v, '%'))
    meter.after(self._grDelay*10, lambda: self.f_tor(meter))

  def f_ini(self, meter):  
    self.Update()
    if self._dfsummary.DCSStableBeam : t = 'Yes'
    else                             : t = 'No'
    s = '%s [stable=%s]' % ( self._dfsummary.DCSBeamMode, t)
    meter.set(1, s)
    meter.after(self._grDelay*10, lambda: self.f_ini(meter))

  def f_phys(self, meter):
    self.Update()
    mbs = self._dfsummary.SFOSavedphysBW
    v   = self._dfsummary.SFOSavedphysRate
    try: p = old_div(mbs,(self._dfsummary.SFOnum * 150)) 
    except (ZeroDivisionError): p = 0
    meter.set(p, "%5d MB/s %8.0f Hz" % (mbs, v))
    meter.after(self._grDelay*10, lambda: self.f_phys(meter))

  def f_calib(self, meter):
    self.Update()
    mbs = self._dfsummary.SFOSavedcalibBW
    v   = self._dfsummary.SFOSavedcalibRate
    try: p = old_div(mbs,(self._dfsummary.SFOnum * 150)) 
    except (ZeroDivisionError): p = 0
    meter.set(p, "%5d MB/s %8.0f Hz" % (mbs, v))
    meter.after(self._grDelay*10, lambda: self.f_calib(meter))

  def f_express(self, meter):
    self.Update()
    mbs = self._dfsummary.SFOSavedexprBW
    v   = self._dfsummary.SFOSavedexprRate
    try: p = old_div(mbs,(self._dfsummary.SFOnum * 150)) 
    except (ZeroDivisionError): p = 0
    meter.set(p, "%5d MB/s %8.0f Hz" % (mbs, v ))
    meter.after(self._grDelay*10, lambda: self.f_express(meter))

  def f_debug(self, meter): 
    self.Update()
    v = self._dfsummary.SFOReceiveddebugRate
    try: p = v / float(self._dfsummary.SFOEvRecRate)
    except (ZeroDivisionError): p = 0
    meter.set(p, "Debug %5d Hz" % (v))
    meter.after(self._grDelay*10, lambda: self.f_debug(meter))


##-- METER --------------------------------------------##
# Meter class definition
class Meter(tkinter.Frame):
    def __init__(self, master, width=200, height=20, bg='white', fillcolor='orchid1',\
            value=0.0, text=None, font=None, textcolor='black', *args, **kw):
        tkinter.Frame.__init__(self, master, bg=bg, width=width, height=height, *args, **kw)
        self._value = value

        self._canv = tkinter.Canvas(self, bg=self['bg'], width=self['width'], height=self['height'],\
                               highlightthickness=0, relief='flat', bd=0)
        self._canv.pack(fill='both', expand=1)
        self._rect = self._canv.create_rectangle(0, 0, 0, self._canv.winfo_reqheight(), fill=fillcolor,\
                                            width=0)
        self._text = self._canv.create_text(old_div(self._canv.winfo_reqwidth(),2), old_div(self._canv.winfo_reqheight(),2),\
                                       text='', fill=textcolor)
        if font:
            self._canv.itemconfigure(self._text, font=font)

        self.set(value, text)
        self.bind('<Configure>', self._update_coords)

    def _update_coords(self, event):
        '''Updates the position of the text and rectangle inside the canvas when the size of
        the widget gets changed.'''
        # looks like we have to call update_idletasks() twice to make sure
        # to get the results we expect
        self._canv.update_idletasks()
        self._canv.coords(self._text, old_div(self._canv.winfo_width(),2), old_div(self._canv.winfo_height(),2))
        self._canv.coords(self._rect, 0, 0, self._canv.winfo_width()*self._value, self._canv.winfo_height())
        self._canv.update_idletasks()

    def get(self):
        return self._value, self._canv.itemcget(self._text, 'text')

    def set(self, value=0.0, text=None):
        #make the value failsafe:
        if value < 0.0:
            value = 0.0
        elif value > 1.0:
            value = 1.0
        self._value = value
        if text == None:
            #if no text is specified use the default percentage string:
            text = str(int(round(100 * value))) + ' %'
        self._canv.coords(self._rect, 0, 0, self._canv.winfo_width()*value, self._canv.winfo_height())
        self._canv.itemconfigure(self._text, text=text)
        self._canv.update_idletasks()

##-- METER --------------------------------------------##

## ----- Helper structure  -----
class mConf(object):
  _tt     = ""
  _func   = 0
  _mcolor = ""
  _hidden = False
  def __init__(self, tt, func, mcolor, hidden=False):
    self._tt     = tt
    self._func   = func
    self._mcolor = mcolor
    self._hidden = hidden

## ----- Helper functions  -----

def colscale(s,f):
  ''' "Scale" the color code s of a factor f'''
  s = s[1:]
  r,g,b = int(s[:2],16), int(s[2:4],16), int(s[4:],16)
  r,g,b = min(255,round(r*f)), min(255,round(g*f)), min(255,round(b*f))
  return  '#%2X%2X%2X' % (r, g ,b)

def makeLine(parent, title, tcolor, col, mList, relief='ridge', bd=2, ftt=None):
  '''
      Setup a line composed of: Label | Meter | Meter | Meter |
      Max number of meters supported: 3
  '''
  sz = 0
  if   len(mList) == 1 : sz = 542
  elif len(mList) == 2 : sz = 256
  elif len(mList) == 3 : sz = 160
  else : print('makeline error: only 3 meters supported'); sys.exit(1)

  bg  = col
  mbg = colscale(col, 1.07)
  hg  = 16
  
  f = tkinter.Frame(parent, bd=2, relief="flat", bg=col)
  f.pack(side="top", fill='both')
  l = tkinter.Label(f, width=12, anchor="w", text=title, fg=tcolor, bg=col)
  l.pack(side='left')
  if ftt:
    ToolTip.ToolTip(l,  msgFunc=ftt )

  fo   = tkinter.font.Font(family='fixed', size=fontsize)
  # Dirty trick to make 'lambda' working. A for loop doesn't work 
  if len(mList) >= 1 :
    n = mList[0]
    if not n._func:
      m0 = Meter(f, relief='flat', bd=bd, bg=col, fillcolor=col,  text='', width=sz, height=hg)
      m0.pack(fill='x', side='left')
    else:
      if n._hidden:
        m0 = Meter(f, relief='flat', bd=0, fillcolor=n._mcolor, width=sz+4, bg=mbg, font=fo, height=hg)
        m0.pack(fill='x', side='left')
      else:  
        m0 = Meter(f, relief=relief, bd=bd, fillcolor=n._mcolor, width=sz, bg=mbg, font=fo, height=hg)
        m0.pack(fill='x', side='left')
      f0 = n._func
      m0.after(1000, lambda: f0(m0))
    if len(n._tt) > 0: ToolTip.ToolTip(m0,  msg=n._tt )
   
  if len(mList) >= 2 :
    l = tkinter.Label(f, width=3, anchor="w", text='   ', fg=tcolor, bg=col)
    l.pack(side='left')

    n = mList[1]
    if not n._func:
      m1 = Meter(f, relief='flat', bd=bd, bg=col, fillcolor=col,  text='', width=sz, height=hg)
      m1.pack(fill='x', side='left')
    else:
      m1 = Meter(f, relief=relief, bd=bd, fillcolor=n._mcolor, width=sz, bg=mbg, font=fo, height=hg)
      m1.pack(fill='x', side='left')
      f1 = n._func
      m1.after(1000, lambda: f1(m1))
    if len(n._tt) > 0: ToolTip.ToolTip(m1,  msg=n._tt )

  if len(mList) >= 3 :
    l = tkinter.Label(f, width=3, anchor="w", text='   ', fg=tcolor, bg=col)
    l.pack(side='left')
    n = mList[2]
    if not n._func:
      m2 = Meter(f, relief='flat', bd=bd, bg=col, fillcolor=col,  text='', width=sz, height=hg)
      m2.pack(fill='x', side='left')
    else:
      m2 = Meter(f, relief=relief, bd=bd, fillcolor=n._mcolor, width=sz, bg=mbg, font=fo, height=hg)
      m2.pack(fill='x', side='left')
      f2 = n._func
      m2.after(1000, lambda: f2(m2))
    if len(n._tt) > 0: ToolTip.ToolTip(m2,  msg=n._tt )

# Helper function
def makeToolTip(parent, sz, txt, tcolor, func): 
    l = tkinter.Label(parent, width=sz,  anchor="w", text=txt, fg=tcolor); 
    l.pack(side='left') 
    ToolTip.ToolTip(l,  msgFunc=func )

# Menu functions
def screenshot():
  filename = '/tmp/ispymon.%d.png' % (int(time.time()))
  msg = 'Shot will be saved in file: %s' % (filename)
  k = tkinter.messagebox.showinfo("Shot", msg)
  os.spawnl(os.P_NOWAIT, '/usr/bin/import', 'import', filename)

def panic():
  os.spawnl(os.P_NOWAIT, '/usr/bin/display', 'display', '/atlas-home/1/anegri/panic.jpg')

def phonebook():
  os.spawnl(os.P_NOWAIT, '/usr/bin/konqueror', 'konqueror', 'http://pc-atlas-www.cern.ch/status/Phone.htm')

def network():
  url='http://pc-tdq-net-mon-es/es/rrddisplay/es/rrdpage/page/?&name=dataflow&viewtype=static::net_trf_agg'
  os.spawnl(os.P_NOWAIT, '/usr/bin/firefox', 'firefox', url)

def isHistoryPlots():
  url='http://pc-tdq-net-mon-es/es/rrddisplay/es/rrdpage/single/daq/is/?&name=DF.DFM-1&viewtype=xmlrpc::daq_is_server()'
  os.spawnl(os.P_NOWAIT, '/usr/bin/firefox', 'firefox', url)

def sfoDisplay():
   os.spawnl(os.P_NOWAIT, '/etc/CRD/bin/sfo-display.py', 'sfo-display.py')

def hfetch(par): 
   os.spawnl(os.P_NOWAIT, '/det/tdaq/scripts/oh.fetch.py', 'oh.fetch.py', par)

def hHistory(): hfetch('-B')
def hSfoStr():  hfetch('-s')
def hEfio():    hfetch('-E')
def hRobin():   hfetch('-r')
def hRos():     hfetch('-R')
def hPt():      hfetch('-p')
def hSize():    hfetch('-z')
def hEfd():     hfetch('-e')
def hEfRacks(): hfetch('-a')
def hTime():    hfetch('-t')
def hL2():      hfetch('-2')

def forceFullUpdate(): 
  worker.readRDB()
  worker._1stLoop = True


#==================== MAIN =====================================

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hvu:w:g:t:p:s:STDIXCZ")
except getopt.GetoptError as err:
  print(str(err)) 
  usage()
  sys.exit(2)
for o,a in opts:
  if    o == "-h": usage()
  elif  o == "-p": part_name=a
  elif  o == "-t": fontsize    = int(a)
  elif  o == "-u": isupdate    = int(a)
  elif  o == "-w": isslowupdate= int(a)
  elif  o == "-g": graphupdate = int(a)
  elif  o == "-s": limitSize   = int(a)
  elif  o == "-v": verbose     = True
  elif  o == "-S": blkSizes    = False
  elif  o == "-T": blkToolTips = False
  elif  o == "-D": blkDF       = False
  elif  o == "-R": blkRC       = False
  elif  o == "-I": blkIni      = False


print('----------------------------------------------')
print('NB: if the text does not fit inside the boxes')
print('    use the "-t" switch to tune the font size')
print('    eg: ispymon -t 10')
print('----------------------------------------------')
print('Partition name =', part_name)
print('Verbose        =', verbose)
print('IS update      =', isupdate, 's')
print('IS slow update =', isslowupdate, 's')
print('Graph update   =', graphupdate, 'ms')
print('Font size      =', fontsize)
print('Limit size     =', limitSize)
print('blkToolTips    =', blkToolTips)
print('blkRC          =', blkRC)
print('blkDF          =', blkDF)
print('blkSizes       =', blkSizes)
print('blkIni         =', blkIni)
print('---------------------------')


# Instantiate worker object
worker = Worker(part_name, blkIni)
worker._isDelay    = isupdate
worker._grDelay    = graphupdate
worker._limitSize  = limitSize
worker._isDelayFact= old_div(isslowupdate,isupdate)
worker._verbose    = verbose

# Do the 1st update
worker.Update()

# Prevent the start if the partition is not running
noway = False
state = worker._rc.state
if  not state or state=='NONE':
  msg = "The '%s' partition either does not exist or is not booted. Exiting." % part_name
  noway = True
try: worker._dfsummary.RunOverview
except AttributeError:
  msg = "No DFSummary object published in IS under the partition '%s'. Exiting." % part_name
  noway = True

if noway:
  print("\033[91m"+msg+"\033[0m")
  root = tkinter.Tk()
  root.title("DFSummary")
  w = tkinter.Label(root, text=msg)
  w.pack()
  root.mainloop()
  sys.exit(1)

# Create the root window
root = tkinter.Tk(className='DataFlow Summary')
fo   = tkinter.font.Font(family='fixed', size=fontsize, weight="bold")
root.option_add("*font", fo)

menubar  = tkinter.Menu(root)
filemenu = tkinter.Menu(menubar, tearoff=0)
filemenu.add_command(label="Launch sfo-display",             command=sfoDisplay)
filemenu.add_command(label="On-call phones (konqueror)",     command=phonebook )
filemenu.add_command(label="Take a Screenshot",              command=screenshot)
filemenu.add_command(label="Force full update",              command=forceFullUpdate)
filemenu.add_separator()
filemenu.add_command(label="Panic",                          command=panic)
filemenu.add_command(label="Exit",                           command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)
histmenu = tkinter.Menu(menubar, tearoff=0)
histmenu.add_command(label="Network history plots (firefox)",command=network   )
histmenu.add_command(label="IS history plots (firefox)",     command=isHistoryPlots)
histmenu.add_separator()
histmenu.add_command(label="Robins occupancy",               command=hRobin)
histmenu.add_command(label="ROSs",                           command=hRos)
histmenu.add_separator()
histmenu.add_command(label="L2",                             command=hL2)
histmenu.add_command(label="EBEF history",                   command=hHistory)
histmenu.add_command(label="EFD",                            command=hEfd)
histmenu.add_command(label="PTs",                            command=hPt)
histmenu.add_command(label="Processing time at L2&EF",       command=hTime)
histmenu.add_command(label="EF stats by rack",               command=hEfRacks)
histmenu.add_command(label="EFIO errors",                    command=hEfio)
histmenu.add_separator()
histmenu.add_command(label="Stream names & correlation @SFO",command=hSfoStr)
histmenu.add_command(label="Size stream correlations",       command=hSize)
menubar.add_cascade(label="Histograms", menu=histmenu)
root.config(menu=menubar)


tcol = '#000000'
# Run frame and RC frame
if blkRC:
  bcol = '#F7F8E0'
  fcol = '#FFFF00'
  ecol = '#F9A3A3'
  fa = tkinter.Frame(root, bd=2, relief="raised")
  fa.pack(side="top", fill='both')
  ToolTip.ToolTip(fa,  msgFunc=worker.tt_error )
  makeLine(fa, "Run ", '#000000', bcol, 
    [ mConf("", worker.f_run, fcol ), mConf("", worker.f_rc, fcol ) ] )
  makeLine(fa, "Busy", tcol, bcol, [
    mConf( "L1CT-History.ISCTPBUSY.ctpcore_mon0_rate",      worker.f_globalbusy, ecol ) ,
    mConf( "L1CT-History.ISCTPBUSY.ctpcore_daq_slink_rate", worker.f_daqbusy,    ecol ) ], ftt=worker.tt_l1ct)
  makeLine(fa, "Trig.Keys", tcol, bcol, [  mConf( "", worker.f_hlt, bcol ) ], relief='flat', bd=0)
  if blkIni:
    bcol = '#FFEEFF'
    fb = tkinter.Frame(root, bd=2, relief="raised")
    fb.pack(side="top", fill='both')
    makeLine(fb, "Beams", '#000000',  bcol,[ mConf( "", worker.f_ini, bcol ) ], relief="flat", bd=0)
    makeLine(fb, "Magnets", tcol, bcol, [
      mConf( "Solenoid", worker.f_sol, bcol ) ,
      mConf( "Toroid",   worker.f_tor, bcol ) ])

# Tool tips frame 
if blkToolTips:
  tcol = '#444444'
  fz = tkinter.Frame(root, bd=2, relief="raised")
  fz.pack(side="top", fill='both')
  fz.option_add("*font", fo)

  fz1= tkinter.Frame(fz, bd=2, relief="flat")
  fz1.pack(side="top", fill='both')
  l1 =tkinter.Label(fz1, width=9, anchor="w", text="ToolTips:"); 
  l1.pack(side='left')
  makeToolTip(fz1, 8, "Magnets"  ,tcol, worker.tt_mag)
  makeToolTip(fz1,10, "Detectors",tcol, worker.tt_det)
  makeToolTip(fz1, 5, "RoIB",     tcol, worker.tt_roib)
  makeToolTip(fz1, 4, "ROS",      tcol, worker.tt_ros)
  makeToolTip(fz1, 5, "L2SV",     tcol, worker.tt_l2sv)
  makeToolTip(fz1, 5, "L2RH",     tcol, worker.tt_l2rh)
  makeToolTip(fz1, 4, "DFM",      tcol, worker.tt_dfm)
  makeToolTip(fz1, 4, "SFI",      tcol, worker.tt_sfi)
  makeToolTip(fz1, 4, "EFD",      tcol, worker.tt_efd)
  makeToolTip(fz1, 4, "SFO",      tcol, worker.tt_sfo)
  makeToolTip(fz1, 6, "Disks",    tcol, worker.tt_disk)

  fz2= tkinter.Frame(fz, bd=2, relief="flat")
  fz2.pack(side="top", fill='both')
  makeToolTip(fz2, 9, " ",        tcol, worker.tt_qlog)
  makeToolTip(fz2, 8, "RunCtrl",  tcol, worker.tt_rc)
  makeToolTip(fz2, 7, "RunPar",   tcol, worker.tt_rp)
  makeToolTip(fz2, 5, "Busy",     tcol, worker.tt_l1ct)
  makeToolTip(fz2, 7, "GlBusy",   tcol, worker.tt_gb)
  makeToolTip(fz2, 4, "Top",      tcol, worker.tt_top)
  makeToolTip(fz2, 6, "Farms",    tcol, worker.tt_sv)
  makeToolTip(fz2, 5, "Keys",     tcol, worker.tt_keys)
  makeToolTip(fz2, 8, "Streams",  tcol, worker.tt_streams)
  makeToolTip(fz2, 8, "EvSizes",  tcol, worker.tt_sizes )
  makeToolTip(fz2, 9, "Counters", tcol, worker.tt_counters )

#--- DF frame   
if blkDF:
  bcol = '#E0ECF8'
  fcol = '#A9A9F5'
  wcol = '#FE9A2E'
  ecol = '#F9A3A3'
  fdf  =  tkinter.Frame(root, bd=2, relief="raised")
  fdf.pack(side="top", fill='both')

  # Head
  fdf0 =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdf0.pack(side="top", fill='both')
  makeLine(fdf0, "DataFlow", tcol, bcol, [ mConf("", worker.f_df, ecol ) ], bd=0, relief='flat')
  t = "                       Rate                     Occupancy                Busy/Errors"
  tkinter.Label(fdf0, width=60, anchor="w", text=t, fg=tcol, bg=bcol).pack(fill='x')

  # Level 1
  fdf1 =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdf1.pack(side="top", fill='both')
  makeLine(fdf1, "Level1", tcol, bcol, [  mConf( "", None, bcol ) ], relief='flat', bd=0)
  makeLine(fdf1, " Ros", tcol, bcol, [
    mConf( "Most accessed ROBIN"                                       , worker.f_rosRate,  fcol ) ,
    mConf( "Most occupied ROBIN"                                       , worker.f_robinOcc, wcol ) , 
    mConf( "Number of ROL-Xoff set\n (NB: some ROLs could be disabled)", worker.f_rosbusy,  ecol)])
  makeLine(fdf1, " L1 out", tcol, bcol, [
    mConf( "L1 accept rate (Hz)", worker.f_l1Rate, fcol ) ,
    mConf( "",                    None           , wcol ) ,
    mConf( "",                    None           , fcol ) ] )

  # Level 2
  fdf2 =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdf2.pack(side="top", fill='both')
  makeLine(fdf2, "Level2", tcol, bcol, [  mConf( "", worker.f_l2, bcol ) ], relief='flat', bd=0)
  makeLine(fdf2, " L2 in", tcol, bcol, [
    mConf( "Bandwidth ROS to L2",                              worker.f_ros2l2,   fcol ) ,
    mConf( "Events inside L2",                                 worker.f_l2Occ,     wcol ) ,
    mConf( "Output Channel Status\n (0/1 is the busy status)", worker.f_roibbusy, ecol ) ])
  makeLine(fdf2, " L2PUs",tcol, bcol, [
    mConf( "L2 acceptance (instantaneous)",                 worker.f_l2A,   bcol, hidden=True ) ,
    mConf( "Interval CPU usage",                            worker.f_l2Cpu, wcol ),
    mConf( "",                                              None          , fcol ) ], ftt=worker.tt_l2sv)
  makeLine(fdf2, " L2RH",tcol, bcol, [
    mConf( "Max = %d MB/s" % (100*worker._dfsummary.numL2RHs), worker.f_l2rhRate, fcol ) ,
    mConf( "Max = %d #" % (worker._conf_l2rh.storeSize)   , worker.f_l2rhOcc,  wcol ) ,
    mConf( "Missing LVL2 result"                          , worker.f_l2rhErr,  fcol ) ], ftt=worker.tt_l2rh)
  makeLine(fdf2, " L2 out", tcol, bcol, [
    mConf( "L2 accept rate (Hz)",             worker.f_dfmRate,   fcol ) ,
    mConf( "",                    None           , wcol ) ,
    mConf( "Force accepted events\n Interval [cumulative]", worker.f_l2Err, ecol ) ] , ftt=worker.tt_l2sv)

  # Event Builder
  fdfb =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdfb.pack(side="top", fill='both')
  makeLine(fdfb, "EventBuilder", tcol, bcol, [  mConf( "", worker.f_eb, bcol ) ], relief='flat', bd=0)
  v = worker._conf_dfm.MaxInputQueueSize
  makeLine(fdfb, " DFM", tcol, bcol, [
    mConf( "",                                                       None,             fcol ) , 
    #mConf( "Events to be built (Assigned - Built)\n Max = %d" % (v), worker.f_dfmOcc,  wcol ) ,
    mConf( "Input queue \n Max = %d" % (v),                          worker.f_dfmOcc,  wcol ) ,
    mConf( "DFM deadtime (%) - Cumulative XOFF",                              worker.f_dfmbusy, ecol )] , ftt=worker.tt_dfm)
  v = worker._dfsummary.EBnumSFI * 100
  y = worker._conf_sfi.MaxAssignEvents * worker._dfsummary.EBnumSFI
  makeLine(fdfb, " EB in", tcol, bcol, [
    mConf( "Max = %d MB/s\n (100 * num_of_SFIs)" % (v),                   worker.f_sfiRateI, fcol ) ,
    mConf( "Events to be built (Assigned - Built)\n Max = %d#" % (y),     worker.f_sfiOccIn, wcol ) ,
    mConf( "Number of busy msg:\n interval [cumulative]",                 worker.f_sfibusy,  ecol ) ], ftt=worker.tt_sfi)
  v = worker._conf_sfi.MaxOutputQueueSize * worker._dfsummary.EBnumSFI
  makeLine(fdfb, " EB out", tcol, bcol, [
    mConf( ""                                                              , None,               fcol ) ,
    mConf( "Events to be delivered to EF (Built-Deleted)\n Max = %d#" % (v), worker.f_sfiOccOut, wcol ) ,
    mConf( "SFI/EF broken connections:\n interval [cumulative]",             worker.f_efioSfi,   ecol ) ], ftt=worker.tt_sfi )

  # Event filter
  fdfe =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdfe.pack(side="top", fill='both')
  makeLine(fdfe, "EventFilter", tcol, bcol, [  mConf( "", worker.f_ef, bcol ) ], relief='flat', bd=0)
  v = worker._dfsummary.EFnumXpuRacks*200 + worker._dfsummary.EFnumEfRacks*1000
  makeLine(fdfe, " EF in", tcol, bcol, [
    mConf( "Max = %d MB/s\n (1000 * #efRacks + 200 * #xpuRacks@EF)" % (v), worker.f_sfiRate, fcol ) ,
    mConf( "Process queue:\n if full => CPU limited"     , worker.f_efdProc, wcol ) ,
    mConf( "Flow ctrl delay. Check the upstream backpressure. Measure the delay applied by the EFD in requesting events to the SFIs. The mean value of the delay wrt a watermark (~ 20% of buffer occupancy) is showed."                                          , worker.f_efdFlow, ecol ) ], ftt=worker.tt_efd )
  makeLine(fdfe, " PTs",tcol, bcol, [ 
    mConf( "EF acceptance (R = Events rejected)"         , worker.f_efA,   bcol, hidden=True ) ,
    mConf( "Interval CPU usage",                           worker.f_efCpu, wcol ),
    mConf( "Force accepted events\n Interval [cumulative]",worker.f_efErr, ecol ) ] )
  v = worker._dfsummary.SFOnum * 150
  makeLine(fdfe, " EF out", tcol,  bcol,[
    mConf( "Max = %d MB/s\n (150 * num_of_SFOs)" % (v)         , worker.f_sfoDataR, fcol ) ,
    mConf( "Delivery queue:\n if full => downstream limited"   , worker.f_efdDeliv, wcol ) ,
    mConf( "EF/SFO broken connections:\n interval [cumulative]", worker.f_efioSfo,  ecol ) ], ftt=worker.tt_efd )

  # SFO
  fdfs =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdfs.pack(side="top", fill='both')
  makeLine(fdfs, "SFO", tcol, bcol, [  mConf( "", worker.f_sfo, bcol ) ], relief='flat', bd=0, ftt=worker.tt_sfo)
  t = 'Total data write speed (all the SFOs) \nN.B. it is greater than the receive rate'
  makeLine(fdfs, " disk", tcol, bcol, [
    mConf( t                               , worker.f_sfoDataS, fcol ) ,
    mConf( "Top used disk raid"            , worker.f_sfoUse,   wcol ) ,
    mConf( "Event rate in the debug stream", worker.f_debug,    ecol ) ], ftt=worker.tt_disk )
  v = "Interval CPU usage \n Max 400%, but actual limit ~ 200% "
  makeLine(fdfs, "   physics", tcol, bcol, [
    mConf( "Event and data rate in physics stream" , worker.f_phys,  fcol ) ,
    mConf( v,                                        worker.f_sfoCpu,wcol ) ,
    mConf( "",                                       None,          ecol ) ], ftt=worker.tt_streams )
  makeLine(fdfs, "   express",   tcol, bcol, [
    mConf( "Event and data rate in express stream",  worker.f_express, fcol ) ,
    mConf( "",                                       None,             wcol ) ,
    mConf( "",                                       None,             ecol ) ], ftt=worker.tt_streams)
  makeLine(fdfs, "   calibrat.", tcol, bcol, [
    mConf( "Event and data rate in calibration stream",  worker.f_calib, fcol ) ,
    mConf( "",                                           None,           wcol ) ,
    mConf( "",                                           None,           ecol ) ], ftt=worker.tt_streams )

  fcol = '#FF88FF'
  fdft =  tkinter.Frame(fdf, bd=2, relief="flat")
  fdft.pack(side="top", fill='both')
  makeLine(fdft, "T0 transfer", tcol, bcol, [
    mConf( "Fraction of data transferred to T0\nfor the current run" , worker.f_t0,   fcol ) ,
    mConf( "", None,          wcol ) ,
    mConf( "", None,          ecol ) ] )

 
#-- Sizes frame
if blkSizes:
  bcol = '#D0F8D0'
  fcol = '#81F781'
  fs = tkinter.Frame(root, bd=2, relief="raised")
  fs.pack(side="top", fill='both')
  ToolTip.ToolTip(fs,  msgFunc=worker.tt_sizes )
  makeLine(fs, "EvSize@SFI", tcol, bcol, [
    mConf( "", worker.f_PhySfiSize, fcol ) ,
    mConf( "", worker.f_CalSfiSize, fcol ) ,
    mConf( "", worker.f_AvgSfiSize, fcol ) ], ftt=worker.tt_sizes )
  makeLine(fs, "EvSize@SFO", tcol, bcol, [
    mConf( "", worker.f_PhySfoSize, fcol ) ,
    mConf( "", worker.f_CalSfoSize, fcol ) ,
    mConf( "", worker.f_AvgSfoSize, fcol ) ], ftt=worker.tt_sizes )
  makeLine(fs, "EvSize@disk", tcol, bcol, [
    mConf( "", worker.f_PhySSfoSize, fcol ) ,
    mConf( "", worker.f_CalSSfoSize, fcol ) ,
    mConf( "", None, fcol ) ], ftt=worker.tt_sizes )


#---------------------------------

root.mainloop()
