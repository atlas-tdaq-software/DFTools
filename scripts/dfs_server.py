#!/usr/bin/env tdaq_python
# Collects data from IS variables, OKS, cool DB and publishes
# data in IS according to a specific class

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object
import random
import sys
#print sys.path # to check pythonpath
import time
import re
import getopt
import os
from eformat import helper
from ispy import *
import ers
from ipc  import OWLTime
import pm.project
#import dl
#sys.setdlopenflags(dl.RTLD_GLOBAL | dl.RTLD_NOW)
import signal
from hilltrot.hilltrot import *
from sfodb.sfodb import SFOFetch
from optparse import OptionParser
from operator import itemgetter

# Global variables

# Max SFO writing bandwidth in MB/s / server
maxSFOBW = 800

blackDict={}
specialAttributesDict = { 'RunNumber'      : ('_lb', 'RunNumber'),
                          'RunLB'          : ('_lb', 'LumiBlockNumber'),
                          'RCState'        : ('_rc', 'state'),
                          'DCSStableBeam'  : ('_stableBeam',),
                          #'DCSBeamMode'    : ('_beamMode',),
                          'Recording'      : ('_rp', 'recording_enabled'),
                          'DCSSolenoid'    : ('F_setMagnets', 'solenoid', 7730.),  #are these values still correct?
                          'DCSToroid'      : ('F_setMagnets', 'toroid', 20400.), #are these values still correct?
                          'T0RunTransferPerc'    : ('F_transfer2t0Perc',),
                          'InstantaneousLumi'    : ('_instLumi',),
                          'Mu'                   : ('_mu',)
                          }
attributesDict = {'RunOverview'    : ('F_runStr',),                       # Run
                  'RunTime'        : ('F_runtime_s',),
                  'RCFault'        : ('_rc', 'fault'),
                  'RCError'        : ('_rc', 'errorReasons'), #problem
                  'RCBusy'         : ('_rc', 'busy'),
                  'RCGlobalBusy'   : ('_ctpcorebusy',), #
                  'RCDAQBusy'      : ('_daqbusy',), #


                  'DAQBackPressureStatus' : ('F_dfStr',),

                  'NumDCM'         : ('_numdcm',),
                  'NumRacks'       : ('_numracks',),
                  'HLTSMK'         : ('_tkeys', 'sm', 0),                 # HLT
                  'HLTBunchGroupK' : ('_tkeys', 'l1bg', 0),
                  'HLTPSL1'        : ('_tkeys', 'l1', 0),
                  'HLTPSHLT'       : ('_tkeys', 'hlt', 0),
                  'HLTPSL1phys'    : ('_tkeys', 'l1.p', 0),
                  'HLTPSL1standby' : ('_tkeys', 'l1.s', 0),
                  'HLTPSHLTphys'   : ('_tkeys', 'hlt.p', 0),
                  'HLTPSHLTstandby': ('_tkeys', 'hlt.s', 0),
                  'HLTSummary'     : ('F_hltStr',),
                  'HotRosName'     : ('_hotRosName',),                    # ROS
                  'HotRosRate'     : ('_hotRosRate',),
                  'HotRobinOcc'    : ('_hotRobinOcc',),
                  'HotRobinName'   : ('_hotRobinName',),
                  'HighROSLoad'   : ('_highRosLoad',),
                  'HighROSLoadName' : ('_highRosLoadName',),
                  'NumOfBusyROLs'  : ('F_len', '_busyRols'),
                  'HighRolXOff'    : ('_highxoff',),

                  'HLTPUKills'      : ('_hltpu','NumKills'),
                  'HLTPUExits'       : ('_hltpu','NumExited'),

                  'L1EventsOut'  : ('_hltsv','LVL1Events'),
                  'HLTAssignedEvents' : ('_hltsv','AssignedEvents'),
                  'HLTReassignedEvents' : ('_hltsv', 'ReassignedEvents'),
                  'HLTProcessedEvents' : ('_hltsv', 'ProcessedEvents'),
                  'HLTAvailableCores' : ('_hltsv','AvailableCores'),
                  'HLTRate': ('_hltsv','Rate'),
                  'HLTLV1ID' : ('_hltsv','Recent_LVL1_ID'),
                  'HLTGlobalID' : ('_hltsv','Recent_Global_ID'),
                  'HLTBusy': ('_hltsv','Busy'),

                  'HLTUsedCores': ('F_HLTOcc',),
                  'HLTOcc': ('F_HLTOccPerc',),

                  'DCML1Rate': ('_dcm','L1Rate'),
                  'DCML1AveRate': ('_dcm', 'L1RateAvg'),
                  'DCMEBRate': ('_dcm', 'EbRate'),
                  'DCMEBAveRate': ('_dcm', 'EbRateAvg'),
                  'DCMDCRate' : ('_dcm', 'DcReqRate'),
                  'DCMDCAveRate' : ('_dcm', 'DcReqRateAvg'),
                  'DCMOutRate' : ('_dcm', 'OutRate'),
                  'DCMOutAveRate' : ('_dcm', 'OutRateAvg'),
                  'DCInBW' : ('F_B_MB_conv','_dcm', 'DcDataBW'),
                  'DCOutBW' : ('F_B_MB_conv','_dcm', 'OutputDataBW'),
                  'DCMLVL1ReqEvent' : ('_dcm', 'L1SourceRequestedEvents'),
                  'DCMLVL1RecEvent' : ('_dcm', 'L1SourceReceivedEvents'),
                  'DCMLVL1AckEvent' : ('_dcm', 'L1SourceDoneEvents'),
                  'DCMLVL1ConTO' : ('_dcm', 'L1SourceConnectionTimeouts'),
                  'DCMLVL1ConErr' : ('_dcm', 'L1SourceConnectionErrors'),
                  'DCMDCROBReq' : ('_dcm', 'DcROBRequests'),
                  'DCMDCPendTrans' : ('_dcm', 'DcPendingRequests'),
                  'DCMDCTrafShape' : ('_dcm', 'DcTrafficShapingCredits'),
                  'DCMDCROBRec' : ('F_B_MB_conv','_dcm', 'DcReceivedData'),
                  'DCMDCActCon' : ('_dcm', 'DcActiveConnections'),
                  'DCMDCConTO' : ('_dcm', 'DcConnectionTimeouts'),
                  'DCMDCConErr' : ('_dcm', 'DcConnectionErrors'),
                  'DCMProcReqEvent' : ('_dcm', 'ProxReqEvents'),
                  'DCMProcFetchEvent' : ('_dcm', 'ProxL1Events'),
                  'DCMProcRejEvent' : ('_dcm', 'ProxRejEvents'),
                  'DCMProcAccEvent' : ('_dcm', 'ProxAccEvents'),
                  'DCMProcBusyPU' : ('_dcm', 'ProxBusyPUs'),
                  'DCMProcFreePU' : ('_dcm', 'ProxFreePUs'),
                  'DCMProcDone' : ('_dcm', 'ProxDoneEvents'),
                  'EBEvents' : ('_dcm', 'EbEvents'),
                  'DCMOut' : ('_dcm', 'OutputEvents'),
                  'DCMOutDelE' : ('_dcm', 'OutputDeliveredEvents'),
                  'DCMOutDelD': ('F_B_MB_conv','_dcm', 'OutputDeliveredData'),
                  'DCMOutAC' :('_dcm', 'OutputActiveConnections'),
                  'DCMOutConTO' : ('_dcm', 'OutputConnectionTimeouts'),
                  'DCMOutConErr' :('_dcm', 'OutputConnectionErrors'),
                  'DCMsba': ('_dcm', 'SbaFile'),
                  'DCMcpu' : ('_dcm', 'cpuUsageProc'),
                  'DCMnodeCpu' : ('_dcm', 'cpuUsageNode'),
                  'DCMBusyProcessing' : ('_dcm', 'BusyProcessing'),
                  'DCMBusyFromOutput' : ('_dcm', 'BusyFromOutput'),
                  'DCMEventsInside' : ('_dcm', 'EventsInside'),
                  'DCMEventsOnOutputQueue': ('_dcm', 'EventsOnOutputQueue'),

                  'DCMInMaxBW'    : ('F_DCMMax',),
                  'DCMProcPUs'    : ('_dcmPUs',),
                  'DCMOccOut' : ('F_DCMOccOut',),
                  'DCMOccIn' : ('F_DCMOccIn',),
                  'AvgBusyProcessing' : ('F_AvgBusy', '_dcm', 'BusyProcessing'),
                  'AvgBusyOutput' : ('F_AvgBusy', '_dcm', 'BusyFromOutput'),

                  'HLTTimePercentInProcessing': ('_timePercentInProcessing',),
                  'HLTAverageProcessingTime': ('_averageProcessingTime',),

                  'FreeBufAve' : ('_sfoin','FreeBuffersAverage'),
                  'WaitingEventsAve' : ('_sfoin','WaitingEventsAverage'),
                  'ActiveCon' : ('_sfoin','ActiveConnections'),


                  'EventsInsideAve' : ('_gc', 'EventsInsideAverage'),
                  'EventsLifeTimeAve' : ('_gc','EventsLifeTimeAverage'),
                  'ProcTotEvent' : ('_gc','ProcessingTotalEvents'),
                  'ProcEventRate' : ('_gc','ProcessingEventRate'),
                  'ProcTotData' : ('F_B_MB_conv','_gc','ProcessingTotalData'),
                  'ProcDataRate' : ('F_B_MB_conv','_gc','ProcessingDataRate'),
                  'ProcTasksAve' : ('_gc','ProcessingTasksAverage'),
                  'ProcRunTimeAve' : ('_gc','ProcessingRunTimeAverage'),
                  'ProcLifeTimeAve' : ('_gc','ProcessingLifeTimeAverage'),
                  'WriteTotEvent' : ('_gc','WritingTotalEvents'),
                  'WriteEventsRate' : ('_gc','WritingEventRate'),
                  'WriteTotData' : ('F_B_MB_conv','_gc','WritingTotalData'),
                  'WriteDataRate' : ('_gc','WritingDataRate'),
                  'WriteTasksAve' : ('_gc','WritingTasksAverage'),
                  'WriteRunTimeAve' : ('_gc','WritingRunTimeAverage'),
                  'WriteLifeTimeAve' : ('_gc','WritingLifeTimeAverage'),



                  'SFOSizeAvg'           : ('F_AvgSfoSize',),

                  'SFOdiskUsageList'     : ('_sfoDisks',),
                  'DetectorsList'        : ('_detectors',),
                  'TopLoadList'          : ('F_topLoad',),
                  'TopLoadMem'           : ('F_topMem',),

                  'Streams'              : ('F_streams',),
                  #'BusyList'             : ('F_l1ct',),  #



                   # 'ROIBOutChStatus': ('_roib', 'Output Channel Status'),
                   # 'ROIBInChStatus' : ('_roib', 'Input Channel Status'),
                   # 'BusyROIB'       : ('F_roibbusy',),



                   'SFOReceivedphysRate'  : ('_sfoRecphy','ProcessingEventRate'),
                   'SFOReceivedcalibRate' : ('_sfoReccal','ProcessingEventRate'),
                   'SFOReceiveddebugRate' : ('_sfoRecdeb','ProcessingEventRate'),
                   'SFOReceivedexprRate'  : ('_sfoRecexp', 'ProcessingEventRate'),
                   'SFOReceivedphysBW'    : ('F_B_MB_conv', '_sfoRecphy', 'ProcessingDataRate'),
                   'SFOReceivedcalibBW'   : ('F_B_MB_conv', '_sfoReccal', 'ProcessingDataRate'),
                   'SFOReceiveddebugBW'   : ('F_B_MB_conv', '_sfoRecdeb', 'ProcessingDataRate'),
                   'SFOReceivedexprBW'    : ('F_B_MB_conv', '_sfoRecexp', 'ProcessingDataRate'),
                   'SFOSavedphysRate'     : ('_sfoSavphy', 'WritingEventRate'),
                   'SFOSavedcalibRate'    : ('_sfoSavcal', 'WritingEventRate'),
                   'SFOSaveddebugRate'    : ('_sfoSavdeb', 'WritingEventRate'),
                   'SFOSavedexprRate'     : ('_sfoSavexp', 'WritingEventRate'),
                   'SFOEvSaveRate'        : ('_sfoSavTotal', 'WritingEventRate'),
                   'SFOSavedphysBW'       : ('F_B_MB_conv', '_sfoSavphy', 'WritingDataRate'),
                   'SFOSavedcalibBW'      : ('F_B_MB_conv', '_sfoSavcal', 'WritingDataRate'),
                   'SFOSaveddebugBW'      : ('F_B_MB_conv', '_sfoSavdeb', 'WritingDataRate'),
                   'SFOSavedexprBW'       : ('F_B_MB_conv', '_sfoSavexp', 'WritingDataRate'),
                   'SFOEvSaveBW'          : ('F_B_MB_conv', '_sfoSavTotal', 'WritingDataRate'),

                   'SFORSizePhys'         : ('F_ComputeMeanEvSize', '_sizesSfoR', 'physics'),
                   'SFORSizeCalib'        : ('F_ComputeMeanEvSize', '_sizesSfoR', 'calibration'),
                   'SFORSizeExp'          : ('F_ComputeMeanEvSize', '_sizesSfoR', 'express'),
                   'SFORSizeDebug'        : ('F_ComputeMeanEvSize', '_sizesSfoR', 'debug'),
                   'SFOSSizePhys'         : ('F_ComputeMeanEvSize', '_sizesSfoS', 'physics'),
                   'SFOSSizeCalib'        : ('F_ComputeMeanEvSize', '_sizesSfoS', 'calibration'),
                   'SFOSSizeExp'          : ('F_ComputeMeanEvSize', '_sizesSfoS', 'express'),
                   'SFOSSizeDebug'        : ('F_ComputeMeanEvSize', '_sizesSfoS', 'debug'),



                  ######################################################################
                   'SFOFarm'          : ('F_sfoStr',),                     # SFO
                   'SFOnum'           : ('_supervsfo', '_tot'),
                   'SFOnum_notup'     : ('_supervsfo', '_notup'),
                   'SFOnum_out'       : ('_supervsfo','_out'),
                   #'SFOnum_fault'     : ('_supervsfo','_fault'),
                   'SFOmaxInBW'       : ('F_maxSFOInBW',),
                   'SFOdataBWPerc'    : ('F_sfoBWPerc',),
                   'SFOdiskUsageTop'  : ('F_sfoUsage',),

                  # 'SFOcpuPerc'       : ('_sfoProcAvg','IntervalCPU',),# was in monaIsa - do I still need something like this?


                  }


# Exceptions
class Exception( ers.Issue ):
    def __init__( self, msg, params, cause ):
        ers.Issue.__init__( self, msg, params, cause )

class ISKeyError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Invalid Criteria "%s" ' % msg , {}, cause )

class ISRepositoryNotFound(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Repository Not Found "%s" ' % msg , {}, cause )

class ISInvalidIterator(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Repository Not Found "%s" ' % msg , {}, cause )

class ISAttributeError(Exception):
    def __init__( self, fname, err, cause = None ):
        Exception.__init__( self, 'Problems with function "%s" due to error "%s ' % (fname, err) , {'function_name': fname, 'error':err}, cause )

class MyIOError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Problem: "%s" ' % msg , {}, cause )

class MyRuntimeError(Exception):
    def __init__( self, msg, cause = None ):
        Exception.__init__( self, 'Problem: "%s" ' % msg , {}, cause )


def treatErrors( e, is_entry, attributeName ):
    if attributeName in blackDict:
        blackDict[attributeName] = blackDict[attributeName]+1
        if blackDict[attributeName] == 3:
            if type(e).__name__ == 'ISAttributeError':
                ers.warning ( e )
            else:
                mrs_msg = ers.Issue( "Problem: '%s'" %e, {}, None)
                ers.warning(mrs_msg)
        elif blackDict[attributeName] == 30:
            if type(e).__name__ == 'ISAttributeError':
                ers.error ( e )
            else:
                mrs_msg = ers.Issue( "Problem: '%s'" %e, {}, None)
                ers.error(mrs_msg)
    else:
        blackDict[attributeName] = 1



def rootControllerState(partition):
    part = IPCPartition(partition)
    if not part.isValid():
        ers.log( '%s is not a valid partition name' % part_name)
        sys.exit(1)

    state = None
    try:
        r = ISObject(part,'RunCtrl.RootController','RCStateInfo')
        r.checkout()
        state = r.state
    except:
        pass

    return state


# Extend the Summer class providing a dump to string
class Xummer(Summer):
    def summary(self):
        s = [ "%s(%d)" % (self.prefix, len(self.objects))]
        for attr in self.attributes:
            t = " sum.%s.%s" % (self.prefix, attr)
            s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
        s.append("\n")
        for attr in self.averages:
            t = " avg.%s.%s" % (self.prefix, attr)
            s.append("\n %-35s: %12.2f" % (t, getattr(self,attr)))
        return ''.join(s) #puts strings in s together with no space

# Statistics of the RunCtrl.Supervision
class superv(object):
    _tot, _notup, _out, _fault = 0, 0, 0, 0
    def reset(self):
        self._tot, self._notup, self._out, self._fault = 0, 0, 0, 0
    def upd(self, y, theSet=None):
        self._tot += 1
        if not y.membership: self._out +=1
        if y.status     != 'UP' : self._notup += 1
        if theSet != None:
            h=y.host.replace('.cern.ch','')
            if   y.status == 'UP': theSet.add(h)
            else:
                try: theSet.remove(h)
                except(KeyError): pass
    def dump(self):
        return '%6d %6d %6d %6d' % (self._notup, self._out, self._fault, self._tot)

# Check ROL enable/disable state

from itertools import chain
from operator import attrgetter

#Get new!
det_folder2det_id = {'FORWARD':(0x81,0x84),
                     'L1CALO':(0x71,0x75),
                     'LAR':(0x41,0x48),
                     'MUON_MDT':(0x61,0x64),
                     'MUON_RPC':(0x65,0x66),
                     'MUON_CSC':(0x69,0x6a),
                     'MUON_TGC':(0x67,0x68),
                     'PIXEL':(0x11,0x13),
                     'SCT':(0x21,0x24),
                     'TDAQ_CTP':(0x77,0x77),
                     'SCT':(0x21,0x24),
                     'TDAQ_MUON_CTP':(0x76,0x76),
                     'TILE':(0x50,0x54),
                     'TRT':(0x30,0x34)}


det_id2det_folder = dict(chain(*[zip(det_ids,[folder]*len(det_ids)) for det_ids,folder in ((range(v[0],v[1]+1),folder) for (folder,v) in list(det_folder2det_id.items()))]))


def rolenabled(partition, rosid, id, rdb):

    part = IPCPartition(partition)

    ros = rdb.getObject('ROS',rosid)# returns oks object of name ROS, type rosid

   # print "ROSID!", rosid
   # print "ID", id
   # print "detector", detector

    #chain is make an iterator that goes through first list until exhausted, then goes through second list
    #attrgetter creates a function f, so f(r) returns r.Contains. then imap calls that function on the list of robins from ros.Contains.

    rolid = [rol.id for rol in chain(*map(attrgetter('Contains'),ros.Contains))if rol.Id in id][0]
    detector = int(rolid.split('-')[-1],16) >> 16

#  robinList = [robin for ros.Contains]
#  rolid = [rol.id for rol in (robin in robinList)
#  for robin in ros.Contains:
#    for rol in robin.Contains:
#      if rol.Id in idlist:
#        rolid = rol.id[0]

#  print "ROLID",rolid
    folder = det_id2det_folder.get(detector,'UNKNOWN')
    if folder == 'UNKNOWN':
        print("unknown folder in rolenabled:",detector)

    isinfo = 'Resources.Enabled:%s/Robins/%s' % \
             (folder, rolid)

    # resenabled = ISObject(part, isinfo, 'EnabledResource')

    try:
        d = ISInfoDictionary(part)
        return d.contains(isinfo)
    except:
        return False


def maxOutputEvents(partition): #should I have type taken as an input parameter?
    part = IPCPartition(partition)
    rdb = pm.project.Project('rdbconfig:RDB@%s' % partition)

    dcm = rdb.getObject('DcmApplication','DCM')
    maxOutEvents = dcm.processor.maxOutputEvents
    return maxOutEvents

# Class Worker
class Worker(object):
    """Class gathering info from IS"""

    # Configuration
    _isDelay    =     5      # Is update delay in s
    _isDelayFact=    20      # Is update delay multiplication factor for slow servers

    _rc         = ISInfoAny();         _rp          = ISInfoAny()
    _lb         = ISInfoAny();         _gb          = ISInfoAny()
    _roib       = ISInfoAny();         _gc          = ISInfoAny()
    _sfoProcAvg = ISInfoAny();         _dcm         = ISInfoAny()
    _hltsv      = ISInfoAny();         _sfoin       = ISInfoAny()
    _sfoDisks   = [];                  _detectors   = ""
    _top10mem   = [];                  _top10load   = []
    _ctpcorebusy= 0;                   _daqbusy     = 0
    _sizesSfoS  = {}; _sizesSfoR = {};
    _magnets    = {}
    _supervsfo  = superv();
    _nSfos      = 0;                   _roibOut     = ''
    _streams    = {};
    _hotRobinOcc= 0;                   _hotRobinName= "";
    _hotRosRate = 0;                   _hotRosName  = ""
    _highRosLoadName = "";             _highRosLoad = 0
    _busyRols   = [];
    _stableBeam = False;               _1stLoop     = True
    _beamMode   = [];                  _forceUpdate = False
    _instLumi   = 0;                   _mu          = 0;
    _verbose    = False;
    _counter    = 0;
    _dcmPUs = 0;                       _numracks = 0;
    _maxEvent = 0;                     _numdcm = 0;

    _v = [0,'']
    _tkeys      = { 'sm':   _v,  'l1':   _v, 'hlt':  _v, 'l1bg':  _v,
                    'l1.p': _v, 'hlt.p': _v, 'l1.s': _v, 'hlt.s': _v }
    _ctpcout12  = ['LUCID', 'PIXEL', 'SCT'     , 'TRT'      , 'CALO']
    _ctpcout13  = ['BCM'  , 'ZDC'  , 'LAR_FCAL', 'LAR_EM_E' , 'LAR_EM_B']
    _ctpcout14  = ['LHCf' , 'MDT_B', 'MDT_E'   , 'TILECAL_B', 'TILECAL_E']
    _ctpcout15  = ['CSC'  , 'ALFA', 'TGC'     , 'RPC'      , 'MUON_CTP']


#  def readRDB(self):
#      ers.log( '\nReading RDB ....')
#      t0 = time.time()
#      db = pm.project.Project('rdbconfig:RDB@%s' % (part_name))

#      t1 = time.time()

        #ers.log( '\nSFI input buffer : %d\nSFI output buffer: %d\nDFM input buffer : %d\nL2SV l2 queue sz : %d\nL2RH store Size  : %d\nReading RDB done [%4.2f s]' % ( self._conf_sfi.MaxAssignEvents, self._conf_sfi.MaxOutputQueueSize, self._conf_dfm.MaxInputQueueSize, self._conf_l2sv.l2puQueueSize, self._conf_l2rh.storeSize, (t1-t0) ) )



    def __init__(self, part_name):
        self._p       = IPCPartition(part_name) # Create partition object
        if not self._p.isValid():
            ers.log('%s is not a valid partition name' % part_name)
            sys.exit(1)

        self._pi      = IPCPartition('initial')
        if not self._pi.isValid():
            ers.log('initial is not a valid partition name')
            sys.exit(1)

        self._pOLC      = IPCPartition('OLC')
        if not self._pOLC.isValid():
            ers.log('OLC is not a valid partition name')

        # This will read the RDB configuration
        # we assume the server is stateless and started at SoR
        # so there is no need to refresh this information
        self._maxEvent = maxOutputEvents(part_name)

        # self.readRDB()

        # Data members

        # self._sfo  = Xummer('SFO', self._p, [ 'DF' ], 'SFO-[0-9]+',
        # [ 'CurrentDataReceivedRate','CurrentDataSavedRate',
        #   'CurrentEventReceivedRate','CurrentEventSavedRate', 'EventsReceived', 'EventsSaved' ])

        self._sfoReccal = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.calibration_.*', ['ProcessingEventRate', 'ProcessingDataRate'] )
        self._sfoRecphy = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.physics_.*', ['ProcessingEventRate','ProcessingDataRate'] )
        self._sfoRecexp = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.express_.*', ['ProcessingEventRate','ProcessingDataRate'] )
        self._sfoRecdeb = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.debug_.*', ['ProcessingEventRate','ProcessingDataRate'] )


        self._sfoSavcal = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.calibration_.*', ['WritingEventRate', 'WritingDataRate'] )
        self._sfoSavphy =Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.physics_.*', ['WritingEventRate', 'WritingDataRate'] )
        self._sfoSavexp = Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.express_.*', ['WritingEventRate', 'WritingDataRate'] )
        self._sfoSavdeb =Xummer('SFOngCounters', self._p, [ 'DF' ], 'TopMIG-IS:HLT.Counters.debug_.*', ['WritingEventRate', 'WritingDataRate'] )

        self._sfoSavTotal = Xummer('SFOngCounters', self._p, ['DF'], 'TopMIG-IS:HLT.Counters.Global', [ 'WritingEventRate', 'WritingDataRate'] )


    def updateRCState(self):
        try:
            self._rc = ISObject(self._p, 'RunCtrl.RootController', 'RCStateInfo')
            self._rc.checkout()
        except ers.Issue as e:
            # caught if in the constructor you pass a wrong ISServer name
            if e.__class__.__name__ == "RepositoryNotFound":
                raise ISRepositoryNotFound(e)
            elif e.__class__.__name__ == "InvalidIterator":
                raise ISInvalidIterator(e)
        except KeyError as e:
            raise ISKeyError(e)
        except AttributeError as e:
            raise ISAttributeError('updateRC', e)

    def updateDCM(self):
     # it = ISInfoIterator(self._p, 'DF_IS:HLT', ISCriteria('DefMIG-IS:HLT-.*.info'))
        #NumRacks= sum(1 for i in it)
        #print "NUM RACKS 1", NumRacks

        #xs=Xummer('DCM', self._p, ['DF_IS:HLT-.*'], 'DCM:HLT-.*', [ 'BusyProcessing', 'BusyFromOutput'] )
        #print "SUMMER"
        #xs.dump()
        #xs.summary()

        it = ISServerIterator(self._p)
        numRacks = 0
        numDCM =0

        timePercentInProcessing = 0.0
        averageProcessingTime   = 0.0

        while it.next():
            if "DF_IS:HLT-" in it.name():
                numRacks+=1
                ii = ISInfoIterator(self._p, it.name(), ISCriteria('DCM.*:HLT-.*'))
                numDCM += sum(1 for i in ii)

                # Collect HLTMPPU times
                s = ISInfoStream(self._p, it.name(), ISCriteria('HLTMPPU-.*:HLT-.*:tpu-rack-.*:pc-tdq-tpu-.*.PU_ChildInfo'), False, 1)
                hltmppu = ISInfoDynAny()
                while not s.eof():
                    s.get(hltmppu)
                    timePercentInProcessing += hltmppu.TimePercentInProcessing
                    averageProcessingTime   += hltmppu.AverageProcessingTime

        self._numdcm = numDCM
        self._numracks = numRacks

        if self._dcmPUs > 0:
            self._timePercentInProcessing = old_div(timePercentInProcessing, self._dcmPUs)
            self._averageProcessingTime   = old_div(averageProcessingTime, self._dcmPUs)
        else:
            self._timePercentInProcessing = 0.0
            self._averageProcessingTime   = 0.0

        #print "NUM RACKS 2",  numRacks
        #print "NUM DCM", numDCM

    def updateRC(self):
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            self._supervsfo.reset()

    #  y  = ISInfoAny()

        self._rc = ISObject(self._p, 'RunCtrl.RootController', 'RCStateInfo')
        self._rc.checkout()

        if ((self._counter % self._isDelayFact) == 1 or self._1stLoop) :  # Parse supervisors occasionally
            it = ISInfoIterator(self._p, 'RunCtrl',ISCriteria('Supervision.SFO-.*'))
            while it.next():
                try:
                    y = ISInfoDynAny()
                    it.value(y)
                    self._supervsfo.upd(y)
                except AttributeError as e:
                    raise ISAttributeError('updateRC', e)


    def updateRP(self):
        try:
            self._rp = ISObject(self._p, 'RunParams.RunParams', 'RunParams')
            self._rp.checkout()
        except:
            print("no RP")
            pass

        try:
            self._lb = ISObject(self._p, 'RunParams.LumiBlock', 'LumiBlock')
            self._lb.checkout()
        except:
            print("no LB")
            pass

        try:
            self._gb = ISObject(self._p, 'RunParams.GlobalBusy', 'GLOBALBUSY')
            self._gb.checkout()
        except:
            pass


        try:
            x = ISObject(self._p, 'RunParams.TrigConfSmKey', 'TrigConfSmKey')
            x.checkout()
            self._tkeys['sm']   = [x.SuperMasterKey, x.SuperMasterComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.TrigConfL1BgKey', 'TrigConfL1BgKey')
            x.checkout()
            self._tkeys['l1bg'] = [x.L1BunchGroupKey, x.L1BunchGroupComment]
        except:
            pass


        try:
            x = ISObject(self._p, 'RunParams.TrigConfL1PsKey', 'TrigConfL1PsKey')
            x.checkout()
            self._tkeys['l1']   = [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.TrigConfHltPsKey', 'TrigConfHltPsKey')
            x.checkout()
            self._tkeys['hlt']  = [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.Physics.L1PsKey', 'TrigConfL1PsKey')
            x.checkout()
            self._tkeys['l1.p'] = [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.Physics.HltPsKey', 'TrigConfHltPsKey')
            x.checkout()
            self._tkeys['hlt.p']= [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.Standby.L1PsKey', 'TrigConfL1PsKey')
            x.checkout()
            self._tkeys['l1.s']= [x.L1PrescaleKey, x.L1PrescaleComment]
        except:
            pass

        try:
            x = ISObject(self._p, 'RunParams.Standby.HltPsKey', 'TrigConfHltPsKey')
            x.checkout()
            self._tkeys['hlt.s'] = [x.HltPrescaleKey, x.HltPrescaleComment]
        except:
            pass


        if(self._counter % self._isDelayFact) == 1:
            self._detectors = ""
            detmask = helper.DetectorMask(self._rp.det_mask)
            "DETECTORS", detmask
            for d in detmask.sub_detectors():
                self._detectors += ( str(d).replace('_SIDE','') + '\n')


    def updateSums(self):
        ht0 = time.time()
        '''
        self._dfm.update()
        self._l2sv.update()
        self._l2svm.update()
        self._l2rh.update()
        '''
        ht1 = time.time()
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            self._sfoRecphy.update()
            self._sfoReccal.update()
            self._sfoRecexp.update()
            self._sfoRecdeb.update()

            '''
            print "cal"
            self._sfoReccal.dump()
            self._sfoReccal.summary()
            print "phs"
            self._sfoRecphy.dump()
            print "express"
            self._sfoRecexp.dump()
            self._sfoRecexp.summary()
            print "debug"
            self._sfoRecdeb.dump()
            '''
            self._sfoSavphy.update()
            self._sfoSavcal.update()
            self._sfoSavexp.update()
            self._sfoSavdeb.update()
            self._sfoSavTotal.update()


        ht2 = time.time()
        ht3 = time.time()
        ht4 = time.time()
        ht5 = time.time()
        if self._verbose:
            ers.log( 'SUM times:\ndfm/l2sv/l2rh meanEvSize sfi/sfo/efd procTime busy/l2exp\n%3.2f %3.2f %3.2f %3.2f %3.2f' % (ht1-ht0, ht2-ht1, ht3-ht2, ht4-ht3, ht5-ht4))


    def updateDF(self):
        self._streams      = {};  busych = {}
        self._hotRobinOcc  = 0;   self._hotRobinName = ""
        self._hotRosRate   = 0;   self._hotRosName   = ""
        self._highRosLoad  = 0;   self._highRosLoadName = ""
        self._highxoff = 0;


        saveRates = {}


        try:
            it = ISInfoIterator(self._p, 'DF', ISCriteria('.*'))
        except ers.Issue as e:
            # caught if in the constructor you pass a wrong ISServer name
            if e.__class__.__name__ == "RepositoryNotFound":
                ers.error(e)
        except KeyError as e:
            raise ISKeyError(e)

        x = ISInfoAny()
        try:
            while it.next():
                if it.name().startswith('DF.RoIB.RoIB_status'):
                    it.value(self._roib); self._roib._update(it.name(), self._p)
                    self._roibOut = re.split("\n", str(self._roib))[7]
                    # this is for listing all streams active in a run


                elif it.name().startswith('DF.ROS.ROS-'):
                    if it.name().find('.ReadoutModule') > 0:
                        it.value(x)
                        x._update(it.name(), self._p)
                        for item in range(len(x.rolEnabled)):
                            if x.rolEnabled[item] == 0: continue
                        #  if item != 0:
                            y = float(x.pagesInUse[item]) / float(x.pagesInUse[item] + x.pagesFree[item])
                            if y > self._hotRobinOcc :
                                self._hotRobinOcc  = y*100
                                self._hotRobinName = it.name().replace('DF.ROS.', '')
                            if x.xoffPercentage[item] > self._highxoff:
                                self._highxoff = x.xoffPercentage[item]
                            # if x.rolXoffStat[item]:
                            #  busych[it.name().replace('DF.ROS.', '')] = x.robId[item]


                    if it.name().endswith('TriggerIn0'):
                        it.value(x)
                        x._update(it.name(), self._p)
                        if x.requestRate > self._hotRosRate:
                            self._hotRosRate = x.requestRate
                            self._hotRosName = it.name().replace('DF.ROS.', '')
                            self._hotRosName = self._hotRosName.replace('.TriggerIn0', '')
                    # if x.numberOfQueueElements > 30:
                    #   if x.rosLoad > 70:
                    #     self._highRosLoadName = it.name().replace('DF.ROS.', '')
                    #     self._highRosLoad     = x.rosLoad


                elif it.name().startswith('DF.HLTSV.Events'):
                    self._hltsv = ISObject(self._p, 'DF.HLTSV.Events', 'HLTSV')
                    self._hltsv.checkout()


                elif it.name().startswith('DF.TopMIG-IS:HLT.info'):
                    self._dcm = ISObject(self._p, 'DF.TopMIG-IS:HLT.info', 'DCM')
                    self._dcm.checkout()

                elif it.name().startswith('DF.TopMIG-IS:HLT.PU_MotherInfo'):
                    self._hltpu = ISObject(self._p, 'DF.TopMIG-IS:HLT.PU_MotherInfo','HLTMPPUMotherInfo')
                    self._hltpu.checkout()


                elif it.name().startswith('DF.TopMIG-IS:HLT.Input'):
                    self._sfoin = ISObject(self._p, 'DF.TopMIG-IS:HLT.Input', 'SFOngInput')
                    self._sfoin.checkout()


                elif it.name().startswith('DF.TopMIG-IS:HLT.Counters'):
                    if re.search('Global',it.name()):
                        self._gc = ISObject(self._p, 'DF.TopMIG-IS:HLT.Counters.Global', 'SFOngCounters')
                        self._gc.checkout()


                    else:
                        it.value(x); x._update(it.name(), self._p)
                        s = it.name().replace('DF.TopMIG-IS:HLT.Counters.','')
                        if s in self._streams:
                            self._streams[s] += x.WritingEventRate
                        else:
                            self._streams[s]  = x.WritingEventRate

            self._dcmPUs = getattr(self._dcm, 'ProxBusyPUs')+getattr(self._dcm, 'ProxFreePUs')


     # Check if the busy channel are disasbled
            self._busyRols = []
            # rdb = pm.project.Project('rdbconfig:RDB@%s' % part_name)
            # for ch, v in busych.iteritems():
            #   try:
            #     if rolenabled(part_name, ch.split('.',1)[0], v, rdb):
            #       self._busyRols.append(ch)
            #     # print "BUSY",self._busyRols
            #   except(KeyError, RuntimeError): self._busyRols.append(ch + '_??')

        except AttributeError as e:
            raise ISAttributeError('updateDF', e)



    def updateMeanEvSizeR(self, path):
        StreamEvDict = {}

        it = ISInfoIterator(self._p, 'DF', ISCriteria(path))
        while it.next():
            if it.name().find("_") == -1: #find location of _ in string. if no _, return -1. This should be only for counters.global (which I already have), otherwise the stream names have an undrescore
                streamName = " "
            else:
                streamName = it.name().rpartition(".")[2] #physics_####
            if streamName != " ":
                streamType = streamName.split("_")[0]#physics
#        print "streamname", streamName, streamName.rpartition("_"), "_" in streamType
                any = ISInfoAny()
                it.value(any)
                any._update(it.name(), self._p)
                rate = getattr(any, 'ProcessingEventRate')
                try:
                    bandwidth = getattr(any,'ProcessingDataRate')
            #    print "thing", streamType, rate, bandwidth
                    evnSize = float(bandwidth)/float(rate)
                except ZeroDivisionError:
                    evnSize = 0

                if evnSize != 0:
                    if not streamType in list(StreamEvDict.keys()):
                        StreamEvDict[streamType] = [evnSize, 1]
                    else:
                        StreamEvDict[streamType][0] = StreamEvDict[streamType][0] + evnSize
                        StreamEvDict[streamType][1] = StreamEvDict[streamType][1] + 1
#    print "STREAM", StreamEvDict
        return StreamEvDict


    def updateMeanEvSizeS(self, path):
        StreamEvDict = {}

        it = ISInfoIterator(self._p, 'DF', ISCriteria(path))
        while it.next():
            if it.name().find("_") == -1:
                streamName = " "
            else:
                streamName = it.name().rpartition(".")[2]
            if streamName != " ":
                streamType = streamName.rpartition("_")[0]
                any = ISInfoAny()
                it.value(any)
                any._update(it.name(), self._p)
                rate = getattr(any, 'WritingEventRate')
                try:
                    bandwidth = getattr(any,'WritingDataRate')
                    evnSize = old_div(bandwidth,rate)
                except ZeroDivisionError:
                    evnSize = 0

                if evnSize != 0:
                    if not streamType in list(StreamEvDict.keys()):
                        StreamEvDict[streamType] = [evnSize, 1]
                    else:
                        StreamEvDict[streamType][0] = StreamEvDict[streamType][0] + evnSize
                        StreamEvDict[streamType][1] = StreamEvDict[streamType][1] + 1
        return StreamEvDict

    def updateMeanFragSize(self, path): #will be added in later with dcm
        tot_frag_detId=[]
        tot_frag_size=[]
        tot_collection={}

        it = ISInfoIterator(self._p, 'DF', ISCriteria(path))
        while it.next():
            any = ISInfoAny()
            it.value(any)
            any._update(it.name(), self._p)
            tot_frag_detId = any.__getattribute__('SouceID')
            tot_frag_size = any.__getattribute__('Size')
            counter = 0
            for entry in tot_frag_detId[:]:
                if not entry in list(tot_collection.keys()):
                    tot_collection[entry] = [tot_frag_size[counter], 1]
                else:
                    tot_collection[entry][0] = tot_collection[entry][0] + tot_frag_size[counter]
                    tot_collection[entry][1] = tot_collection[entry][1] + 1
                    counter = counter + 1
        return tot_collection


    #need to ask someone about, not correct now
    def updateL1CT(self):
        return

    def updatePmg(self):
        if (self._counter % self._isDelayFact) == 1 or self._1stLoop:
            self._top10mem,     self._top10load, self._sfoDisks   = [], [], []
            nSfos = 0
            it = ISInfoIterator(self._p, 'PMG', ISCriteria('AGENT_.*')); it.next()
            x = ISInfoAny()
            while it.next():
                if it.name().startswith('PMG.AGENT_'):
                    it.value(x); x._update(it.name(), self._p)
                    h = it.name().replace('PMG.AGENT_', '').replace('.cern.ch','')#pc-tdq-onl-56
                    self._top10mem.append( '%5d%s [%3d%s] %s' % (x.ram, '%', x.swap, '%', h) )
                    self._top10load.append('%6.1f %s'         % (x.load1, h))
                    if h.startswith('pc-tdq-sfo-'):
                        nSfos += 1
                        l = x.fs.split("* *")
                        l2 = [par for par in l if "raid_cntl" in par]
                        for parp in l2:
                            self._sfoDisks.append('%3s %s:%s' % (parp.split()[1],  h, parp.split()[0] ))


            self._nSfos = nSfos
            self._sfoDisks.sort()
            self._sfoDisks.reverse()

            if self._1stLoop:
                self._1stLoop = False


    def updateIni(self):
        try:
            sol = ISObject(self._pi, 'DCS_GENERAL.MagnetSolenoidCurrent.value','DdcFloatInfo')
            sol.checkout()
            self._magnets['solenoid'] = sol.value

            tor = ISObject(self._pi, 'DCS_GENERAL.MagnetToroidsCurrent.value','DdcFloatInfo')
            tor.checkout()
            self._magnets['toroid'] = tor.value

            r = ISObject(self._pi, 'LHC.StableBeamsFlag','DdcIntInfo')
            r.checkout()
            self._stableBeam = r.value
            r = ISObject(self._pi, 'LHC.BeamMode','DdcStringInfo')
            r.checkout()
            self._beamMode = r.value
        except:
            pass

    def updateOLC(self):
        if not self._pOLC.isValid():
            return

        try:
            r = ISObject(self._pOLC, 'OLC.OLCApp/ATLAS_PREFERRED_LBAv_PHYS', 'OCLumi')
            r.checkout()
            self._instLumi = r.CalibLumi
            self._mu =  r.Mu
        except KeyError as e:
            raise ISKeyError(e)
        except AttributeError as e:
            raise ISAttributeError('updateOLC', e)
        except  ers.Issue as e:
            if e.__class__.__name__ == "RepositoryNotFound":
                raise ISRepositoryNotFound(e)



    # Read from IS
    def Update(self, mode='long'):
        """Update ISInfo objects"""

        self._counter += 1

        tt=[time.time()]
        # 1. Update from initial partition: Magnets and stable beam
        try:
            self.updateIni()
            tt.append(time.time())
        except (ISKeyError,ISAttributeError,ISRepositoryNotFound) as e:
            ers.warning(e)

        # 2. Update from OLC partition: instantaneous luminosity & Mu--put back in at P1
        try:
            self.updateOLC()
            tt.append(time.time())
        except (ISKeyError,ISAttributeError,ISRepositoryNotFound) as e:
            ers.warning(e)

        # 3. Update RunParams.RunParams
        try:
            self.updateRP()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # If not running skip the other updates
        if mode=='short':
            return

        # 4. Update RunCtrl
        try:
            self.updateRC()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 5. Update summers
        try:
            self.updateSums()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 6. Update DF: ROIB, Sizes, BusyRols
        try:
            self.updateDF()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        #DCMs
        try:
            self.updateDCM()
            tt.append(time.time())
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)

        # 7. Update updatemeanEvSize & FragmentSizes
        try:
            self._sizesSfoR = self.updateMeanEvSizeR('TopMIG-IS:HLT.Counters.*')
            self._sizesSfoS = self.updateMeanEvSizeS('TopMIG-IS:HLT.Counters.*')

        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)
        tt.append(time.time())

        # 9. Update L1CT (current CTP busy status)
        try:
            self.updateL1CT()
        except ISKeyError as e:
            ers.warning(e)

        # 10. Update OS info form PMG; do it at a low rate (every 5 loops)
        tt.append(time.time())
        try:
            self.updatePmg()
        except (ISKeyError, ISRepositoryNotFound) as e:
            ers.warning(e)
        tt.append(time.time())

        t = time.time()
        s = ''
        for i in range(1, len(tt)):
            s += '%3.2f ' % (tt[i]-tt[i-1])
        o = "[%5d]: %4.2f s (%s)" % (self._counter, t-tt[0] , s)
        if self._verbose:
            ers.log('%s' % o)



# ------ Functions -----

    def F_l1ct(self):
        out = ['From L1CT History\n']
        dis = ['Disabled(?)\n']
        try:
            out.append('%6.1f%s %-10s [%s]\n' % (self._ctpcorebusy, '%', 'Global', 'ctpcore_moni0_rate'))
            out.append('%6.1f%s %-10s [%s]\n' % (self._daqbusy,     '%', 'DAQ',    'ctpcore_mon_rate'))

            for i in range(len(self._ctpcout12)):
                d = self._ctpcout12[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_12[i], '%', d, 'ctpcout12')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout13)):
                d = self._ctpcout13[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_13[i], '%', d, 'ctpcout13')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout14)):
                d = self._ctpcout14[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_14[i], '%', d, 'ctpcout14')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            for i in range(len(self._ctpcout15)):
                d = self._ctpcout15[i]
                s = '%6.1f%s %-10s [%s]\n' % (self._l1ctH.ctpout_15[i], '%', d, 'ctpcout15')
                if self._detectors.find(d[:3]) >= 0: out.append(s)
                else                    : dis.append(s)
            out.sort(); out.reverse()
            dis.sort(); dis.reverse()
        except AttributeError as e:
            raise ISAttributeError('F_l1ct', e)
        return out + dis

    def F_streams(self):
        out = []
        for k, v in self._streams.items():
            d = self._gc.WritingEventRate
            p = 0.
            if d > 0: p = old_div(100*v,d)
            out.append("%6.1f Hz  %6.1f %s   %s" % (v, p, '%',k))
        out.sort()
        out.reverse()
        return out[:50] #fit in tool tip


    def F_topMem(self):
        self._top10mem.sort()
        self._top10mem.reverse()
        return self._top10mem[:10]

    def F_topLoad(self):
        self._top10load.sort();
        self._top10load.reverse()
        return self._top10load[:10]

    def F_runtime_s(self):
        try: #EOR = end of run time, SOR = start of run time. Returns number of seconds since 00:00:00 UTC 1/1/1970. OWLTime is current time
            if self._rp.timeEOR.c_time() : diffsec =  self._rp.timeEOR.c_time() - self._rp.timeSOR.c_time()
            else                         : diffsec =  OWLTime().c_time() - self._rp.timeSOR.c_time()
            return diffsec
        except AttributeError as e:
            raise ISAttributeError(' F_runtime_s', e)

    def F_runStr(self):
        try:
            diffsec = self.F_runtime_s()
            h, m  = old_div(diffsec,3600), old_div((diffsec%3600),60)
            s = "LB@Run: %d@%d   %dh:%dm" % (self._lb.LumiBlockNumber, self._lb.RunNumber, h, m)
            return s
        except AttributeError as e:
            raise ISAttributeError('F_runStr', e )

    def F_sfoBWPerc(self):
        v = self._gc.WritingEventRate
        try: p = float(v)/ self.F_maxSFOInBW()
        except (ZeroDivisionError): p = 0
        return p * 100


    def F_DCMMax(self):
        return self._numracks * 20000.

    def F_Perc(self, param1, param2):
        #v = getattr(self, param1)
        #t = getattr(self, param2)
        try: p = float(param1)/float(param2)
        except (ZeroDivisionError): p = 0
        return p * 100

    def F_HLTOccPerc(self):
        v = self._dcmPUs-self._hltsv.AvailableCores
        t = self._dcmPUs
        return self.F_Perc(v, t)

    def F_HLTOcc(self):
        return self._dcmPUs-self._hltsv.AvailableCores

    def F_DCMOccOut(self):
        v = self._dcm.OutputEvents - self._dcm.OutputDeliveredEvents
        t = self._maxEvent*self._numdcm
        return self.F_Perc(v,t)

    def F_DCMOccIn(self):
        v = self._dcm.ProxBusyPUs
        t = self._dcmPUs
        return self.F_Perc(v,t)

    def F_AvgBusy(self, dic, param1):
        if type(getattr(self,dic)).__name__=='dict':
            try:
             # print "AVGBUSY", getattr(self,dic)[param1], self._numdcm
                return self.F_Perc(getattr(self,dic)[param1],self._numdcm)
            except AttributeError as e:
                raise ISAttributeError('F_AvgBusy', e)
        else:
            try:
                #print "AVGBUSY", getattr(getattr(self,dic), param1), self._numdcm
                return self.F_Perc(getattr(getattr(self, dic), param1), self._numdcm )
            except AttributeError as e:
                raise ISAttributeError('F_AvgBusy', e)

    def F_dfStr(self):
        bProcessing = self.F_AvgBusy('_dcm', 'BusyProcessing')
        bOutput = self.F_AvgBusy('_dcm', 'BusyFromOutput')
     # print "BUSYPROC",bProcessing, bOutput
     # if bProcessing < 0.2 and bOutput < 0.2:
     #   return ""
        if bProcessing > 0.8 and bOutput < 0.2:
            return "Backpressure from CPU"
        if bProcessing < 0.2 and bOutput > 0.8:
            return "Backpressure from SFOs"
        if bProcessing > 0.8 and bOutput > 0.8:
            return "UNSTABLE!"
        else:
            return ""

    def F_sfoStr(self):
        s =  "%3d SFOs, max input B/W: %d MB/s" % (self._supervsfo._tot-self._supervsfo._out, self.F_maxSFOInBW())
        return s

    def F_ComputeMeanEvSize(self, streamDict_str, streamType):
        streamDict = getattr(self, streamDict_str)
        mean = 0
        if streamType in streamDict:
            try:
                mean = (old_div(streamDict[streamType][0], streamDict[streamType][1]))
            except ZeroDivision:
                mean = 0
    #  print mean, streamDict[streamType][0], streamDict[streamType][1], streamType
     # if streamDict_str == '_sizesSfi':
     #   mean = mean * 1000.
        return mean

    def F_ComputeMeanFragSize(self, id_frag_Dict_str, type):
        id_frag_Dict = getattr(self, id_frag_Dict_str)
        tot_id = []
        tot_size = []
        for entry in id_frag_Dict.keys():
            tot_id.append(entry)
            mean =  old_div(id_frag_Dict[entry][0],id_frag_Dict[entry][1])
            tot_size.append(mean)
        if type == 'id':
            return tot_id
        elif type == 'size':
            return tot_size

    def F_AvgSfoSize(self): #correct?
        try:
            v = (1000.*self._gc.WritingEventRate)/float(self._gc.ProcessingEventRate)
            #(1000.*self._gc['WritingEventRate']) / float(self._gc['ProcessingEventRate'])
        except (ZeroDivisionError): v = 0
        return v

    def F_hltStr(self):
        try:
            s = "SMK %d, BG %d, PS %d/%d [phys %d/%d, standby %d/%d]" % (
              self._tkeys['sm'][0],
              self._tkeys['l1bg'][0],
              self._tkeys['l1'][0],
              self._tkeys['hlt'][0],
              self._tkeys['l1.p'][0],
              self._tkeys['hlt.p'][0],
              self._tkeys['l1.s'][0],
              self._tkeys['hlt.s'][0],
              )
            return s
        except AttributeError as e:
            raise ISAttributeError('F_hltStr', e )

    def F_roibbusy(self): #find if there's a 1 in busy output channel status. If there is, return 1. Else, return 0
        s = ""
        try: s = re.split(":", self._roibOut )[1].replace(' ----','')
        except(IndexError): s = "??"
        p = 0
        if s.find('1') > 0: p=1
        return p

    def F_sfoUsage(self):
        try:
            v = int(self._sfoDisks[0].split('%')[0])
        except(IndexError): v = 0
        return v

    #check
    def F_transfer2t0Perc(self):
        run = self._lb.RunNumber
        p = 0.
        try:
            local_sfo_fetch = SFOFetch(1, 'online') #some db thing
            nr_closed = local_sfo_fetch.nr_closed_files([run]) #returns a list containing the number of closed files for each run in the input list - runs
            nr_trans = local_sfo_fetch.nr_transferred_files([run])#returns a list contianting the number of transferred files for each run in the input list-runs
            p = 1.*nr_trans[0]/nr_closed[0]
        except (TypeError, ZeroDivisionError):
            p = 0.
        except (RuntimeError,NameError) as e:
            raise MyRuntimeError(e)
        return p*100


    def F_setMagnets(self, magnet_name, nominal_value):
        try:
            return (old_div(100*self._magnets[magnet_name],nominal_value))
        except AttributeError as e:
            raise ISAttributeError(' F_setMagnets', e)
        except KeyError as e:
            raise ISKeyError(e)


    def F_len(self, param):
        try:
            return len(getattr(self, param))
        except AttributeError as e:
            raise ISAttributeError(' F_len', e)


    def F_maxSFOInBW(self):
        return (self._supervsfo._tot - self._supervsfo._out) * maxSFOBW


    def F_getOKSparam(self, *args):
        try:
            return getattr(getattr(self, args[0]), args[1])
        except AttributeError as e:
            raise ISAttributeError('F_getOKSparam', e)


    def F_B_MB_conv(self, dic, param1):
        if type(getattr(self,dic)).__name__=='dict':
            try:
                return getattr(self,dic)[param1]/1000000.
            except AttributeError as e:
                raise ISAttributeError('F_B_MB_conv', e)
        else:
            try:
                return getattr(getattr(self, dic), param1) / 1000000.
            except AttributeError as e:
                raise ISAttributeError('F_B_MB_conv', e)

    def Publish(self, dictionary):
        for entry in dictionary.items(): #returns an iterator of tuples (key, value)
            try:
            #  print 'CHECK!', type(getattr(self, entry[1][0])).__name__, entry[0]
            #  print '1', entry[0]
            #  print '1a', getattr(pub, entry[0])
            #  print '2', entry[1][0]
            #  print '2a', getattr(self, entry[1][0])
            #  print 'type', type(getattr(self, entry[1][0]))
            #  print '\n'
                if len(entry[1]) == 1:
                    #if type(getattr(self, entry[1][0])).__name__ == 'instancemethod':
                    if type(getattr(self, entry[1][0])).__name__ == 'method':
                        #if entry[0] == "HLTUsedCores":
                            # print "HERE"
        #        print entry[0],getattr(self, entry[1][0])()
                        setattr(pub, entry[0], getattr(self, entry[1][0])())
                    else:
                        setattr(pub, entry[0], getattr(self, entry[1][0]))

                else:
                    #if type(getattr(self, entry[1][0])).__name__ == 'instancemethod': #if first item in value tuple is a function
                    if type(getattr(self, entry[1][0])).__name__ == 'method':
                        # print "key, function", entry[0], entry[1]
                        for arg in entry[1]:
                            if arg==entry[1][0]: pass #if its the function, pass
                            else:
                                args.append(arg) #make a list of parameters
                        setattr(pub, entry[0], getattr(self, entry[1][0])(*args))
                        args[:]=[]
                    elif type(getattr(self, entry[1][0])).__name__ == 'superv':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'Xummer':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'ISInfoAny':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'ISObject':
                        if entry[1][1]=='errorReasons':
                            attr= str(getattr(getattr(self, entry[1][0]), entry[1][1]))
                            setattr(pub, entry[0], attr)
                        else:
                            setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'instance':
                        setattr(pub, entry[0], getattr(getattr(self, entry[1][0]), entry[1][1]))
                    elif type(getattr(self, entry[1][0])).__name__ == 'dict':
                        if type(getattr(self, entry[1][0])[entry[1][1]]).__name__ == 'list':
                            setattr(pub, entry[0], getattr(self, entry[1][0])[entry[1][1]][entry[1][2]])
                        else:
                            setattr(pub, entry[0], getattr(self, entry[1][0])[entry[1][1]])
                    else:
                        ers.log( 'PROBLEM!!! %s' % type(getattr(self, entry[1][0])))


            except (ISAttributeError, ISKeyError, ISInvalidIterator, AttributeError, OverflowError, UnboundLocalError, MyRuntimeError) as e:
                ers.log( 'Know exception caught in Publish(): %s' % e)
                #print getattr(self, entry[1][0])
                if args:
                    args[:]=[]
                treatErrors( e, entry[0], entry[1])
            except Exception as e:
                ers.log( 'Unknown exception caught in Publish(): %s '% e)

#    print "-------------------------"
#    print 'dictionary', dictionary
#    print 'The dictionary contains %d variables' % len(attributesDict)
#    print 'blackDict', blackDict
#    print "-------------------------"



    def initISObj(self):
        initDict={} #initialize a dictionary where everything is 0
        for entry in attributesDict.items(): #iteritems no longer exists in python 3, its just items(). Only difference is iteritems takes less memory
            #dict.items() returns list of tuple (key, value). So entry = key, value. So entry[0] = key

            #print type(getattr(pub, entry[0])).__name__
            if (type(getattr(pub, entry[0])).__name__ == 'str'):
                initDict[entry[0]] = 'No Info'
            elif (type(getattr(pub, entry[0])).__name__ == 'list'):
                if (pub.getAttributeDescription(entry[0]).typeName() == 'string'):
                    iniList=[]
                    iniList.append("No Info")
                    initDict[entry[0]] = iniList
                elif (pub.getAttributeDescription(entry[0]).typeName() == 'u32'):
                    initDict[entry[0]] = [0]
            elif (type(getattr(pub, entry[0])).__name__ == 'int') or (type(getattr(pub, entry[0])).__name__ == 'float'):
                initDict[entry[0]] = 0
            else:
                ers.log( '??? %s' % type(getattr(pub, entry[0])).__name__ )

        pub.set(initDict)



# ======== signal handler =========
def sig_handler(signum, f_globals):
    global run
    ers.log( 'Signal handler called with signal %s' % signum)
    run = False
    sys.exit(0)

#==================== MAIN =====================================


#Readcommand line parameters
global run
run = True

parser = OptionParser()
parser.add_option('-p', '--partition',
                  dest='part_name', type='string', default = 'ATLAS',
                  help='Pass the partition name - default = ATLAS')
parser.add_option('-v', '--verbose',
                  dest='verbose', action="store_true", default = False,
                  help='Set it if you want a verbose mode - default=False')
parser.add_option('-u', '--isslowupdate',
                  dest='isslowupdate', type='int', default = 6,
                  help='After how many publication you want to update information which are cpu consuming - default = 6')
parser.add_option('-I', '--isinfoname',
                  dest='isinfoname', type='string', default = 'DFSummary',
                  help='Name of the IS variable - default = DFSummary')
(options, args) = parser.parse_args()

ers.log( '\nSTART\nPartition name = %s,\nisinfoname = %s, \nVerbose = %s,\nIS slow update every %s times' % (options.part_name, options.isinfoname, options.verbose, options.isslowupdate))


verbose      = options.verbose
part_name    = options.part_name
isinfoname   = options.isinfoname

# Instantiate worker object
worker = Worker(part_name)
worker._verbose    = verbose
worker._isDelayFact= options.isslowupdate

main_part       = IPCPartition(part_name) #partition

##-I DFSummaryTest!!!
pub = ISObject(main_part, 'DF.%s' %isinfoname, "DFSummary") #gives an IS object that can be added, removed, or updated in IS

#main_part       = IPCPartition('dfs_test')
#pub = ISObject(main_part, 'DF.%s' % options.isinfoname, 'DFSummary')

signal.signal(signal.SIGTERM, sig_handler)
signal.signal(signal.SIGINT, sig_handler)

worker.initISObj()

while run:
    try:
        worker.updateRCState()
        print("RUN CONTROL", worker._rc.state)
        if worker._rc.state == 'RUNNING':
            worker.Update()
            worker.Publish(specialAttributesDict)
            worker.Publish(attributesDict)
            time.sleep(2)
        else:
            worker.initISObj()
            worker.Update('short')
            worker.Publish(specialAttributesDict)
            time.sleep(10)
    except (ISAttributeError, ISRepositoryNotFound, ISInvalidIterator, ISKeyError) as e:
        ers.error( e )
    except Exception as e:
        ers.error( 'Unknown Exception ', e)

    pub.checkin()
