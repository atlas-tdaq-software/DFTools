#!/usr/bin/env tdaq_python
# This utility changes the state of a host.

from __future__ import print_function
import argparse
import sys
import os
import subprocess
import traceback

import config
from config.dal import module

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--release', help='obsolete param')
    parser.add_argument('-s', '--state', choices=['on', 'off'], required=True, help='State to set')
    parser.add_argument('hosts', help='Host names', nargs='+')

    try:
        options= parser.parse_args()
    except:
        print("Parsing commandline options failed")
        sys.exit(184)

    try:
        mydb='oksconfig:'+os.environ['TDAQ_DB_DATA']
        print(mydb)

        db = config.Configuration(mydb)

        for h in options.hosts:
            computer=db.get_dal('Computer', h)

            if options.state == 'on':
                computer.State = 1
            elif options.state == "off":
                computer.State = 0
            
            db.update_dal(computer)

        db.commit(("Enabled" if options.state == 'on' else "Disabled") + " hosts: " + ", ".join(options.hosts))
    except:
        print("Changing the database failed")
        traceback.print_exc()
        sys.exit(183)

    sys.exit(0)
