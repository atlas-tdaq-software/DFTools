#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import str
from builtins import range
import sys
import time
import getopt
from ispy import IPCPartition, InfoReader, IPCPartition, ISInfoIterator, ISInfoAny
from oh import *
from ROOT import TCanvas, TText, TFile


histos = [
  'HistogrammingHLT.Top-SFI-SFO./DEBUG/SFIHistograms/BuiltTime',
  'HistogrammingHLT.Top-SFI-SFO./DEBUG/SFIHistograms/EvtSize',
  'HistogrammingHLT.Top-SFI-SFO./DEBUG/SFIHistograms/PayloadRate',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/CollectionTimeAcceptedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/CollectionTimeRejectedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/DataPerAcceptedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/DataPerRejectedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/RequestsPerAcceptedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/RequestsPerRejectedEvent',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/ResultSize',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./DEBUG/L2PUHistograms/ROSHits',
  'HistogrammingHLT.Top-LVL2-L2-Segment-1-Gatherer./EXPERT/TrigSteer_L2/NInitialRoIsPerEvent',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/efio-efd2sfo-SfoReplyTime',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/efio-sfi2efd-ReplyTime',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/EventLifeTime',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/EventStripTime',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/EventsPerTask',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/EFDHistograms/FlowControlDelay',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/PTHistograms/EventSize_AllEvents',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/PTHistograms/ProcessingTime_AllEvents',
  'HistogrammingHLT.Top-EF-EBEF-Segment./DEBUG/PTHistograms/ResultSize',
  'Histogramming.rosMon./SHIFT/tdq/rosLoad',
]

filename_base = 'diff.'

def Usage():
  print('Fetch histograms from OH servers at two different times and produce the difference of the two histograms')
  print('Usage:', sys.argv[0], '[options]')
  print('   -t  sec      set the time interval')
  print('   -C  hPath    use a user defined histogram path')
  print('   -p partition [def=ATLAS]')
  for i in range(0, len(histos)):
    print('   -c %d         %s' %(i, histos[i]))    
  sys.exit(2)


pname = 'ATLAS'
t = 100
hs= histos[0].split('.')

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "HC:t:c:p:")
except getopt.GetoptError as err:
  print(str(err))
  Usage()
  sys.exit(2)

try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-H": Usage()
    elif  o == "-t": t= int(a)
    elif  o == "-C": hs = a.split('.')
    elif  o == "-c": hs = histos[int(a)].split('.')
    elif  o == "-p": pname=a
except(AttributeError): pass

# Partition
print("Partition =", pname)
p = IPCPartition(pname)


#Run number
it = ISInfoIterator(p, 'RunParams')
while next(it):
  if it.name().find('RunParams.RunParams') == 0 :
    break
rp = ISInfoAny()
it.value(rp)
rp._update(it.name(), p)

print('Histogram: ', hs)
print('Run num  : ', rp.run_number)

c=TCanvas('c1','Histo Interval',200,10,600,600)
c.Divide(1,2)
b1 = getRootObject(p, hs[0], hs[1], hs[2])
c.cd(1)
b1.SetFillColor(41); b1.Draw()
c.cd(2) 
msg = "Please wait %d s" % t
tmsg = TText()
tmsg.DrawText(.4,.5,msg)
c.Update()

time.sleep(t)
b2 = getRootObject(p, hs[0], hs[1], hs[2])
b2.Add(b2,b1,1,-1)
tit = '%s  [last %s s]' % (b2.GetTitle(), t)
b2.SetTitle(tit)
c.cd(2); b2.SetFillColor(42); b2.Draw()
c.Update()

#Dump
v = hs[2].split("/")
n = v[len(v)-1]
tm = time.strftime('%y.%m.%d-%H.%m')
filename = filename_base + n + '_' + tm + "_" + str(rp.run_number) + '.root'
f = TFile(filename, "recreate")
b1.Write()
b2.Write()
f.Close()
print('Histogram saved in file', filename)



while(True):
  time.sleep(100)
  






