#!/usr/bin/env tdaq_python

from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import map
from builtins import range
from tkinter import *
from tktools import Meter
import cx_Oracle
import time
import datetime
import math
from operator import add
from operator import pow
from operator import itemgetter
import sfodb.sfodb_auth


class RunDetails(Frame):

    def __init__ (self, master, runnr = 0, oracle = None, onclose = None):
        self.master = Toplevel(master)
        self.master.title(str(runnr))
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.orcl = oracle
        self.runnr = runnr
        self.onclose = onclose
        Frame.__init__(self, self.master, relief='ridge')
        self.pack()

        self.bars = {}

        self.create()

    def close(self):
        if self.onclose: self.onclose()
        self.master.destroy()

    def create(self):
        sfostatus = self.fetchdata()

        for i,sfo in enumerate(sorted(sfostatus.keys())):
            e = StatusBar(self, text=sfo+': Transfer', value=sfostatus[sfo][0],
                          color=sfostatus[sfo][2],
                          label_width=25)
            e.grid(row=i*2,column=0,sticky=W)
            self.bars[sfo+'up'] = e
            e = StatusBar(self,text=sfo+': Deletion', value=sfostatus[sfo][1],
                          color = 'gray',
                          meter_color='yellow', label_width=25)
            e.grid(row=i*2+1,column=0,sticky=W)
            self.bars[sfo+'del'] = e



    def update(self):
        sfostatus = self.fetchdata()

        for i,sfo in enumerate(sorted(sfostatus.keys())):
            if sfo+'up' in list(self.bars.keys()):
                self.bars[sfo+'up'].setvalue(sfostatus[sfo][0])
                self.bars[sfo+'up'].setstatus(sfostatus[sfo][2])
                self.bars[sfo+'del'].setvalue(sfostatus[sfo][1])
            else:
                e = StatusBar(self, text=sfo+': Transfer',
                              value=sfostatus[sfo][0], color=sfostatus[sfo][2],
                              label_width=25)
                e.grid(row=len(self.bars),column=0,sticky=W)
                self.bars[sfo+'del'] = e
                e = StatusBar(self, text=sfo+': Deletion', value=sfostatus[sfo][1],
                              color='gray',
                              label_width=25)
                e.grid(row=len(self.bars),column=0,sticky=W)
                self.bars[sfo+'del'] = e

    def fetchdata(self):
        curs = self.orcl.cursor()
        curs.arraysize = 10

        query = """select sfoid from SFO_TZ_RUN
        where runnr = :srunnr group by sfoid"""
        curs.execute(query, {'srunnr':self.runnr})
        sfos = list(map(itemgetter(0), curs.fetchall()))

        query = """select sfoid, count(lfn) from SFO_TZ_FILE
        where filestate='OPENED' and runnr = :srunnr group by sfoid"""
        curs.execute(query, {'srunnr':self.runnr})
        tmp = curs.fetchall()
        #If all the files are closed for a given sfo, than we have to add a 0
        tmp_dict = dict(tmp)
        nr_openedfiles = [tmp_dict.get(s,0) for s in sfos]

        query = """select sfoid, count(lfn) from SFO_TZ_FILE
        where filestate<>'OPENED' and runnr = :srunnr group by sfoid"""
        curs.execute(query, {'srunnr':self.runnr})
        tmp = curs.fetchall()
        #If all the files are closed for a given sfo, than we have to add a 0
        tmp_dict = dict(tmp)
        nr_closedfiles = [tmp_dict.get(s,0) for s in sfos]

        query = """select sfoid, count(lfn) from SFO_TZ_FILE
        where filestate='DELETED' and runnr = :srunnr group by sfoid"""
        curs.execute(query, {'srunnr':self.runnr})
        tmp = curs.fetchall()
        #If all the files are closed for a given sfo, than we have to add a 0
        tmp_dict = dict(tmp)
        nr_deletedfiles = [tmp_dict.get(s,0) for s in sfos]

        query = """select sfoid, count(lfn) from SFO_TZ_FILE
        where filestate<>'OPENED' and transferstate='TRANSFERRED' and
        runnr = :srunnr group by sfoid"""
        curs.execute(query, {'srunnr':self.runnr})
        tmp = curs.fetchall()
        #If all the files are closed for a given sfo, than we have to add a 0
        tmp_dict = dict(tmp)
        nr_transferfiles = [tmp_dict.get(s,0) for s in sfos]

        sfostatus={}
        for s,op,cl,de,tr in zip(sfos, nr_openedfiles, nr_closedfiles, nr_deletedfiles, nr_transferfiles):

            try:
                upratio = 1.*tr/cl
            except ZeroDivisionError:
                upratio = 0.

            try:
                delratio = 1.*de/cl
            except ZeroDivisionError:
                delratio = 0.

            sfostatus[s] = [upratio,delratio]

            if op != 0:
                sfostatus[s].append(StatusBar.colormap['opened'])
            elif cl == tr:
                sfostatus[s].append(StatusBar.colormap['transferred'])
            else:
                sfostatus[s].append(StatusBar.colormap['closed'])

        return sfostatus


class StatusBar(Frame):

    colormap = {'opened':'blue', 'closed':'yellow', 'transferred': 'green'}

    LABEL_WIDTH = 12
    BAR_WIDTH = 300
    CANVAS_WIDTH = 20

    @staticmethod
    def titleBar(master, column_offset = 0):

        f = Frame(master, relief='ridge')
        f.pack()
        l = Label(f, width=12, text='Run Nr',
              relief='ridge')
        l.grid(row=0, column=column_offset, sticky=W)
        column_offset += 1
        l = Label(f, width=42, text='Fraction of transferred files',
              relief='ridge')
        l.grid(row=0, column=column_offset, sticky=W)
        column_offset += 1
        l = Label(f, width=3, text='',
              relief='ridge')
        l.grid(row=0, column=column_offset, sticky=W)

        return f


    def __init__(self, master, text = 0, value = 0.0,
                 label_width = 12,
                 color='blue', column_offset = 0, meter_color = 'orchid1'):

        Frame.__init__(self, master, relief='ridge')
        self.pack()

        self.master = master

        self.label = Label(self, width=label_width, text=text,
                           relief='ridge')
        self.label.grid(row=0, column=column_offset,
                        sticky=W)

        column_offset += 1
        self.bar = Meter.Meter(self,relief='ridge', bd=3, height=20,
                               width=StatusBar.BAR_WIDTH,  fillcolor = meter_color)
        self.bar.grid(row=0, column=column_offset, sticky=W)
        self.bar.set(value)

        column_offset += 1

        self.canv = Canvas(self, height=20,
                           width=StatusBar.CANVAS_WIDTH, relief='ridge')
        self.canv.grid(row=0, column=column_offset, sticky=W)
        self.circ=self.canv.create_oval(2,2,18,18,fill=color)

    def setlabel(self,text):
        self.label.config(text=text)

    def setvalue(self,value):
        self.bar.set(value)

    def setstatus(self,color):
        self.canv.itemconfig(self.circ,fill=color)



class RunEntry(StatusBar):

    @staticmethod
    def titleBar(master):

        frame = StatusBar.titleBar(master, 1)
        l = Label(frame, width=4, text='Info',
                  relief='ridge')
        l.grid(row=0, column=0, sticky=W)

        return frame

    def __init__(self,master, runnr = 0,
                 value = 0.0, color='blue', oracle = None, warning = False):

        StatusBar.__init__(self, master, text = runnr, value = value,
                           color = color, column_offset = 1)
        self.master = master
        self.runnr = runnr
        self.orcl = oracle
        self.r = None

        self.b = Button(self, text="I", command=self.details)
        self.b.grid(row=0, column=0, sticky=W)
        if warning: self.b.config(bg='red')


    def details(self):
        self.r = RunDetails(self.master, self.runnr,
                            self.orcl, self.resetDet)

    def update(self, runnr, value, status, warning=False):
        self.runnr = runnr
        StatusBar.setlabel(self,runnr)
        StatusBar.setvalue(self,value)
        StatusBar.setstatus(self,status)
        if self.r: self.r.update()
        if warning: self.b.config(bg='red')
        else: self.b.config(bg='light gray')


    def resetDet(self):
        self.r = None


class SFODisplay(Frame):

    def __init__(self,master):
        Frame.__init__(self, master)
        self.pack()
        self.bars = []
        self.minrunnr = 1
        db_connection = sfodb.sfodb_auth.useCoral()
        self.orcl = cx_Oracle.connect(db_connection)

        title = RunEntry.titleBar(self)
        title.grid(row=0,column=0,sticky=W)

        runstatus = self.fetchdata()
        for i,run in enumerate(sorted(list(runstatus.keys()),reverse=True)):
            e = RunEntry(self,run,runstatus[run][0],runstatus[run][1],self.orcl)
            e.grid(row=i+1,column=0,sticky=W)
            self.bars.append(e)

        if len(runstatus) < 10:
            for i in range(0,10-len(runstatus)):
                e = RunEntry(self,0,0.0,
                             RunEntry.colormap['transferred'])
                e.grid(row=1+i+len(fileinfo),column=0,sticky=W)
                self.bars.append(e)

        self.statusbar = Canvas(self,height=20,width=365)
        self.statusbar.create_oval(2,2,18,18,fill=RunEntry.colormap['opened'])
        t = self.statusbar.create_text(22,10,text="Active",
                                       anchor=W,font=("Helvetica", 6))
        t = self.statusbar.create_oval(self.statusbar.bbox(t)[2]+4,2,
                                       self.statusbar.bbox(t)[2]+20,18,
                                       fill = RunEntry.colormap['closed'])
        t = self.statusbar.create_text(self.statusbar.bbox(t)[2]+4,10,
                                       text="Ended",
                                       anchor=W,font=("Helvetica", 6))
        t = self.statusbar.create_oval(self.statusbar.bbox(t)[2]+4,2,
                                       self.statusbar.bbox(t)[2]+20,18,
                                       fill=RunEntry.colormap['transferred'])
        t = self.statusbar.create_text(self.statusbar.bbox(t)[2]+4,10,
                                       text="Transferred",
                                       anchor=W,font=("Helvetica", 6))

        now=datetime.datetime.fromtimestamp(time.time())

        self.time = self.statusbar.create_text(self.statusbar.bbox(t)[2]+6,10,
                                               text=now.strftime('%d-%b-%Y %H:%M:%S'),
                                               anchor=W,font=("Helvetica", 6))

        self.statusbar.grid(row=len(self.bars)+1,column=0,sticky=W)

    def update(self):
        runstatus = self.fetchdata()
        for i,run in enumerate(sorted(list(runstatus.keys()),reverse=True)):
            self.bars[i].update(run,runstatus[run][0],runstatus[run][1])

        now = datetime.datetime.fromtimestamp(time.time())
        self.statusbar.itemconfig(self.time,
                                  text=now.strftime('%d-%b-%Y %H:%M:%S'))

    def fetchdata(self):
        curs = self.orcl.cursor()
        curs.arraysize = 10
        query = """select runnr from SFO_TZ_RUN
        where runnr < 1000000 and runnr >=: minrunnr
        and (SFOID like 'SFO%' or SFOID like 'MUCal%')
        group by runnr order by runnr desc"""
        curs.execute(query,{'minrunnr':(self.minrunnr-1)})
        validruns = list(map(itemgetter(0), curs.fetchmany()))

        run_selec=[]
        vars={}
        for i in validruns:
            varname = 'run%d' % i
            run_selec.append(' runnr =: %s ' % varname)
            vars[varname] = i

        selec = ' '.join(['(',' or '.join(run_selec),')'])

        query = """select runnr, count(lfn) from SFO_TZ_FILE
        where filestate='OPENED' and """ + \
        selec +" group by runnr"
        curs.execute(query, vars)
        tmp = curs.fetchall()
        #If all the files are closed for a given run, than we have to add a 0
        tmp_dict = dict(tmp)
        nr_openedfiles = [tmp_dict.get(r,0) for r in validruns]


        query = """select runnr,count(lfn) from SFO_TZ_FILE
        where filestate<>'OPENED' and """ + \
        selec +" group by runnr"
        curs.execute(query, vars)
        tmp = curs.fetchall()
        tmp_dict = dict(tmp)
        nr_closedfiles = [tmp_dict.get(r,0) for r in validruns]

        query = """select runnr,count(lfn) from SFO_TZ_FILE
        where filestate<>'OPENED' and transferstate='TRANSFERRED' and """ + \
        selec +" group by runnr"
        curs.execute(query, vars)
        tmp = curs.fetchall()
        tmp_dict = dict(tmp)
        nr_transferfiles = [tmp_dict.get(r,0) for r in validruns]

        runstatus = {}
        for r,op,cl,tr in zip(validruns, nr_openedfiles, nr_closedfiles, nr_transferfiles):

            try:
                ratio = 1.*tr/cl
            except ZeroDivisionError:
                ratio = 0.

            runstatus[r] = [ratio,]
            if op != 0:
                runstatus[r].append(RunEntry.colormap['opened'])
            elif tr == cl:
                runstatus[r].append(RunEntry.colormap['transferred'])
            else:
                runstatus[r].append(RunEntry.colormap['closed'])

        self.minrunnr = min(validruns)

        return runstatus

def timedupdate(dis):
    dis.update()
    dis.after(30000, lambda : timedupdate(dis))

root = Tk()
root.title("SFO Transfer State")
dis = SFODisplay(root)
dis.after(30000, lambda : timedupdate(dis))
root.mainloop()
