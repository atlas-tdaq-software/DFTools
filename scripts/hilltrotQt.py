#!/usr/bin/env tdaq_python


from builtins import str
import os
import sys
from PyQt4.QtGui import QApplication, QMainWindow, QMessageBox
from hilltrot.guiCore import MainWidget
from hilltrot.hilltrot import parseArguments
from hilltrot.hWidgets import Action, Dialogs


class MainWindow(QMainWindow):
   def __init__(self):
      super(MainWindow, self).__init__()
      self.mainWidget = MainWidget()
      self.setCentralWidget(self.mainWidget)
      self.initMenus()

      self.setWindowTitle('hilltrot')
      self.resize(700, 500)
      self.show()

   def initMenus(self):
      # The status and menu bar
      self.statusBar()
      menubar = self.menuBar()

      ### File menu and actions
      # The open action
      openAct = Action('&Open', self, self.openFile)
      openAct.setShortcut('Ctrl+O')
      openAct.setStatusTip('Open a file')
      # The save action
      self.saveAct = saveAct = Action('&Save', self, self.mainWidget.save)
      saveAct.setEnabled(False)
      saveAct.setShortcut('Ctrl+S')
      saveAct.setStatusTip('Save the current file')
      # The 'save as' action
      self.saveAsAct = saveAsAct = Action('Save &As', self, self.saveFileAs)
      saveAsAct.setEnabled(False)
      saveAsAct.setShortcut('Shift+Ctrl+S')
      saveAsAct.setStatusTip('Save the current file with a different name')
      # The 'print to log' action
      printLogAct = Action('&Print To Log', self, self.mainWidget.logState)
      printLogAct.setShortcut('Ctrl+P')
      printLogAct.setStatusTip('Output the current view to the log panel')
      # The quit action
      quitAct = Action('&Quit', self, self.close)
      quitAct.setShortcut('Ctrl+Q')
      quitAct.setStatusTip('Quit application')

      fileMenu = menubar.addMenu('&File')
      if 'dbe_running' in globals():
         fileMenu.addAction(printLogAct)
         fileMenu.addSeparator()
         fileMenu.addAction(quitAct)
      else:
         fileMenu.addAction(openAct)
         fileMenu.addSeparator()
         fileMenu.addActions([saveAct, saveAsAct])
         fileMenu.addSeparator()
         fileMenu.addAction(printLogAct)
         fileMenu.addSeparator()
         fileMenu.addAction(quitAct)
      
      ### Help menu and actions
      helpAct = Action('&Help', self, self.helpMessage)
      helpAct.setShortcut('F1')
      helpAct.setStatusTip('Open the hilltrot help')
      
      helpMenu = menubar.addMenu('&Help')
      helpMenu.addAction(helpAct)
   
   def helpMessage(self):
      txt = ('\tGeneral information\n'
           + 'Hilltrot is a tool to balance the HLT farms.'
           + ' It allows the user to change the HLT rack sharing, and'
           + ' provides information about the available CPU power and'
           + ' network bandwidth & connectivity.'
           + '\nFor a complete description, see'
           + ' https://twiki.cern.ch/twiki/bin/viewauth/Atlas/Hilltrot'
           + '\n\n'
           + '\tRack representation\n'
           + 'Racks in hilltrot can be in one of five states:\n'
           + '* Enabled (enabled in the current farm, disabled or not'
           + ' present in the other)\n'
           + '* Conflict (enabled in both farms)\n'
           + '* Disabled (disabled in the current farm, but enabled in'
           + ' the other)\n'
           + '* Unused (disabled in the current farm, disabled or not'
           + ' present in the other)\n'
           + '* Disabled by parent (the segment containing this rack is'
           + ' disabled)')
      helpBox = QMessageBox(QMessageBox.Information, 'hilltrot Help', txt)
      helpBox.exec_()

   def openFile(self, fileName=None, partName=None, existingProject=None):
      if existingProject:
         self.mainWidget.load(fileName, partName, existingProject)
      elif not fileName:
         fileName = Dialogs.openFile(self, 'Open a database file...',
               os.getcwd(), 'XML Data File (*.data.xml)')
         fileName = str(fileName)
         if not fileName:
            return
      self.mainWidget.load(fileName, partName, existingProject)
      self.saveAct.setEnabled(True)
      self.saveAsAct.setEnabled(True)
      if self.mainWidget.fileName.startswith('rdbconfig:'):
         self.saveAsAct.setEnabled(False) # 'Save As' is for files, not RDBs
      fname = self.mainWidget.fileName
      pname = self.mainWidget.partName
      if fname and pname:
         s = '{0} / {1} - hilltrot'
         self.setWindowTitle(s.format(fname, pname))

   def saveFileAs(self):
      fileName = Dialogs.saveFile(self, 'Save file as...', os.getcwd(),
            'XML Data File (*.data.xml)')
      fileName = str(fileName)
      if fileName:
         self.mainWidget.saveAs(fileName)


def main():
   if 'dbe_running' in globals():
      import hilltrot.guiCore
      hilltrot.guiCore.dbe_running = True
      global gui
      gui = MainWindow()
      gui.openFile('dbe', None, database)
   else:
      fileName, partName = parseArguments()
      app = QApplication(sys.argv)
      gui = MainWindow()
      if fileName: gui.openFile(fileName, partName)
      sys.exit(app.exec_())

if __name__ == '__main__':
   main()
