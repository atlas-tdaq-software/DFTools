#!/usr/bin/env tdaq_python
from __future__ import print_function
import cx_Oracle
import sys
import datetime
from datetime import timedelta

from sfodb.sfodb_auth import *

#solvedb = useCoral('atlas_config', 'ATLAS_SFO_T0_R')
try:
    solvedb = on_off_db_switch('online')
except (RuntimeError,NameError) as e:
    print('problem connecting to db.', e)
    exit(1)

db = cx_Oracle.connect(solvedb)
or_cur = db.cursor()

#sql="select creation_time,modification_time,filenr from SFO_TZ_FILE where runnr=166846 "

#sql="select creation_time, filesize from SFO_TZ_FILE where creation_time between '1 Jan 2012 01:00:00' and '1 Aug 2012 12:00:00' and sfoid like 'SFO%'"

sql="select creation_time, filesize from SFO_TZ_FILE where creation_time between '1 Aug 2012 01:00:00' and '3 Aug 2012 12:00:00' and sfoid like 'SFO%' and lfn like 'data12_8TeV%'"
or_cur.execute(sql)
tot_list = or_cur.fetchall()

tot_size = 0
for file_tuple in tot_list:
    tot_size = tot_size + file_tuple[1]

print(tot_size)
