#!/usr/bin/env tdaq_python
from __future__ import print_function
from builtins import map
import sys
import pm.project
from ispy import IPCPartition
from itertools import groupby
from operator import itemgetter

def help():
  print('usage:', sys.argv[0], '$TDAQ_DB_DATA $TDAQ_PARTITION ')
  print('   [-h]    print usage infos')
  print('   [-r]    disable colors')
  print('   [-n]    show (template) host names')  
  sys.exit(1)

def find_disabled_and_print(disabledObjs, seg_list, farm_name, memo_name, memo_col):
  dictio = {}
  for m in seg_list:
    if m.id.find(farm_name) == 0:
      head = "\t\t"
      disabled = False
      if m.id in disabledObjs:
        print("\t" + col_red + "!" + m.id + col_def)
        disabled = True
      else:
        print("\t " + m.id)
        # Loop over subsegments
      for s in m.Segments: 
        r = re.search("rack-Y.*", s.id)
        rack = r.group(0)
        if rack not in dictio: 
          dictio[rack] = ""
        if s.id in disabledObjs:
          print(head + col_red + "!" + s.id + col_def)
        else:
          if disabled:
            print(head + col_yel + s.id + col_def)
          else:
            print(head + s.id)
            dictio[rack] = memo_col + " " + memo_name \
                           + " " + col_def
  return dictio

def coalesce(numbers):
    """Return consecutive groups of numbers"""
    
    numbers.sort()
    groups = []   # Consecutive group of numbers
    for k, g in groupby(enumerate(numbers), lambda a_b:a_b[0]-a_b[1]):
        groups += [list(map(itemgetter(1), g))]

    ranges = [ ('%d' % g[0]) if len(g)==1 
               else ('%d-%d' % (g[0], g[-1])) for g in groups ]
    
    return ranges

def get_hosts_in_rack(seg_list):
  dictio = {}
  for m in seg_list:
    for s in m.Segments:
      rack = re.search("rack-Y.*", s.id).group(0)
      hosts = [h.id.replace(".cern.ch","") for h in s.TemplateHosts]
      htype = "-".join(hosts[0].split("-")[0:3])  # xpu or ef
      # Bit of an overkill. Works also for non-consecutive hostnames in rack
      ranges = coalesce([int(h.split("-")[3]) for h in hosts])
      
      dictio[rack] = "%s %s" % (htype, ("%s" % ranges).replace("'",""))
  return dictio

  
#Color definitions
col_red = "\033[31m"
col_def = "\033[0m"
col_gre = "\033[32m"
col_bol = "\033[1m"
col_bla = "\033[30m"
col_blu = "\033[34m"
col_yel = "\033[33m"

#Parse command line
if len(sys.argv) < 3:
  help()
partFile = sys.argv[1]
partName = sys.argv[2]

noColor   = False
showHost  = False
for i in sys.argv:
  if(i.find('-h') == 0) : help()
  if(i.find('-r') == 0) : noColor = True
  if(i.find('-n') == 0) : showHost = True

if noColor:
  col_red = col_def = col_gre = col_bol = ""
  col_bla = col_blu = col_yel = ""


print(col_bol + "Partition File = ", col_def, partFile)
print(col_bol + "Partition Name = ", col_def, partName)

#Read the DB
proj = pm.project.Project(partFile)

#load DAL tools
from config.dal import module as dal_module
dal    = dal_module('dal',    'daq/schema/core.schema.xml' )
DFdal  = dal_module('DFdal',  'daq/schema/df.schema.xml', [dal])

#Ok, we use the underlying Configuration methods
#rather than the project methods
#to get the partition parsing smoking fast
partition = proj.get_obj("Partition", partName)

disabledObjs=[k.UID() for k in partition['Disabled']]

print("Disabled objs  = ", len(disabledObjs))

ebSeg = None
l2Seg = None
for n in partition['Segments']:
  if n.UID().startswith("EBEF-Segment") and not ebSeg: ebSeg = proj.getObject(n.class_name(), n.UID())
  if n.UID().startswith("L2-Segment") and not l2Seg: l2Seg = proj.getObject(n.class_name(), n.UID())
  for m in n['Segments']:
    if m.UID().startswith("EBEF-Segment")and not ebSeg: ebSeg = proj.getObject(m.class_name(), m.UID())
    if m.UID().startswith("L2-Segment") and not l2Seg: l2Seg = proj.getObject(m.class_name(), m.UID())

if not isinstance(ebSeg, dal.Segment): print("EBEF Segment not found"); help()
if not isinstance(l2Seg, dal.Segment): print("L2 Segment not found"); help()

print(col_bol + "EF segment = ", ebSeg.id)
print(col_bol + "L2 segment = ", l2Seg.id)

print()
print(col_bol + '----------------------------------------' + col_def) 
import re

print(col_bol + ebSeg.id + col_def)

efracks = find_disabled_and_print(disabledObjs, ebSeg.Segments, \
                                  'EBF', 'EF', col_blu)

print(col_bol + l2Seg.id + col_def)

l2racks = find_disabled_and_print(disabledObjs, l2Seg.Segments, \
                                  'L2SV-Segment', 'L2', col_gre)


##Merge dictionaries
dictio ={}
for k in l2racks:
  dictio[k] = l2racks[k]
  if k in efracks:
    dictio[k] = dictio[k] + efracks[k]
    
for k in efracks:
  if not k in dictio:
    dictio[k] = efracks[k]


if showHost:
  hosts = get_hosts_in_rack(l2Seg.Segments+ebSeg.Segments)
  
print()
print(col_bol + '----------------------------------------') 
print('---- HLT RACK SHARING ------------------')
print('----------------------------------------' + col_def)
out = [] 
for k in dictio.keys():
  if 'EF' in dictio[k] and 'L2' in dictio[k]:
    s = col_red + k + col_def + dictio[k]
  else:
    s = k + dictio[k]

  s += (hosts[k] if showHost else "")    
  out.append(s)

out.sort()
out.reverse()

for i in out:
  print(i)


