#!/usr/bin/env tdaq_python


from __future__ import print_function
import sys

from hilltrot.hilltrot import parseArguments, Database, NoPartitionError
from hilltrot.Segment import Segment

def cell(text, colour='', size='', rowspan=1):
   if colour:
      text = '<font color="{0}">{1}</font>'.format(colour, text)
   if size:
      text = '<{0}>{1}</{0}>'.format(size, text)
   tag = '<td align="center" width=200'
   if rowspan != 1:
      tag += ' rowspan={0}'.format(rowspan)
   tag += '>'
   return '{0}{1}</td>'.format(tag, text)

def row(cells, cls='', onclick=''):
   tag = '<tr'
   if cls: tag += ' class="{0}"'.format(cls)
   if onclick: tag += ' onclick="{0}"'.format(onclick)
   tag += '>'
   cells = '\n      '.join(cells)
   return '''   {0}
      {1}
   </tr>'''.format(tag, cells)

rackColours = {
   Segment.UNUSED   : '#00BB00',
   Segment.ENABLED  : '#000000',
   Segment.DISABLED : '#777777',
   Segment.HIERARCHY: '#0000FF',
   Segment.CONFLICT : '#FF0000',
}

def makeRackCell(rack):
   rackColour = rackColours[rack.status]
   return cell(rack.name, colour=rackColour, size='h4')

if __name__ == '__main__':
   try:
      dbName, partName = parseArguments()
      db = Database()
      db.load(dbName, partName)
   except AssertionError as e:
      print('No database to load!')
      sys.exit(1)
   except NoPartitionError as e:
      print(e)
      sys.exit(1)

   HEADER = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>HLT Racks Configuration</title>
</head>

<body>
'''
   FOOTER = '''
</body>
</html>'''
   
   SCRIPT_TEMPLATE = '''
   var contents{0} = new Array();
   contents{0}[0] = "";
   contents{0}[1] = "";
   function toggle{0}() {{
      var rows = document.getElementsByClassName("stats{0}");
      for (i = 0; i < rows.length; i++) {{
         if (rows[i].innerHTML == "") {{
            rows[i].innerHTML = contents{0}[i];
            contents{0}[i] = "";
         }} else {{
            contents{0}[i] = rows[i].innerHTML;
            rows[i].innerHTML = "";
         }}
      }}
   }}
   toggle{0}();
'''

   TABLE_HEAD = '<table align="center" bgcolor="#BBBBFF" width=500>'
   SEPARATOR = '<table align="center" bgcolor="#9999FF" width=500><tr><td></td></tr></table>'
   
   l2Racks, efRacks = db.l2Racks, db.efRacks
   enabledL2 = [r for r in l2Racks if r.status == Segment.ENABLED]
   enabledEF = [r for r in efRacks if r.status == Segment.ENABLED]

   rackNumbers = (rack.name.split('-')[-1] for rack in l2Racks + efRacks)
   rackNumbers = sorted(set(rackNumbers))
   print(HEADER)
   print('<h1 align="center">HLT Racks Configuration (web hilltrot)</h1>')
   print(TABLE_HEAD)

   print(row([cell('L2'), cell('Parameter'), cell('EF')]))
   # Total bandwidth
   l2 = sum(r.dcBW for r in enabledL2)
   ef = sum(r.beBW for r in enabledEF)
   print(row([cell(l2, size='h4'), cell('Bandwidth (MB/s)', size='h4'), cell(ef, size='h4')]))

   # Total racks enabled in DC1 (for L2) or BE1 (for EF)
   l2 = len([r for r in enabledL2 if r.dc == 'dc1'])
   ef = len([r for r in enabledEF if r.be == 'ef1'])
   print(row([cell(l2, size='h4'), cell('Enabled in DC1/BE1', size='h4'), cell(ef, size='h4')]))

   # Total racks enabled in DC2 (for L2) or BE2 (for EF)
   l2 = len([r for r in enabledL2 if r.dc == 'dc2'])
   ef = len([r for r in enabledEF if r.be == 'ef2'])
   print(row([cell(l2, size='h4'), cell('Enabled in DC2/BE2', size='h4'), cell(ef, size='h4')]))

   # Total number of cores per farm
   l2 = sum(r.numMachines * r.numCores for r in enabledL2)
   ef = sum(r.numMachines * r.numCores for r in enabledEF)
   print(row([cell(l2, size='h4'), cell('Number of cores', size='h4'), cell(ef, size='h4')]))

   # The number of cores scaled by the processor performance
   l2 = int(sum(r.numMachines * r.numCores * r.cpuWeight for r in enabledL2))
   ef = int(sum(r.numMachines * r.numCores * r.cpuWeight for r in enabledEF))
   print(row([cell(l2, size='h4'), cell('Scaled number of cores', size='h4'), cell(ef, size='h4')]))

   print('</table>')
   print(SEPARATOR)
   print(TABLE_HEAD)

   scriptSection = ''
   for num in rackNumbers:
      l2Rack = [r for r in l2Racks if num in r.name]
      efRack = [r for r in efRacks if num in r.name]
      rack = (l2Rack or efRack)[0]

      cells = [makeRackCell(r[0]) if r else cell('', size='h3') for r in [l2Rack, efRack]]
      print(row(cells, onclick='toggle{0}()'.format(num)))

      stats = []
      if rack.be != 'none':
         stats.append('net {0}: {1} MB/s'.format(rack.be.upper(), rack.beBW))
      else:
         stats.append('no EF net')
      stats.append('# machines in rack: {0}'.format(rack.numMachines))
      print(row([cell(s) for s in stats], cls='stats{0}'.format(num)))

      stats = []
      if rack.dc != 'none':
         stats.append('net {0}: {1} MB/s'.format(rack.dc.upper(), rack.dcBW))
      else:
         stats.append('no DC net')
      stats.append('# cores / machine: {0}'.format(rack.numCores))
      print(row([cell(s) for s in stats], cls='stats{0}'.format(num)))

      scriptSection += SCRIPT_TEMPLATE.format(num)
   print('</table>')
   
   print(SEPARATOR)
   print(TABLE_HEAD)
   cell1 = cell('Legend', rowspan=len(Segment.statuses), size='h3')
   cell2 = cell(Segment.statuses[0], colour=rackColours[Segment.statuses[0]])
   print(row([cell1, cell2]))
   for status in Segment.statuses[1:]:
      print(row([cell(status, colour=rackColours[status])]))
   print('</table>')

   print('<script type="text/javascript">')
   print(scriptSection)
   print('</script>')
   
   print(FOOTER)
