#!/usr/bin/env tdaq_python
import sys
import os
from xml.dom import minidom


    
def getPath(filename, pathenv):
    pathlist=''
    try:
        pathlist = os.getenv(pathenv,'').split(os.pathsep)
    except:
        raise NameError('os.getenv failed!')

    for path in pathlist:
        f = os.path.join( path, filename )
        if os.access( f, os.R_OK ):
            return f


def useCoral(db='atlas_config', user='ATLAS_SFO_T0_R'):
    dbauthfilename=''
    try:
        dbauthfilename = getPath('authentication.xml','CORAL_AUTH_PATH')
        if dbauthfilename == None:
            raise RuntimeError('database not found in CORAL_AUTH_PATH')
    except NameError:
        raise

    authDict = {}
    doc = minidom.parse(dbauthfilename)
    full_db = 'oracle://'+ db + '/' + user
    for cn in doc.getElementsByTagName('connection'):
        pw = ""
        svc = cn.attributes['name'].value
        if (svc == full_db): 
            for p in cn.getElementsByTagName('parameter'):
                if p.attributes['name'].value == 'password':
                    pw = p.attributes['value'].value
                authDict[cn.attributes['name'].value] = (user,pw)
    doc.unlink()
    
    solvedb = authDict[full_db][0] + '/'+ authDict[full_db][1] + '@' + db
        
    return solvedb


def on_off_db_switch(switch='offline'):
    db =''
    user='ATLAS_SFO_T0_R'
    if switch == 'offline':
        db='atlas_config'
    elif switch == 'online':
        db='atonr_conf'
    else:
        raise NameError('db must be offline or online')
    solved_db = ''
    try:
        solved_db = useCoral(db, user)

    except (RuntimeError,NameError):
        raise
    return solved_db


