#!/usr/bin/env tdaq_python

from __future__ import absolute_import
from builtins import map
from builtins import object
import cx_Oracle
from operator import itemgetter

import threading
import logging
from sfodb.sfodb_auth import on_off_db_switch

class SFOFetch(object):
    """This class provides a db connection and methods to send queries to
    the db. It also provides several precanned queries with useful output
     formatting in form of additional class methods"""

    def __init__(self,arraysize=5,db='offline'):
        logging.info("Creating new SQL connection")
        try:
            solved_db = on_off_db_switch(db)
            self.oracle = cx_Oracle.connect(solved_db)
            self.curs=self.oracle.cursor()
            self.curs.arraysize=arraysize
        except cx_Oracle.Error as e:
            raise RuntimeError('Database ', solved_db, 'connection went wrong')
        except (RuntimeError,NameError):
            raise

    def query(self,query,parameterdict=None,fetchall=False):
        """This is a generic function returning the reply from the db given a SQL query. parameterdict
        is a dictionary containing any replaceable parameters to send to the query"""
        t=threading.Timer(1000,self.oracle.cancel)
        t.start()
        try:
            if parameterdict is None:
                self.curs.execute(query)
            else:
                self.curs.execute(query,parameterdict)
            if fetchall:
                res = self.curs.fetchall()
            else:
                res = self.curs.fetchmany()

        except cx_Oracle.OperationalError:
            raise
        finally:
            t.cancel()
        return res

    def valid_runs(self,parameterdict,
            sfo_search_string="""(SFOID like 'SFO%' or SFOID like 'MUCal%')"""):
        """A precanned query, returning the number of valid runs in SFO_TZ_RUN"""
        runs = self.query("""select runnr from SFO_TZ_RUN
                    where runnr < 1000000 and runnr >=:minrunnr
                    and"""+sfo_search_string+
                    """group by runnr
                    order by runnr desc""",parameterdict=parameterdict)
        return list(map(itemgetter(0),runs))

    def __query_maker__(self,runs):
        """this helper function avoids code duplication in nr_opened_files, nr_closed_files and nr_transferred files """
        vars={}
        run_selec=[]

        for i in runs:
            varname = 'run%d' %i
            run_selec.append(' runnr =: %s ' % varname)
            vars[varname] = i

        selec = ''.join(['(',' or '.join(run_selec),')'])
        return (selec,vars)



    def nr_opened_files(self,runs):
        """Returns the number of open files for each run in the input list - runs"""
        selec,vars = self.__query_maker__(runs)
        tmp=dict(self.query("""select runnr,count(lfn) from SFO_TZ_FILE
                          where filestate='OPENED' and """ + selec +
                          " group by runnr",vars))
        return [tmp.get(r,0) for r in runs]

    def nr_closed_files(self,runs):
        """returns a list containing the number of closed files for each
         run in the input list -
        runs"""
        selec,vars = self.__query_maker__(runs)
        tmp=dict(self.query("""select runnr,count(lfn) from SFO_TZ_FILE
                            where filestate<>'OPENED' and """ + selec +
                            " group by runnr",vars))
        return [tmp.get(r,0) for r in runs]

    def nr_transferred_files(self,runs):
        """Returns a list containing the number of transferred files for
        each run in the input list - runs"""
        selec,vars = self.__query_maker__(runs)
        tmp = dict(self.query("""select runnr, count(lfn) from SFO_TZ_FILE
                              where filestate<>'OPENED' and
                              transferstate='TRANSFERRED' and """ + selec +
                              "group by runnr",vars))
        return[tmp.get(r,0) for r in runs]
    def run_creation_time(self,run):
        """Returns the run creation time"""

        tmp=self.query("""select CREATION_TIME from SFO_TZ_RUN
                            where runnr=:srunnr
                            group by CREATION_TIME""",{'srunnr':run})
        return list(map(itemgetter(0),tmp))


    def get_run_data(self, run):
        """gets the number of open, closed and transferred files and also the
        run creation time for the specified run number"""
        return [self.nr_opened_files([run])[0],self.nr_closed_files([run])[0],
                                     self.nr_transferred_files([run])[0],
                                     self.run_creation_time(run),
                                     self.transferred_volume(run)]

    def get_sfos(self,run):
        """returns a list of the SFOs associated with a run"""
        tmp=self.query("""select sfoid from SFO_TZ_RUN where runnr =: srunnr
                            group by sfoid
                            ORDER BY sfoid ASC""",{'srunnr':run})
        return list(map(itemgetter(0),tmp))
    def open_per_sfo(self,run,sfos):
        tmp=dict(self.query("""select sfoid,count(lfn) from SFO_TZ_FILE
                       where filestate='OPENED' and runnr=:srunnr
                       group by sfoid""",{'srunnr':run}))
        return [tmp.get(s,0) for s in sfos]

    def closed_per_sfo(self,run,sfos):
        """note that this isn't in face closed! it's !open"""
        tmp=dict(self.query("""select sfoid,count(lfn) from SFO_TZ_FILE
                       where filestate<>'OPENED' and runnr=:srunnr
                       group by sfoid""",{'srunnr':run}))
        return [tmp.get(s,0) for s in sfos]

    def deleted_per_sfo(self,run,sfos):
        tmp=dict(self.query("""select sfoid,count(lfn) from SFO_TZ_FILE
                       where filestate='DELETED' and runnr=:srunnr
                       group by sfoid""",{'srunnr':run}))
        return [tmp.get(s,0) for s in sfos]

    def transferred_per_sfo(self,run,sfos):
        tmp=dict(self.query("""select sfoid,count(lfn) from SFO_TZ_FILE
                       where filestate<>'OPENED' and
                       transferstate='TRANSFERRED'  and runnr=:srunnr
                       group by sfoid""",{'srunnr':run}))
        return [tmp.get(s,0) for s in sfos]

    def truncated_per_sfo(self, run,sfos):
        querydict={'srunnr':run}
        tmp=dict(self.query("""select sfoid,count(lfn) from SFO_TZ_FILE
                    where filehealth='TRUNCATED' and runnr=:srunnr
                    group by sfoid""",querydict))
        return [tmp.get(s,0) for s in sfos]

    def transferred_volume(self,run):
        querydict={'srunnr':run}
        tmp=self.query("""select sum(filesize) from SFO_TZ_FILE where
                       transferstate='TRANSFERRED' and runnr=:srunnr""",
                       querydict)[0]
        return tmp[0]

    def get_run_details(self, run):
        querydict={'srunnr':run}
        sfostatus={}
        sfos=self.get_sfos(run)
        for s,op,cl,de,tr,trun in zip(sfos,self.open_per_sfo(run,sfos),
                                  self.closed_per_sfo(run,sfos),
                                  self.deleted_per_sfo(run,sfos),
                                  self.transferred_per_sfo(run,sfos),
                                  self.truncated_per_sfo(run,sfos)):

            try:
                upratio=1.*tr/cl
            except ZeroDivisionError:
                upratio=0

            try:
                delratio=1.*de/cl
            except ZeroDivisionError:
                delratio=0

            sfostatus[s]=[op,cl,tr,upratio,delratio,trun]
        return sfostatus
