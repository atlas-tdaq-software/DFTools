#!/usr/bin/env tdaq_python


from __future__ import print_function
from __future__ import absolute_import
from builtins import range
from builtins import object
import sys
import shutil
import optparse
from pm.project import Project
from .Segment import Segment, findSegment


class LoadInterruptedError(Exception):
   """Raise this when load is interrupted."""

class NoPartitionError(Exception):
   """Raise this when the database has no valid partitions"""


class Database(object):

   """
   Wrapper for pm.project.Project.

   Include methods that make access to certain objects easy and
   uniform, as well as methods to manage file loading/saving.

   Example:
   l2Seg returns the top level Level 2 segment, whether it's a direct
   descendant of the partition or the TDAQ segment.
   """

   def __init__(self):
      self.fileName = None # Core variables
      self.partName = None
      self.project = None
      self.parameters = None

   @property
   def partition(self):
      return self.project.getObject('Partition', self.partName)

   @property
   def l2Seg(self):
      dalSeg = findSegment(self.partition, 'L2')
      return Segment(dalSeg, self.partition, self.parameters)

   @property
   def efSeg(self):
      dalSeg = findSegment(self.partition, 'EBEF')
      return Segment(dalSeg, self.partition, self.parameters)

   @property
   def l2Racks(self):
      return self.l2Seg.Racks

   @property
   def efRacks(self):
      return self.efSeg.Racks

   def load(self, fileName, partName=None, existingProject=None):
      assert fileName
      oldValues = [self.fileName, self.partName, self.project]
      # First, it's crucial that we don't use the old Segments
      Segment.clearInstances()
      # Open the said project
      self.fileName = fileName
      if existingProject:
         self.project = existingProject
      else:
         self.project = Project(fileName)
      # Set the partition name
      if partName:
         self.partName = partName
      else: # If partName is not given, try to figure it out
         partitions = self._getPartitions()
         if len(partitions) < 1:
            raise NoPartitionError('No valid partition to load!')
         elif len(partitions) == 1:
            # The most common case
            self.partName = partitions[0].id
         else: # len(partitions) > 1
            # Too many partitions to atomatically select from!
            # selectPartition is not implemented in this base class
            # One can implement a GUI or CLI selection method
            try:
               self.partName = self.selectPartition(partitions)
            except LoadInterruptedError:
               # Load process was interrupted, so no instance
               # variables get changed
               self.fileName, self.partName, self.project = oldValues
               raise
      try:
         self.parameters = Project('daq/hw/rack-properties.data.xml')
         self.parameters.getObject('RackParameters')
      except RuntimeError:
         self.parameters = None
         raise IOError('Could not load "rack-properties.data.xml"!')

   def _getPartitions(self):
      partitions = self.project.getObject('Partition')
      return [p for p in partitions if p.Segments]

   def save(self):
      if not self.project: return
      self.project.updateObjects([self.partition], recurse=False)

   def saveAs(self, newName):
      if not self.project: return
      # Get a set of segment ids from the old Disabled list
      oldDisabled = set(s.id for s in self.partition.Disabled)
      shutil.copy2(self.fileName, newName)
      self.load(newName, self.partName)
      # Get a set of segment ids from the new Disabled list
      newDisabled = set(s.id for s in self.partition.Disabled)
      # Update the new disabled list to match the old one
      # First remove what's in the new one, but not in the old
      # Start removing from the end to the start
      toRemove = newDisabled - oldDisabled
      for i in reversed(list(range(len(self.partition.Disabled)))):
         if self.partition.Disabled[i].id in toRemove:
            del(self.partition.Disabled[i])
      # Now add what's in the old one, but not in the new
      toAdd = oldDisabled - newDisabled
      for s in toAdd:
         self.partition.Disabled.append(self.project.getObject('Segment', s))
      # Done updating the disabled list, now save
      self.save()


def parseArguments(argv=None):
   if not argv:
      argv = sys.argv[1:]
   parser = optparse.OptionParser()
   parser.add_option('-d', '--database', dest='database',
                     help='open DATABASE', metavar='DATABASE')
   parser.add_option('-p', '--partition', dest='partition',
                     help='open PARTITION', metavar='PARTITION')

   (options, args) = parser.parse_args(argv)

   return (options.database, options.partition)


def main():
   print()
   print('*******************************************')
   print('* NOTE: THIS FILE IS MEANT FOR IMPORTING. *')
   print('*******************************************')
   print()

if __name__ == '__main__':
   main()
