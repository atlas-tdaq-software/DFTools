#!/usr/bin/env tdaq_python


from __future__ import absolute_import
from builtins import str
import os
import subprocess as subpr
import sys

from PyQt4.QtCore import *
from PyQt4.QtGui  import *

from .hWidgets import *
from .hilltrot import Database, LoadInterruptedError, NoPartitionError
from .Segment import Segment


class MainWidget(QFrame, Database):
   def __init__(self):
      QFrame.__init__(self)
      Database.__init__(self)

      self.addViews()
      self.addButtons()

   @property
   def listsAndTrees(self):
      return ((self.l2List, self.efList), (self.l2Tree, self.efTree))

   def addViews(self):
      # List widgets
      l2List = self.l2List = TreeWidget()
      efList = self.efList = TreeWidget()
      # Tree widgets
      l2Tree = self.l2Tree = TreeWidget()
      efTree = self.efTree = TreeWidget()

      # Summary/Legend
      l2Info = self.l2Info = Label()
      efInfo = self.efInfo = Label()
      # Put all in two frames
      l2Frame = VBox([Label('L2'), l2List, l2Tree, l2Info])
      efFrame = VBox([Label('EF'), efList, efTree, efInfo])
      # The log panel
      self.log = LogWidget(self)
      # Show the errors in the log panel
      sys.stderr = self.log
      # And warn the user if modifying a file can potentially commit to OKS
      warn = 'Any changes done to an OKS file are automatically commited when saving!'
      if 'TDAQ_DB_REPOSITORY' in list(os.environ.keys()):
         self.log.warning(warn)

      splitter = HSplitter([l2Frame, efFrame])
      splitter = VSplitter([splitter, self.log])
      self.toolbar = QToolBar()
      self.vbox = VLayout([self.toolbar, splitter])
      self.setLayout(self.vbox)

      # Setup the enable and disable actions
      enable = Action('enable', self, self.enableSegment)
      disable = Action('disable', self, self.disableSegment)

      # Create the right-click menu
      for i in (l2List, l2Tree, efList, efTree):
         i.createRightClickMenu([enable, disable])

   @property
   def focusedView(self):
      lists, trees = self.listsAndTrees
      for i in lists + trees:
         if i.hasFocus():
            return i

   def refreshSummary(self):
      # Helper functions for the embedded HTML code
      def cell(item):
         # For a QBrush get the color and return an empty colored cell
         try:
            color = item.color().name()
            return '<td bgcolor={0}>&nbsp;&nbsp;</td>'.format(color)
         # For any other item return a cell containing it's representation
         except AttributeError:
            return '<td>{0}</td>'.format(item)
      def row(items):
         # Return a row containing cells of all the items
         return '<tr>{0}</tr>'.format(''.join(cell(i) for i in items))
      def table(items):
         # Return a table containing rows of all the items
         # The row borders are invisible
         s = '<table border=0>{0}</table>'
         return s.format(''.join(row(i) for i in items))

      # Nothing to update if we haven't got a project loaded
      if not self.project: return
      # For each panel (L2 and EF)
      for items, label in [(self.l2Racks, self.l2Info), (self.efRacks, self.efInfo)]:
         # One field for each status
         fields = []
         for status in Segment.statuses:
            color = colorsUsed[status]
            count = len([i for i in items if i.status == status])
            # Each field contains a color, status and count
            fields.append([color, status, count])
         # Display the fields in two columns, left & right
         left  = table(fields)
         stats = []
         if self.parameters:
            enabled = [i for i in items if i.status == Segment.ENABLED]
            if label is self.l2Info:
               stats.append(['Bandwidth (MB/s)', sum(i.dcBW for i in enabled)])
               dc1 = [i for i in enabled if i.dc == 'dc1']
               dc2 = [i for i in enabled if i.dc == 'dc2']
               stats.append(['Enabled in DC1', len(dc1)])
               stats.append(['Enabled in DC2', len(dc2)])
            else:
               stats.append(['Bandwidth (MB/s)', sum(i.beBW for i in enabled)])
               ef1 = [i for i in enabled if i.be == 'ef1']
               ef2 = [i for i in enabled if i.be == 'ef2']
               stats.append(['Enabled in BE1', len(ef1)])
               stats.append(['Enabled in BE2', len(ef2)])
            numCores = [i.numCores * i.numMachines for i in enabled]
            scaledCores = [i.numCores * i.numMachines * i.cpuWeight for i in enabled]
            stats.append(['Number of cores', sum(numCores)])
            stats.append(['Scaled number of cores', int(sum(scaledCores))])
         right = table(stats)
         # A table containing two cells on the same row: left & right
         summary = table([[left, right]])
         label.setText(summary.format(left, right))

   def enableSegment(self):
      view = self.focusedView
      disabled = []
      for item in view.selectedItems:
         disabled += item.seg.enable()
      self.refreshColors()
      if disabled:
         # Update the log if any racks were disabled
         msg = 'The following racks were automatically disabled:\n'
         msg += '\n'.join(str(s) for s in disabled)
         self.log.warning(msg)
      if 'dbe_running' in globals():
         self.save()

   def disableSegment(self):
      view = self.focusedView
      for item in view.selectedItems:
         item.seg.disable()
      self.refreshColors()
      if 'dbe_running' in globals():
         self.save()

   def updateViews(self):
      lists, trees = self.listsAndTrees
      if self.treeButton.isChecked():
         for l in lists: l.hide()
         for t in trees: t.show()
      else:
         for l in lists: l.show()
         for t in trees: t.hide()

   def addButtons(self):
      # Add the tree/list view radio buttons
      self.treeButton = RadioButton('&Tree', 'Tree view', self.updateViews)
      self.listButton = RadioButton('&List', 'List view')
      self.toolbar.addWidget(self.treeButton)
      self.toolbar.addWidget(self.listButton)
      self.treeButton.toggle()
      # Add a search bar
      def onChange(text):
         l, t = self.listsAndTrees
         for i in l + t:
            i.filterItems(str(text))
      self.searchBar = QLineEdit(self)
      self.connect(self.searchBar, SIGNAL('textChanged(QString)'), onChange)
      self.toolbar.addSeparator()
      label = QLabel('&Search')
      label.setBuddy(self.searchBar)
      self.toolbar.addWidget(label)
      self.toolbar.addWidget(self.searchBar)
      button = QPushButton('Create DQMD XMLs')
      QObject.connect(button, SIGNAL('clicked()'), self.createDqmd)
      self.toolbar.addWidget(button)
   
   def createDqmd(self):
      # Get the name of the loaded database
      dbName = self.fileName
      if not dbName or 'dbe_running' in globals():
         return

      dirName = Dialogs.getDir(self, 'Select destination directory...',
                               os.getcwd())
      dirName = str(dirName)
      if not dirName: # The user cancelled the dialog
         return
      # The output of the scripts is in the 'out' dir
      if not os.path.exists(dirName + os.sep + 'out'):
         os.makedirs(dirName + os.sep + 'out')
      # Run the scripts, one at a time
      for script in ['tdaqm_seg.py', 'tdaqm_l2seg.py', 'tdaqm_rosseg.py']:
         self.log.info('Running "{0} {1}"'.format(script, dbName))
         proc = subpr.Popen([script, dbName], stdout=subpr.PIPE,
                            stderr=subpr.STDOUT, cwd=dirName)
         if proc.returncode:
            self.log.error(proc.communicate()[0])
            break # Stop running if one script failed


   def addItemsToList(self, items, parent):
      items.sort(key=lambda rack: rack.rackName)
      for rack in items:
         RackViewItem(rack, parent)

   def selectPartition(self, partitions):
      names = [p.id for p in partitions]
      selection = Dialogs.listSelect(self, 'Select partition',
                     'Partition:', names)
      if not selection[1]:
         raise LoadInterrupted()
      return str(selection[0])

   def load(self, fileName, partName=None, existingProject=None):
      try:
         Database.load(self, fileName, partName, existingProject)
      except NoPartitionError as e:
         self.log.error(str(e))
         return          # Can't load, so exit
      except LoadInterruptedError:
         return          # User cancelled load, nothing to do
      except IOError as e: # Rack properties was not loaded
         self.log.warning(str(e))
      self.refreshItems()
      self.searchBar.clear()

   def saveAs(self, newName):
      Database.saveAs(self, newName)
      self.refreshItems()

   def refreshItems(self):
      lists, trees = self.listsAndTrees
      # First clear everything
      for i in lists + trees: i.clear()
      # Then add the items back
      SegmentViewItem(self.l2Seg, self.l2Tree)
      SegmentViewItem(self.efSeg, self.efTree)
      self.addItemsToList(self.l2Racks, self.l2List)
      self.addItemsToList(self.efRacks, self.efList)
      self.refreshSummary()

   def refreshColors(self):
      lists, trees = self.listsAndTrees
      for view in lists + trees:
         for item in view.allItems:
            item.updateColor()
      self.refreshSummary()

   def logState(self):
      s = '{0}\n{1}'
      if self.treeButton.isChecked():
         s = s.format(self.l2Tree.printableStr, self.efTree.printableStr)
      else:
         s = s.format(self.l2List.printableStr, self.efList.printableStr)
      self.log.info(s)
