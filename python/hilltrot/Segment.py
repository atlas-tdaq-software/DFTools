#!/usr/bin/env tdaq_python


from builtins import object
def findSegment(seg, expr):
   """
   Find a subsegment of seg with expr in it's name.
   """
   # seg is a dal object, since we're accessing seg.id.
   # Using this function to get the top L2/EF segments, instead of
   # retrieving it directly with project.getObject, since this
   # it also works for files containing multiple partitions
   myQueue = [seg]
   while myQueue:
      s = myQueue.pop(0)
      if expr in s.id:
         return s
      myQueue += s.Segments


class SegmentError(Exception):
   """
   An error thrown by the Segment methods.
   """


class Segment(object):

   """
   Wraps a DAL Segment/HLTSegment from a partition.

   Include some useful methods like enabling or disabling a segment,
   which are normally partition operations.
   """

   # The possible statuses of a segment
   UNUSED    = 'Unused'
   ENABLED   = 'Enabled'
   DISABLED  = 'Disabled'
   HIERARCHY = 'Disabled by parent'
   CONFLICT  = 'In conflict'

   statuses = [UNUSED, ENABLED, DISABLED, HIERARCHY, CONFLICT]
   statuses.sort()

   # Keep a record of existing instances and reuse them
   # Dictionary from the dal id to the actual object
   _instances = {}

   @classmethod
   def clearInstances(cls):
      """
      Clear all the existing instances.

      Use when loading a new partition, for example. It may have
      dal objects with the same name, but we want them represented
      by different Segment objects.
      """
      cls._instances = {}

   def __new__(cls, dal, partition, parameters=None):
      """
      Create a new instance or return an existing object.
      """
      try:
         # If the object we want already exists
         return cls._instances[dal.id]
      except KeyError:
         # If not, create, and add it to the record
         newObj = super(Segment, cls).__new__(cls)
         cls._instances[dal.id] = newObj
         return newObj

   def __init__(self, dal, partition, parameters=None):
      """
      Initialize a segment if it's been just created.

      First check the segment's attributes - if it has specific
      attributes, it is not a new segment, so don't initialize it.

      Keyword arguments:

      dal -- The DAL object it wraps.

      partition -- The DAL Partition object in which the previous
      argument is contained.
      """

      # This is useful to avoid reinitializing existing objects
      if hasattr(self, '_dal') and hasattr(self, '_part'):
         return

      # It's a new object, so set these
      self._dal = dal
      self._part = partition
      self.name = dal.id

      # dalType can be 'rack' or 'segment'
      if 'rack' in dal.id and dal.className() == 'HLTSegment':
         self.dalType = 'rack'
         self.rackName = dal.id[dal.id.index('rack'):]
         if parameters:
            params = parameters.getObject('RackParameters', self.rackName)
            self.type = params.Type
            self.be = params.BackEnd
            self.beBW = params.BackEndBW
            self.cpuWeight = params.CPUweight
            self.dc = params.DataCollection
            self.dcBW = params.DataCollectionBW
            self.numMachines = params.NumberOfMachines
            self.numCores = params.NumberOfPhysicalCores
            if self.type == 'EF' and self.dalType == 'rack':
               self.rackName += ' (EF)'
      elif dal.className() == 'Segment':
         self.dalType = 'segment'
      else:
         msg = '{0} is neither a rack nor a segment'
         raise SegmentError(msg.format(dal.id))

      # belongsTo 'L2' or 'EF'. May also be the root L2 or EF segment
      if 'L2' in dal.id:
         self.belongsTo = 'L2'
      elif 'EF' in dal.id or 'EBF' in dal.id:
         self.belongsTo = 'EF'
      else:
         msg = '{0} belongs neither to L2 nor to EF'
         raise SegmentError(msg.format(dal.id))

      # Create segments
      self.Segments = []
      if self.dalType == 'segment':
         for seg in dal.Segments:
            if 'L2' in seg.id or 'EF' in seg.id or 'EBF' in seg.id:
               self.Segments.append(Segment(seg, partition, parameters))

      # Get L2 and EF segments
      l2 = findSegment(partition, 'L2')
      ef = findSegment(partition, 'EBEF')

      # Get parent
      myQueue = [l2, ef]
      if dal in myQueue: # L2 and EF are top-level segments
         self.parent = None
      else:
         while myQueue:
            seg = myQueue.pop(0)
            if dal in seg.Segments:
               self.parent = Segment(seg, partition, parameters)
               break
            if seg.className() == 'Segment':
               myQueue += seg.Segments

      # Get a rack's "twin", or same rack in the other segment
      if self.dalType == 'rack':
         self.twin = None
         myQueue = [l2] if self.belongsTo == 'EF' else [ef]
         while myQueue:
            seg = myQueue.pop()
            if seg.className() == 'Segment':
               myQueue += seg.Segments
            else:
               if self.rackName == seg.id[seg.id.index('rack'):]:
                  self.twin = Segment(seg, partition, parameters)
                  break

   def __str__(self):
      """
      Return the segment name.
      """
      return self.name

   def __repr__(self):
      """
      Return the segment name and dalType.
      """
      return '<{0} {1}>'.format(self.name, self.dalType)

   @property
   def _simpleStatus(self):
      """
      Return the status without doing 'conflict' or 'unused' checks.
      """
      if self._dal in self._part.Disabled:
         return Segment.DISABLED
      if self.parent is None or self.parent._simpleStatus \
                                == Segment.ENABLED:
         return Segment.ENABLED
      return Segment.HIERARCHY

   @property
   def status(self):
      """
      Return the status of a segment.

      Properly check for conflicts and unused racks. The returned
      string is one of Segment.Statuses
      """

      status = self._simpleStatus
      if self.dalType == 'segment':
         # Only racks can be unused or in conflict, so segments don't
         # require additional checks
         return status

      # If it's enabled check for conflicts
      if status == Segment.ENABLED:
         if self.twin and self.twin._simpleStatus == Segment.ENABLED:
            status = Segment.CONFLICT

      # If it's disabled check if unused
      elif status == Segment.DISABLED:
         if not self.twin or self.twin._simpleStatus \
                             == Segment.DISABLED:
            status = Segment.UNUSED
      return status

   def _solveConflicts(self):
      """
      Disable a rack's twin if causing conflicts.

      If self is a segment, recurse on it's subsegments.
      """
      disabled = []
      if self.dalType == 'rack':
         if self.twin and self.twin.status == Segment.CONFLICT:
            self.twin.disable()
            disabled = [self.twin]
      else:
         for seg in self.Segments:
            disabled += seg._solveConflicts()
      return disabled

   def disable(self):
      """
      Disable the segment, if not already.
      """
      if self._dal not in self._part.Disabled:
         self._part.Disabled.append(self._dal)

   def enable(self):
      """
      Enable the segment, if not already.

      Conflicts created by enabling this segment are resolved by
      disabling racks in the other segment (L2/EF).
      """
      if self._dal in self._part.Disabled:
         self._part.Disabled.remove(self._dal)
      return self._solveConflicts()

   @property
   def Racks(self):
      """
      The sub-racks of this segment.

      A list containing the object itself if it's dalType is rack.
      """
      if self.dalType == 'rack':
         return [self]
      racks = []
      for seg in self.Segments:
         racks += seg.Racks
      return racks
