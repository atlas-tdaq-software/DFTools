#!/usr/bin/env tdaq_python


from __future__ import absolute_import
from builtins import str
from builtins import object
import time
from PyQt4.QtCore import *
from PyQt4.QtGui  import *
from .Segment import Segment


class Color(object):
   RED        = QBrush(Qt.red)
   GREEN      = QBrush(Qt.green)
   BLUE       = QBrush(Qt.blue)
   BLACK      = QBrush(QColor(  0,   0,   0))
   WHITE      = QBrush(QColor(255, 255, 255))
   PURPLE     = QBrush(QColor(128,   0, 128))
   PINK       = QBrush(QColor(255,   0, 255))
   YELLOW     = QBrush(QColor(255, 255,   0))
   LIGHT_GREY = QBrush(QColor(192, 192, 192))
   DARK_RED   = QBrush(QColor(192,   0,   0))


class Label(QLabel):
   def __init__(self, text=''):
      super(Label, self).__init__(text)
      self.setAlignment(Qt.AlignHCenter)


class VLayout(QVBoxLayout):
   def __init__(self, items=[]):
      super(VLayout, self).__init__()
      self.setMargin(0)
      self.setSpacing(0)
      self.addWidgets(items)
   
   def addWidgets(self, items):
      for item in items:
         self.addWidget(item)


class VBox(QFrame):
   def __init__(self, items=[]):
      super(VBox, self).__init__()
      layout = VLayout(items)
      self.setLayout(layout)


class HLayout(QHBoxLayout):
   def __init__(self, items=[]):
      super(HLayout, self).__init__()
      self.setMargin(0)
      self.setSpacing(0)
      self.addWidgets(items)
   
   def addWidgets(self, items):
      for item in items:
         self.addWidget(item)


class HBox(QFrame):
   def __init__(self, items=[]):
      super(HBox, self).__init__()
      layout = HLayout(items)
      self.setLayout(layout)


class HSplitter(QSplitter):
   def __init__(self, items=[]):
      super(HSplitter, self).__init__()
      for item in items:
         self.addWidget(item)


class VSplitter(QSplitter):
   def __init__(self, items=[]):
      super(VSplitter, self).__init__(Qt.Vertical)
      for item in items:
         self.addWidget(item)


class TreeWidget(QTreeWidget):
   def __init__(self):
      super(TreeWidget, self).__init__()
      self.setColumnCount(1)
      self.header().close()
      self.children = []

   def createRightClickMenu(self, actions):
      self.setContextMenuPolicy(Qt.ActionsContextMenu)
      self.setSelectionMode(QAbstractItemView.ExtendedSelection)
      for action in actions:
         self.addAction(action)

   def filterItems(self, text):
      for c in self.children:
         c.filterItems(text)

   @property
   def allItems(self):
      items = []
      for child in self.children:
         items += child.allItems
      return items

   @property
   def selectedItems(self):
      return [item for item in self.allItems if item.isSelected()]

   def clear(self):
      super(TreeWidget, self).clear()
      self.children = []

   @property
   def printableStr(self):
      return ''.join(c.printableStr() for c in self.children)


class TreeWidgetItem(QTreeWidgetItem):
   def __init__(self, name, parent):
      super(TreeWidgetItem, self).__init__(parent)
      self.setText(0, name)
      self.name = name
      self.parent = parent
      self.children = []
      parent.children.append(self)

   @property
   def parentTree(self):
      if isinstance(self.parent, TreeWidget):
         return self.parent
      return self.parent.parentTree

   @property
   def allItems(self):
      items = [self]
      for child in self.children:
         items += child.allItems
      return items

   def filterItems(self, text):
      visible = text.lower() in str(self.text(0)).lower()
      for c in self.children:
         visible |= c.filterItems(text)
      self.setHidden(not visible)
      return visible

   def printableStr(self, spaces=''):
      marker = '!' if self.seg.status == Segment.DISABLED else ''
      printStr = '{0}{1}{2}\n'.format(spaces, marker, self.name)
      for c in self.children:
         printStr += c.printableStr('  ' + spaces)
      return printStr


# Predefined colors for different statuses
colorsUsed = {
   Segment.UNUSED : Color.GREEN,
   Segment.ENABLED : Color.BLACK,
   Segment.DISABLED : Color.LIGHT_GREY,
   Segment.HIERARCHY : Color.BLUE,
   Segment.CONFLICT : Color.RED
}


class SegmentViewItem(TreeWidgetItem):
   def __init__(self, seg, parent):
      super(SegmentViewItem, self).__init__(seg.name, parent)
      self.seg = seg
      self.updateColor()
      for item in seg.Segments:
         SegmentViewItem(item, self)

   def updateColor(self):
      self.setForeground(0, colorsUsed[self.seg.status])


class RackViewItem(TreeWidgetItem):
   def __init__(self, seg, parent):
      super(RackViewItem, self).__init__(seg.rackName, parent)
      self.seg = seg
      self.updateColor()

   def updateColor(self):
      self.setForeground(0, colorsUsed[self.seg.status])


class RadioButton(QRadioButton):
   def __init__(self, name, statusTip, toggleAction=None):
      super(RadioButton, self).__init__(name)
      self.setStatusTip(statusTip)
      if toggleAction:
         QObject.connect(self, SIGNAL('toggled(bool)'), toggleAction)


class Action(QAction):
   def __init__(self, name, parent, func=None,
                shortcut=None, statusTip=None):
      super(Action, self).__init__(name, parent)
      if func:
         QObject.connect(self, SIGNAL('triggered()'), func)
      if shortcut:
         self.setShortcut(shortcut)
      if statusTip:
         self.setStatusTip(statusTip)


class Dialogs(object):
   openFile = QFileDialog.getOpenFileName
   saveFile = QFileDialog.getSaveFileName
   getDir   = QFileDialog.getExistingDirectory

   @staticmethod
   def listSelect(parent, title, label, items):
      return QInputDialog.getItem(parent, title, label, items, 0, False)


class LogWidget(QTextEdit):
   def __init__(self, parent):
      super(LogWidget, self).__init__(parent)
      self.setReadOnly(True)

   def _append(self, logType, txt):
      """Write a text with a header of the form TIMESTAMP - TYPE"""
      header = '%d-%m-%Y %H:%M:%S - {0}\n'.format(logType)
      header = time.strftime(header)
      self.append(header + txt)
   
   def error(self, txt):
      self._append('ERROR', txt + '\n')

   def warning(self, txt):
      self._append('WARNING', txt + '\n')

   def info(self, txt):
      self._append('INFO', txt + '\n')

   def write(self, txt):
      """Write a text to the log window without adding a header"""
      self.append(txt)
